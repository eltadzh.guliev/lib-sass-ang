import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { CountdownModule,FileUploadModule, IconnedTextModule, PaginationModule, PanelInfoModule, RecordColumnModule, TabsBigModule, TabsSmallModule, ToastModule, ConfirmationWithIconModule, ConfirmationWithReasonModule} from 'components';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TabsSmallModule,
    TabsBigModule,
    RecordColumnModule,
    PanelInfoModule,
    PaginationModule,
    IconnedTextModule,
    FileUploadModule,
    ToastModule,
    CountdownModule,
    ConfirmationWithIconModule,
    ConfirmationWithReasonModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
