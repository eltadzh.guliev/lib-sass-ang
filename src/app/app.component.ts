import { Component } from '@angular/core';
import { ConfirmationWithIconService, ConfirmationWithReasonService, ToastService } from 'components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  constructor(private toast: ToastService,
    private dialogService: ConfirmationWithReasonService) {
      dialogService.open({
        data: {
          title: 'Sorğunu ləğv edək?',
          description:
            'Sorğunu ləğv etdiyiniz təqdirdə daxil etdiyiniz məlumatlar silinəcək.',
          iconType: 'danger',
          closeButtonText: 'Xeyr, ləğv etmə',
          confirmatioButtonText: 'Bəli, ləğv et',
        },
      });
    toast.showMessage('hello', 'error')
  }
  test = [
    { id: '1', text: 'test1' },
    { id: '2', text: 'test12' },
  ];
}
