import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "textShortener",
})
export class TextShortenerPipe implements PipeTransform {
  transform(text: string, length: number = 10): string {
    return text.length > length ? text.substr(0, length) + "..." : text;
  }
}
