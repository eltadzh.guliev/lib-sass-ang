import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { FileTypePipe } from './file-type.pipe';
import { TextShortenerPipe } from './text-shortener.pipe';

@NgModule({
  declarations: [FileSizePipe, FileTypePipe, TextShortenerPipe],
  exports: [FileSizePipe, FileTypePipe, TextShortenerPipe],
})
export class LibsPipesModule {}
