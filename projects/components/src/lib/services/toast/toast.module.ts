import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ToastSnackbarComponent } from "./components/toast-snackbar/toast-snackbar.component";
import { ToastService } from "./services/toast.service";

@NgModule({
  declarations: [ToastSnackbarComponent],
  imports: [CommonModule, MatSnackBarModule],
  providers: [ToastService],
})
export class ToastModule {}
