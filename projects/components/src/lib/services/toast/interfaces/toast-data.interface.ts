import { ToastType } from "../types/toast.type";

export interface ToastData {
  message: string;
  status: ToastType;
  action?: () => void;
  actionText: string;
}
