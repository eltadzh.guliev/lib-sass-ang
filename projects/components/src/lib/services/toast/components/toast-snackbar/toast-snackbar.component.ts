import { Component, Inject } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';
import { CommonHelperService } from '../../../common.helper';
import { ToastData } from '../../interfaces/toast-data.interface';

@Component({
  selector: 'app-toast-snackbar',
  templateUrl: './toast-snackbar.component.html',
  styleUrls: ['./toast-snackbar.component.sass'],
})
export class ToastSnackbarComponent {
  get messageIsHTML(): boolean {
    return this.commonHelperService.isHTML(this.toastData.message);
  }

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public toastData: ToastData,
    private readonly matSnackBarRef: MatSnackBarRef<ToastSnackbarComponent>,
    private readonly commonHelperService: CommonHelperService
  ) {}

  onActionClick(): void {
    this.toastData?.action();
    this.close();
  }

  close(): void {
    this.matSnackBarRef.dismiss();
  }
}
