import { Injectable } from "@angular/core";
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarRef,
} from "@angular/material/snack-bar";
import { ToastSnackbarComponent } from "../components/toast-snackbar/toast-snackbar.component";
import { ToastData } from "../interfaces/toast-data.interface";
import { ToastType } from "../types/toast.type";

@Injectable({
  providedIn: "root",
})
export class ToastService {
  readonly config: MatSnackBarConfig = {
    duration: 50000,
    horizontalPosition: "right",
    verticalPosition: "bottom",
    panelClass: ["corad-snack-bar-container"],
  };

  constructor(private readonly snackBar: MatSnackBar) {}

  showMessage(
    message: string,
    status: ToastType = "info",
    action?: () => void | null,
    actionText?: string
  ): MatSnackBarRef<ToastSnackbarComponent> {
    const data: ToastData = {
      message,
      status,
      action,
      actionText,
    };
    return this.snackBar.openFromComponent<ToastSnackbarComponent>(
      ToastSnackbarComponent,
      {
        ...this.config,
        panelClass: [...this.config.panelClass, status],
        data,
      }
    );
  }
}
