import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoaderService {
  isLoaderActive: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor() {}

  increaseLoader(): void {
    this.isLoaderActive.next(this.isLoaderActive.value + 1);
  }

  decreaseLoader(): void {
    this.isLoaderActive.next(
      this.isLoaderActive.value > 1 ? this.isLoaderActive.value - 1 : 0
    );
  }
}
