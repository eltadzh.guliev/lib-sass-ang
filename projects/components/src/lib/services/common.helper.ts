import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CommonHelperService {
  constructor() {}

  downloadBLOBWithDispositionName(blob: Blob, headers: HttpHeaders): void {
    const filename = this.getFilenameFromHeader(headers) || 'file';
    this.downloadBLOB(blob, filename);
  }

  getFilenameFromHeader(headers: HttpHeaders): string {
    const contentDisposition = headers.get('Content-Disposition');

    if (contentDisposition) {
      const contentDispositionMatches = contentDisposition.match(
        /.*filename="(.*)"/
      );

      return contentDispositionMatches.length >= 2
        ? contentDispositionMatches[1]
        : '';
    } else {
      return '';
    }
  }

  downloadBLOB(blob: Blob, filename: string): void {
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    // the filename you want
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }

  isHTML(text: string): boolean {
    const doc = new DOMParser().parseFromString(text, 'text/html');
    return Array.from(doc.body.childNodes).some((node) => node.nodeType === 1);
  }

  getRandomNumbers(numbersLength: number): string {
    return Math.floor(
      Math.pow(10, numbersLength - 1) +
        Math.random() * Math.pow(9, numbersLength - 1)
    ).toString();
  }

  blobToDataURL(blob: Blob): Promise<string | ArrayBuffer> {
    return new Promise<string | ArrayBuffer>((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }

  readFileAsText(file: File): Promise<string | ArrayBuffer> {
    return new Promise<string | ArrayBuffer>((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsText(file);
    });
  }
}
