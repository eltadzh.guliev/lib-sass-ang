import { Component, Inject, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ConfirmationWithReasonDialogData } from "../../interfaces/confirmation-with-reason-dialog-data.interface";

@Component({
  selector: "app-confirmation-with-reason",
  templateUrl: "./confirmation-with-reason.component.html",
  styleUrls: ["./confirmation-with-reason.component.sass"],
})
export class ConfirmationWithReasonComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: MatDialogRef<ConfirmationWithReasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationWithReasonDialogData
  ) {}

  ngOnInit(): void {
    this.createFormGroup();
  }

  private createFormGroup(): void {
    this.formGroup = this.fb.group({
      reason: [null],
    });

    const validatorFns: ValidatorFn[] = [];

    if (this.data.isReasonRequired) {
      validatorFns.push(Validators.required);
    }

    if (this.data.maxLength !== undefined) {
      validatorFns.push(Validators.maxLength(this.data.maxLength));
    }

    this.formGroup.get("reason")?.setValidators(validatorFns);
    this.formGroup.get("reason")?.updateValueAndValidity();
  }

  onConfirmClick(): void {
    this.formGroup.markAsTouched();
    if (this.formGroup.invalid) {
      return;
    }

    this.dialogRef.close(this.formGroup.getRawValue().reason);
  }
}
