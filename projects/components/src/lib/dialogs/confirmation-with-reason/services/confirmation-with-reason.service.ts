import { Injectable } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from "@angular/material/dialog";
import { ConfirmationWithReasonComponent } from "../dialogs/confirmation-with-reason/confirmation-with-reason.component";
import { ConfirmationWithReasonDialogData } from "../interfaces/confirmation-with-reason-dialog-data.interface";

@Injectable({
  providedIn: "root",
})
export class ConfirmationWithReasonService {
  private readonly configs: MatDialogConfig = {
    width: "468px",
    data: {
      isReasonRequired: false,
    },
  };
  constructor(private dialog: MatDialog) {}

  open(
    configs: MatDialogConfig = {}
  ): MatDialogRef<ConfirmationWithReasonComponent, ConfirmationWithReasonDialogData> {
    return this.dialog.open(ConfirmationWithReasonComponent, {
      ...this.configs,
      ...configs,
      data: {
        ...this.configs.data,
        ...configs.data,
      },
    });
  }
}
