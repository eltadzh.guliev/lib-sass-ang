import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConfirmationWithReasonComponent } from "./dialogs/confirmation-with-reason/confirmation-with-reason.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ConfirmationWithReasonService } from "./services/confirmation-with-reason.service";
import { MatDialogModule } from "@angular/material/dialog";

@NgModule({
  declarations: [ConfirmationWithReasonComponent],
  imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
  providers: [ConfirmationWithReasonService],
})
export class ConfirmationWithReasonModule {}
