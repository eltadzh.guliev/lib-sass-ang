export interface ConfirmationWithReasonDialogData {
  title: string;
  description: string;
  isReasonRequired: boolean;
  maxLength?: number;
  closeButtonText?: string;
  confirmatioButtonText: string;
}
