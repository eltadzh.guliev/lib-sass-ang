import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationWithIconDialogData } from '../../interfaces/confirmation-with-icon-dialog-data.interface';

@Component({
  selector: 'app-confirmation-with-icon',
  templateUrl: './confirmation-with-icon.component.html',
  styleUrls: ['./confirmation-with-icon.component.sass'],
})
export class ConfirmationWithIconComponent implements OnInit {
  constructor(
    private readonly dialogRef: MatDialogRef<ConfirmationWithIconComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationWithIconDialogData
  ) {}

  ngOnInit(): void {}

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
