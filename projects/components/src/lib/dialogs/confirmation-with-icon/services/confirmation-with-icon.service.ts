import { Injectable } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { ConfirmationWithIconComponent } from '../dialogs/confirmation-with-icon/confirmation-with-icon.component';
import { ConfirmationWithIconDialogData } from '../interfaces/confirmation-with-icon-dialog-data.interface';

@Injectable({
  providedIn: 'root',
})
export class ConfirmationWithIconService {
  private readonly configs: MatDialogConfig = {
    width: '340px',
    panelClass: 'ib-mat-dialog',
  };
  constructor(private dialog: MatDialog) {}

  open(
    configs: MatDialogConfig = {}
  ): MatDialogRef<
    ConfirmationWithIconComponent,
    ConfirmationWithIconDialogData
  > {
    return this.dialog.open(ConfirmationWithIconComponent, {
      ...this.configs,
      ...configs,
      data: {
        ...configs.data,
      },
    });
  }
}
