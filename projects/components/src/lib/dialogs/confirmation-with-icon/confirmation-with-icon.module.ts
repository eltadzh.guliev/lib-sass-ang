import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationWithIconComponent } from './dialogs/confirmation-with-icon/confirmation-with-icon.component';
import { ConfirmationWithIconService } from './services/confirmation-with-icon.service';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [ConfirmationWithIconComponent],
  imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
  providers: [ConfirmationWithIconService],
})
export class ConfirmationWithIconModule {}
