import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsBigComponent } from './components/tabs-big/tabs-big.component';



@NgModule({
  declarations: [
    TabsBigComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TabsBigComponent
  ]
})
export class TabsBigModule { }
