import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tabs-big',
  templateUrl: './tabs-big.component.html',
  styleUrls: ['./tabs-big.component.sass'],
})
export class TabsBigComponent implements OnInit {
  @Input() tabs;
  @Input() selectedId: string;

  @Output() selectedIdChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  onTabClick(id: string): void {
    this.selectedId = id;
    this.selectedIdChange.emit(id);
  }
}
