import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownComponent } from './components/countdown/countdown.component';
import { FormatTimePipe } from './pipes/format-time.pipe';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [CountdownComponent, FormatTimePipe],
  imports: [CommonModule, RoundProgressModule],
  exports: [CountdownComponent],
})
export class CountdownModule {}
