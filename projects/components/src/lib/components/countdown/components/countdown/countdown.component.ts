import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { timer } from 'rxjs';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.sass'],
})
@UntilDestroy()
export class CountdownComponent implements OnInit {
  @Input() set counterMinutes(value: number) {
    this.currentCounterMinutes = value;
    this.maxCounterMinutes = value;
  }

  @Output() timeIsUp = new EventEmitter<void>();

  currentCounterMinutes: number;
  maxCounterMinutes: number;

  constructor() {}

  ngOnInit(): void {
    this.startTick();
  }

  startTick(): void {
    timer(0, 1000)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        if (this.currentCounterMinutes <= 0) {
          this.timeIsUp.emit();
        } else {
          this.currentCounterMinutes--;
        }
      });
  }
}
