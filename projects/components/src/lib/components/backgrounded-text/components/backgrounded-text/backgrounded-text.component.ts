import { Component, HostBinding, Input } from "@angular/core";

@Component({
  selector: "app-backgrounded-text",
  templateUrl: "./backgrounded-text.component.html",
  styleUrls: ["./backgrounded-text.component.sass"],
})
export class BackgroundedTextComponent {
  @Input() text: string;
  @Input() set type(value: "success" | "warning" | "danger") {
    this.innerType = value;

    this.resetTypeClass();
    this.setTypeClass();
  }

  innerType: "success" | "warning" | "danger";

  @HostBinding("class.backgrounded-text_success") successTypeClass: boolean;
  @HostBinding("class.backgrounded-text_warning") warningTypeClass: boolean;
  @HostBinding("class.backgrounded-text_danger") dangerTypeClass: boolean;

  constructor() {}

  private resetTypeClass(): void {
    this.successTypeClass = false;
    this.dangerTypeClass = false;
    this.warningTypeClass = false;
  }

  private setTypeClass(): void {
    switch (this.innerType) {
      case "success":
        this.successTypeClass = true;
        break;
      case "danger":
        this.dangerTypeClass = true;
        break;
      case "warning":
        this.warningTypeClass = true;
    }
  }
}
