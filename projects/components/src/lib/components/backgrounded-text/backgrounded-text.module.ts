import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BackgroundedTextComponent } from "./components/backgrounded-text/backgrounded-text.component";

@NgModule({
  declarations: [BackgroundedTextComponent],
  imports: [CommonModule],
  exports: [BackgroundedTextComponent],
})
export class BackgroundedTextModule {}
