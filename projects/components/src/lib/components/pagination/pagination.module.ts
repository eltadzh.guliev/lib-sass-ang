import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PaginationComponent } from "./components/pagination/pagination.component";
import { PaginationInfoComponent } from "./components/pagination-info/pagination-info.component";

@NgModule({
  declarations: [PaginationComponent, PaginationInfoComponent],
  imports: [CommonModule],
  exports: [PaginationComponent],
})
export class PaginationModule {}
