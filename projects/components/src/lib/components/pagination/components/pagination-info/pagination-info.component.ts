import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-pagination-info",
  templateUrl: "./pagination-info.component.html",
  styleUrls: ["./pagination-info.component.sass"],
})
export class PaginationInfoComponent implements OnInit {
  @Input() pageFrom: number;
  @Input() pageTo: number;
  @Input() totalItemCount: number;

  constructor() {}

  ngOnInit(): void {}
}
