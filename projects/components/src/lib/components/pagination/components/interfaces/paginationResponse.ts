export interface PagenationResponse<T> {
  content: [T];
  number: number;
  size: number;
  totalElements: number;
  totalPages: number;
  last: boolean;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
  sort: Sort;
  pageable?: Pageable;
}
export interface Sort {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}
export interface Pageable {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: Sort;
  unpaged: boolean;
}

export interface ItemsResponse<T> {
  items: T[];
}
