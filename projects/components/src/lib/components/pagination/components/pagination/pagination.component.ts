import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { PagenationResponse } from '../interfaces/paginationResponse';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.sass'],
})
export class PaginationComponent implements OnInit {
  @Input() identifier = 'pagination';
  @Input() pageableData: PagenationResponse<any>;

  @Output() pageChange = new EventEmitter<number>();
  constructor() {}

  ngOnInit(): void {}

  onPageChange(page: number): void {
    this.pageChange.emit(page);
  }
}
