import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-record-column',
  templateUrl: './record-column.component.html',
  styleUrls: ['./record-column.component.sass'],
})
export class RecordColumnComponent implements OnInit {
  @Input() name: string;
  @Input() value: string;

  @Input() valueColorName: 'brand' | 'default' = 'default';

  constructor() {}

  ngOnInit(): void {}
}
