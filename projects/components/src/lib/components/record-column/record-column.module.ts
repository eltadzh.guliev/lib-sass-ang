import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RecordColumnComponent } from './components/record-column/record-column.component';

@NgModule({
  declarations: [RecordColumnComponent],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [RecordColumnComponent],
})
export class RecordColumnModule {}
