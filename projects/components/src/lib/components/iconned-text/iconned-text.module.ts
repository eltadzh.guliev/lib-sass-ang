import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconnedTextComponent } from './components/iconned-text/iconned-text.component';

@NgModule({
  declarations: [IconnedTextComponent],
  imports: [CommonModule],
  exports: [IconnedTextComponent],
})
export class IconnedTextModule {}
