import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-iconned-text',
  templateUrl: './iconned-text.component.html',
  styleUrls: ['./iconned-text.component.sass'],
})
export class IconnedTextComponent {
  @Input() text: string;
  @Input() iconName: string;

  constructor() {}

}
