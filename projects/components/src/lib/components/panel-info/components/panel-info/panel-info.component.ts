import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'app-panel-info',
  templateUrl: './panel-info.component.html',
  styleUrls: ['./panel-info.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PanelInfoComponent implements OnInit {
  @Input() iconName: string;
  @Input() title: string;
  @Input() description: string;

  @Input() set backgroundColorName(backgroundColorName: 'default' | 'brand') {
    this.backgroundColorBrand = backgroundColorName === 'brand';
    this.backgroundColorDefault =
      backgroundColorName === 'default' || !backgroundColorName;
  }

  @Input() isDescriptionHTML = false;

  @HostBinding('class.panel-info_default') backgroundColorDefault = false;
  @HostBinding('class.panel-info_brand') backgroundColorBrand = true;

  constructor() {}

  ngOnInit(): void {}
}
