import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelInfoComponent } from './components/panel-info/panel-info.component';

@NgModule({
  declarations: [PanelInfoComponent],
  imports: [CommonModule],
  exports: [PanelInfoComponent],
})
export class PanelInfoModule {}
