import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FileUploadItemComponent } from './components/file-upload-item/file-upload-item.component';
import { LibsPipesModule } from '../../pipes/libs-pipes.module';
import { FileSizePipe } from '../../pipes/file-size.pipe';
import { ToastModule } from '../../services/toast/toast.module';

@NgModule({
  declarations: [FileUploadComponent, FileUploadItemComponent],
  imports: [CommonModule, LibsPipesModule, ToastModule],
  providers: [FileSizePipe],
  exports: [FileUploadComponent],
})
export class FileUploadModule {}
