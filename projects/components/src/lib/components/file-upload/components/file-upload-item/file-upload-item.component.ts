import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";

@Component({
  selector: "app-file-upload-item",
  templateUrl: "./file-upload-item.component.html",
  styleUrls: ["./file-upload-item.component.sass"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileUploadItemComponent {
  @Input() id: string;
  @Input() name: string;
  @Input() type: string;
  @Input() size: number;

  @Input() hasDownloadButton = true;
  @Input() hasDeleteButton = false;

  @Input() icon: "file_icon" = "file_icon";

  @Output() deleteFile = new EventEmitter<void>();
  @Output() downloadFile = new EventEmitter<void>();

  constructor() {}

  onDeleteFile(): void {
    this.deleteFile.emit();
  }

  onDownloadFile(): void {
    this.downloadFile.emit();
  }
}
