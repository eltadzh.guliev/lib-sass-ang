import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FileSizePipe } from '../../../../pipes/file-size.pipe';
import { ToastService } from '../../../../services/toast/services/toast.service';
import { FileUpload } from '../../interfaces/file-upload';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileUploadComponent {
  @Input() fileUploads!: FileUpload[];
  @Input() maxCount = -1;
  @Input() accept: (
    | ''
    | '.pdf'
    | '.docx'
    | '.doc'
    | '.xlsx'
    | '.xls'
    | '.adoc'
    | '.jpg'
    | '.jpeg'
    | '.png'
  )[] = [''];
  @Input() maxSize = -1;

  @Input() disabled = false;
  @Input() fileLabel = 'Sənəd';

  @Input() hasDownloadButton = true;
  @Input() hasDeleteButton = false;

  @Output() addFile = new EventEmitter<File>();
  @Output() deleteFile = new EventEmitter<string>();
  @Output() downloadFile = new EventEmitter<string>();

  constructor(
    private readonly fileSizePipe: FileSizePipe,
    private readonly toastService: ToastService
  ) {}

  onFileChange(event: Event): void {
    const fileElement: HTMLInputElement = event.target as HTMLInputElement;

    if (fileElement.files && fileElement.files.length > 0) {
      const file = fileElement.files[0];

      if (this.maxSize === -1 || this.maxSize >= file.size) {
        this.addFile.emit(file);
      } else {
        this.toastService.showMessage(
          `Fayl ${this.fileSizePipe.transform(this.maxSize)} həcmindən çoxdur!`,
          'error'
        );
      }

      fileElement.value = '';
    }
  }

  onDeleteFile(id: string): void {
    this.deleteFile.emit(id);
  }

  onDownloadFile(id: string): void {
    this.downloadFile.emit(id);
  }
}
