export interface FileUpload {
  id: string;
  name: string;
  type: string;
  size: number;
  file?: File;
}
