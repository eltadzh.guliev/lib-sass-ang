import * as i0 from '@angular/core';
import { EventEmitter, Component, Input, Output, NgModule, ChangeDetectionStrategy, HostBinding, Pipe, Injectable, Inject } from '@angular/core';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i1$4 from '@angular/forms';
import { ReactiveFormsModule, Validators } from '@angular/forms';
import * as i1$1 from '@angular/material/snack-bar';
import { MAT_SNACK_BAR_DATA, MatSnackBarModule } from '@angular/material/snack-bar';
import { __decorate } from 'tslib';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { timer, BehaviorSubject } from 'rxjs';
import * as i1$2 from 'angular-svg-round-progressbar';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import * as i1$3 from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

class TabsSmallComponent {
    constructor() {
        this.selectedIdChange = new EventEmitter();
    }
    ngOnInit() { }
    onTabClick(id) {
        this.selectedId = id;
        this.selectedIdChange.emit(id);
    }
}
TabsSmallComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TabsSmallComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: TabsSmallComponent, selector: "app-tabs-small", inputs: { tabs: "tabs", selectedId: "selectedId" }, outputs: { selectedIdChange: "selectedIdChange" }, ngImport: i0, template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-small__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:inline-flex;align-items:center;overflow:auto;padding:2px;border-radius:12px;background-color:#f5f5f5}:host .tabs-small__button{display:block;padding:12px 24px;font-size:14px;font-weight:500;line-height:20px;border-radius:12px;color:#222;cursor:pointer;background-color:unset;border:none;margin-right:4px}:host .tabs-small__button:last-child{margin-right:0}:host .tabs-small__button:hover{background-color:#fbfbfb}:host .tabs-small__button.active{background-color:#fff}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-tabs-small', template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-small__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:inline-flex;align-items:center;overflow:auto;padding:2px;border-radius:12px;background-color:#f5f5f5}:host .tabs-small__button{display:block;padding:12px 24px;font-size:14px;font-weight:500;line-height:20px;border-radius:12px;color:#222;cursor:pointer;background-color:unset;border:none;margin-right:4px}:host .tabs-small__button:last-child{margin-right:0}:host .tabs-small__button:hover{background-color:#fbfbfb}:host .tabs-small__button.active{background-color:#fff}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { tabs: [{
                type: Input
            }], selectedId: [{
                type: Input
            }], selectedIdChange: [{
                type: Output
            }] } });

class TabsSmallModule {
}
TabsSmallModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
TabsSmallModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, declarations: [TabsSmallComponent], imports: [CommonModule], exports: [TabsSmallComponent] });
TabsSmallModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TabsSmallComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        TabsSmallComponent
                    ]
                }]
        }] });

class TabsBigComponent {
    constructor() {
        this.selectedIdChange = new EventEmitter();
    }
    ngOnInit() { }
    onTabClick(id) {
        this.selectedId = id;
        this.selectedIdChange.emit(id);
    }
}
TabsBigComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TabsBigComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: TabsBigComponent, selector: "app-tabs-big", inputs: { tabs: "tabs", selectedId: "selectedId" }, outputs: { selectedIdChange: "selectedIdChange" }, ngImport: i0, template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-big__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center;flex-wrap:wrap}:host .tabs-big__button{display:block;font-size:20px;font-weight:600;line-height:32px;color:#999;cursor:pointer;background-color:unset;border:none;margin-right:24px}:host .tabs-big__button:last-child{margin-right:0}:host .tabs-big__button:hover{color:#838282}:host .tabs-big__button.active{color:#222}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-tabs-big', template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-big__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center;flex-wrap:wrap}:host .tabs-big__button{display:block;font-size:20px;font-weight:600;line-height:32px;color:#999;cursor:pointer;background-color:unset;border:none;margin-right:24px}:host .tabs-big__button:last-child{margin-right:0}:host .tabs-big__button:hover{color:#838282}:host .tabs-big__button.active{color:#222}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { tabs: [{
                type: Input
            }], selectedId: [{
                type: Input
            }], selectedIdChange: [{
                type: Output
            }] } });

class TabsBigModule {
}
TabsBigModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
TabsBigModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TabsBigModule, declarations: [TabsBigComponent], imports: [CommonModule], exports: [TabsBigComponent] });
TabsBigModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TabsBigComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        TabsBigComponent
                    ]
                }]
        }] });

class RecordColumnComponent {
    constructor() {
        this.valueColorName = 'default';
    }
    ngOnInit() { }
}
RecordColumnComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
RecordColumnComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: RecordColumnComponent, selector: "app-record-column", inputs: { name: "name", value: "value", valueColorName: "valueColorName" }, ngImport: i0, template: "<span class=\"record-column__name\">{{ name }}</span>\r\n<span\r\n  *ngIf=\"value != undefined\"\r\n  class=\"record-column__value\"\r\n  [ngClass]=\"{\r\n    'record-column__value_default': valueColorName === 'default',\r\n    'record-column__value_brand': valueColorName === 'brand'\r\n  }\"\r\n  >{{ value }}</span\r\n>\r\n\r\n<ng-content *ngIf=\"value == undefined\"></ng-content>\r\n", styles: [":host .record-column__name{display:block;font-weight:400;font-size:14px;line-height:24px;color:#a0a0a0}:host .record-column__value{display:block;font-weight:400;font-size:14px;line-height:24px}:host .record-column__value.record-column__value_default{color:#222}:host .record-column__value.record-column__value_brand{color:#e66e4b}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-record-column', template: "<span class=\"record-column__name\">{{ name }}</span>\r\n<span\r\n  *ngIf=\"value != undefined\"\r\n  class=\"record-column__value\"\r\n  [ngClass]=\"{\r\n    'record-column__value_default': valueColorName === 'default',\r\n    'record-column__value_brand': valueColorName === 'brand'\r\n  }\"\r\n  >{{ value }}</span\r\n>\r\n\r\n<ng-content *ngIf=\"value == undefined\"></ng-content>\r\n", styles: [":host .record-column__name{display:block;font-weight:400;font-size:14px;line-height:24px;color:#a0a0a0}:host .record-column__value{display:block;font-weight:400;font-size:14px;line-height:24px}:host .record-column__value.record-column__value_default{color:#222}:host .record-column__value.record-column__value_brand{color:#e66e4b}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { name: [{
                type: Input
            }], value: [{
                type: Input
            }], valueColorName: [{
                type: Input
            }] } });

class RecordColumnModule {
}
RecordColumnModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
RecordColumnModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, declarations: [RecordColumnComponent], imports: [CommonModule, ReactiveFormsModule], exports: [RecordColumnComponent] });
RecordColumnModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, imports: [CommonModule, ReactiveFormsModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [RecordColumnComponent],
                    imports: [CommonModule, ReactiveFormsModule],
                    exports: [RecordColumnComponent],
                }]
        }] });

class PanelInfoComponent {
    set backgroundColorName(backgroundColorName) {
        this.backgroundColorBrand = backgroundColorName === 'brand';
        this.backgroundColorDefault =
            backgroundColorName === 'default' || !backgroundColorName;
    }
    constructor() {
        this.isDescriptionHTML = false;
        this.backgroundColorDefault = false;
        this.backgroundColorBrand = true;
    }
    ngOnInit() { }
}
PanelInfoComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PanelInfoComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: PanelInfoComponent, selector: "app-panel-info", inputs: { iconName: "iconName", title: "title", description: "description", backgroundColorName: "backgroundColorName", isDescriptionHTML: "isDescriptionHTML" }, host: { properties: { "class.panel-info_default": "this.backgroundColorDefault", "class.panel-info_brand": "this.backgroundColorBrand" } }, ngImport: i0, template: "<img src=\"./assets/icons/{{ iconName }}.svg\" class=\"panel-info__img\" />\r\n\r\n<span class=\"panel-info__title\">{{ title }}</span>\r\n\r\n<span *ngIf=\"!isDescriptionHTML\" class=\"panel-info__description\">{{\r\n  description\r\n}}</span>\r\n\r\n<div\r\n  *ngIf=\"isDescriptionHTML\"\r\n  [innerHTML]=\"description\"\r\n  class=\"panel-info__html-description\"\r\n></div>\r\n", styles: [":host{display:block;padding:16px 40px 24px;border-radius:12px}:host.panel-info_default{background-color:#f7f7f7}:host.panel-info_brand{background-color:#e66e4b1a}:host .panel-info__img{display:block;width:24px;height:24px;margin-bottom:16px}:host .panel-info__title{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;margin-bottom:10px}:host .panel-info__description{display:block;font-size:14px;font-weight:400;line-height:20px;color:#222}:host .panel-info__html-description{font-size:14px}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], changeDetection: i0.ChangeDetectionStrategy.OnPush });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-panel-info', changeDetection: ChangeDetectionStrategy.OnPush, template: "<img src=\"./assets/icons/{{ iconName }}.svg\" class=\"panel-info__img\" />\r\n\r\n<span class=\"panel-info__title\">{{ title }}</span>\r\n\r\n<span *ngIf=\"!isDescriptionHTML\" class=\"panel-info__description\">{{\r\n  description\r\n}}</span>\r\n\r\n<div\r\n  *ngIf=\"isDescriptionHTML\"\r\n  [innerHTML]=\"description\"\r\n  class=\"panel-info__html-description\"\r\n></div>\r\n", styles: [":host{display:block;padding:16px 40px 24px;border-radius:12px}:host.panel-info_default{background-color:#f7f7f7}:host.panel-info_brand{background-color:#e66e4b1a}:host .panel-info__img{display:block;width:24px;height:24px;margin-bottom:16px}:host .panel-info__title{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;margin-bottom:10px}:host .panel-info__description{display:block;font-size:14px;font-weight:400;line-height:20px;color:#222}:host .panel-info__html-description{font-size:14px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { iconName: [{
                type: Input
            }], title: [{
                type: Input
            }], description: [{
                type: Input
            }], backgroundColorName: [{
                type: Input
            }], isDescriptionHTML: [{
                type: Input
            }], backgroundColorDefault: [{
                type: HostBinding,
                args: ['class.panel-info_default']
            }], backgroundColorBrand: [{
                type: HostBinding,
                args: ['class.panel-info_brand']
            }] } });

class PanelInfoModule {
}
PanelInfoModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
PanelInfoModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, declarations: [PanelInfoComponent], imports: [CommonModule], exports: [PanelInfoComponent] });
PanelInfoModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [PanelInfoComponent],
                    imports: [CommonModule],
                    exports: [PanelInfoComponent],
                }]
        }] });

class PaginationInfoComponent {
    constructor() { }
    ngOnInit() { }
}
PaginationInfoComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationInfoComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PaginationInfoComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: PaginationInfoComponent, selector: "app-pagination-info", inputs: { pageFrom: "pageFrom", pageTo: "pageTo", totalItemCount: "totalItemCount" }, ngImport: i0, template: "{{pageFrom}}-{{pageTo}} {{totalItemCount}}-dan", styles: [":host{font-size:14px;line-height:24px;color:#000}\n"] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationInfoComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-pagination-info", template: "{{pageFrom}}-{{pageTo}} {{totalItemCount}}-dan", styles: [":host{font-size:14px;line-height:24px;color:#000}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { pageFrom: [{
                type: Input
            }], pageTo: [{
                type: Input
            }], totalItemCount: [{
                type: Input
            }] } });

class PaginationComponent {
    constructor() {
        this.identifier = 'pagination';
        this.pageChange = new EventEmitter();
    }
    ngOnInit() { }
    onPageChange(page) {
        this.pageChange.emit(page);
    }
}
PaginationComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PaginationComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: PaginationComponent, selector: "app-pagination", inputs: { identifier: "identifier", pageableData: "pageableData" }, outputs: { pageChange: "pageChange" }, ngImport: i0, template: "<app-pagination-info\r\n  class=\"mr-50\"\r\n  [pageFrom]=\"\r\n    pageableData.numberOfElements > 0\r\n      ? pageableData.number * pageableData.size + 1\r\n      : 0\r\n  \"\r\n  [pageTo]=\"\r\n    pageableData.number * pageableData.size + pageableData.numberOfElements\r\n  \"\r\n  [totalItemCount]=\"pageableData.totalElements\"\r\n></app-pagination-info>\r\n<button\r\n  id=\"{{ identifier }}-left-button\"\r\n  class=\"pagination-button pagination-button_icon-after pagination-button_icon-after_left mr-10\"\r\n  [attr.disabled]=\"pageableData.first ? true : null\"\r\n  [ngClass]=\"{ disabled: pageableData.first }\"\r\n  (click)=\"onPageChange(pageableData.number - 1)\"\r\n></button>\r\n<button\r\n  id=\"{{ identifier }}-right-button\"\r\n  class=\"pagination-button pagination-button_icon-after pagination-button_icon-after_right\"\r\n  [attr.disabled]=\"pageableData.last ? true : null\"\r\n  [ngClass]=\"{\r\n    disabled: pageableData.last\r\n  }\"\r\n  (click)=\"onPageChange(pageableData.number + 1)\"\r\n></button>\r\n", styles: [":host{display:inline-flex;align-items:center}.pagination-button{display:flex;align-items:center;justify-content:center;width:44px;height:44px;background-color:#f5f5f5;border-radius:8px;border:none;cursor:pointer}.pagination-button:hover{background-color:#f3f2f2}.pagination-button:active{background-color:#eaeaea}.pagination-button.pagination-button_icon-after:after{content:\"\";display:block;background-repeat:no-repeat;background-position:center;background-size:contain}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_right:after{width:8px;height:12px;background-image:url(/assets/icons/arrow_right_black.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_right.disabled:after{background-image:url(/assets/icons/arrow-right-grey.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_left:after{width:8px;height:12px;background-image:url(/assets/icons/arrow_left_black.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_left.disabled:after{background-image:url(/assets/icons/arrow_left_grey.svg)}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "component", type: PaginationInfoComponent, selector: "app-pagination-info", inputs: ["pageFrom", "pageTo", "totalItemCount"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-pagination', template: "<app-pagination-info\r\n  class=\"mr-50\"\r\n  [pageFrom]=\"\r\n    pageableData.numberOfElements > 0\r\n      ? pageableData.number * pageableData.size + 1\r\n      : 0\r\n  \"\r\n  [pageTo]=\"\r\n    pageableData.number * pageableData.size + pageableData.numberOfElements\r\n  \"\r\n  [totalItemCount]=\"pageableData.totalElements\"\r\n></app-pagination-info>\r\n<button\r\n  id=\"{{ identifier }}-left-button\"\r\n  class=\"pagination-button pagination-button_icon-after pagination-button_icon-after_left mr-10\"\r\n  [attr.disabled]=\"pageableData.first ? true : null\"\r\n  [ngClass]=\"{ disabled: pageableData.first }\"\r\n  (click)=\"onPageChange(pageableData.number - 1)\"\r\n></button>\r\n<button\r\n  id=\"{{ identifier }}-right-button\"\r\n  class=\"pagination-button pagination-button_icon-after pagination-button_icon-after_right\"\r\n  [attr.disabled]=\"pageableData.last ? true : null\"\r\n  [ngClass]=\"{\r\n    disabled: pageableData.last\r\n  }\"\r\n  (click)=\"onPageChange(pageableData.number + 1)\"\r\n></button>\r\n", styles: [":host{display:inline-flex;align-items:center}.pagination-button{display:flex;align-items:center;justify-content:center;width:44px;height:44px;background-color:#f5f5f5;border-radius:8px;border:none;cursor:pointer}.pagination-button:hover{background-color:#f3f2f2}.pagination-button:active{background-color:#eaeaea}.pagination-button.pagination-button_icon-after:after{content:\"\";display:block;background-repeat:no-repeat;background-position:center;background-size:contain}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_right:after{width:8px;height:12px;background-image:url(/assets/icons/arrow_right_black.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_right.disabled:after{background-image:url(/assets/icons/arrow-right-grey.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_left:after{width:8px;height:12px;background-image:url(/assets/icons/arrow_left_black.svg)}.pagination-button.pagination-button_icon-after.pagination-button_icon-after_left.disabled:after{background-image:url(/assets/icons/arrow_left_grey.svg)}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { identifier: [{
                type: Input
            }], pageableData: [{
                type: Input
            }], pageChange: [{
                type: Output
            }] } });

class PaginationModule {
}
PaginationModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
PaginationModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: PaginationModule, declarations: [PaginationComponent, PaginationInfoComponent], imports: [CommonModule], exports: [PaginationComponent] });
PaginationModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [PaginationComponent, PaginationInfoComponent],
                    imports: [CommonModule],
                    exports: [PaginationComponent],
                }]
        }] });

class IconnedTextComponent {
    constructor() { }
}
IconnedTextComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
IconnedTextComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: IconnedTextComponent, selector: "app-iconned-text", inputs: { text: "text", iconName: "iconName" }, ngImport: i0, template: "<img\r\n  class=\"iconned-text__img\"\r\n  src=\"/assets/icons/{{ iconName }}.svg\"\r\n  alt=\"{{ iconName }}\"\r\n/>\r\n{{ text }}\r\n", styles: [":host{display:flex;align-items:center;font-size:12px;font-weight:400;line-height:18px;color:#222}:host .iconned-text__img{margin-right:12px}\n"] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-iconned-text', template: "<img\r\n  class=\"iconned-text__img\"\r\n  src=\"/assets/icons/{{ iconName }}.svg\"\r\n  alt=\"{{ iconName }}\"\r\n/>\r\n{{ text }}\r\n", styles: [":host{display:flex;align-items:center;font-size:12px;font-weight:400;line-height:18px;color:#222}:host .iconned-text__img{margin-right:12px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { text: [{
                type: Input
            }], iconName: [{
                type: Input
            }] } });

class IconnedTextModule {
}
IconnedTextModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
IconnedTextModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, declarations: [IconnedTextComponent], imports: [CommonModule], exports: [IconnedTextComponent] });
IconnedTextModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [IconnedTextComponent],
                    imports: [CommonModule],
                    exports: [IconnedTextComponent],
                }]
        }] });

class FileSizePipe {
    transform(size) {
        return (size / (1024 * 1024)).toFixed(2) + "MB";
    }
}
FileSizePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileSizePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
FileSizePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FileSizePipe, name: "fileSize" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileSizePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: "fileSize",
                }]
        }] });

class CommonHelperService {
    constructor() { }
    downloadBLOBWithDispositionName(blob, headers) {
        const filename = this.getFilenameFromHeader(headers) || 'file';
        this.downloadBLOB(blob, filename);
    }
    getFilenameFromHeader(headers) {
        const contentDisposition = headers.get('Content-Disposition');
        if (contentDisposition) {
            const contentDispositionMatches = contentDisposition.match(/.*filename="(.*)"/);
            return contentDispositionMatches.length >= 2
                ? contentDispositionMatches[1]
                : '';
        }
        else {
            return '';
        }
    }
    downloadBLOB(blob, filename) {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
    isHTML(text) {
        const doc = new DOMParser().parseFromString(text, 'text/html');
        return Array.from(doc.body.childNodes).some((node) => node.nodeType === 1);
    }
    getRandomNumbers(numbersLength) {
        return Math.floor(Math.pow(10, numbersLength - 1) +
            Math.random() * Math.pow(9, numbersLength - 1)).toString();
    }
    blobToDataURL(blob) {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        });
    }
    readFileAsText(file) {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsText(file);
        });
    }
}
CommonHelperService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
CommonHelperService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }], ctorParameters: function () { return []; } });

class ToastSnackbarComponent {
    get messageIsHTML() {
        return this.commonHelperService.isHTML(this.toastData.message);
    }
    constructor(toastData, matSnackBarRef, commonHelperService) {
        this.toastData = toastData;
        this.matSnackBarRef = matSnackBarRef;
        this.commonHelperService = commonHelperService;
    }
    onActionClick() {
        this.toastData?.action();
        this.close();
    }
    close() {
        this.matSnackBarRef.dismiss();
    }
}
ToastSnackbarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastSnackbarComponent, deps: [{ token: MAT_SNACK_BAR_DATA }, { token: i1$1.MatSnackBarRef }, { token: CommonHelperService }], target: i0.ɵɵFactoryTarget.Component });
ToastSnackbarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: ToastSnackbarComponent, selector: "app-toast-snackbar", ngImport: i0, template: "<img\r\n  src=\"/assets/icons/toast_{{ toastData.status }}.svg\"\r\n  class=\"toast-snackbar__icon\"\r\n  alt=\"{{ toastData.status }}\"\r\n/>\r\n<ng-container [ngSwitch]=\"messageIsHTML\">\r\n  <span class=\"toast-snackbar__message\" *ngSwitchCase=\"false\">\r\n    {{ toastData.message }}\r\n  </span>\r\n  <div class=\"toast-snackbar__message\" *ngSwitchDefault [innerHTML]=\"toastData.message\"></div>\r\n</ng-container>\r\n<button\r\n  *ngIf=\"!toastData.action\"\r\n  class=\"toast-snackbar__close\"\r\n  (click)=\"close()\"\r\n></button>\r\n<button\r\n  *ngIf=\"toastData.action\"\r\n  class=\"toast-snackbar__action\"\r\n  (click)=\"onActionClick()\"\r\n>\r\n  {{ toastData.actionText }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center}:host .toast-snackbar__icon{display:block;width:24px;height:24px;margin-right:10px}:host .toast-snackbar__message{font-size:16px;font-weight:400;line-height:24px;margin-right:10px}:host .toast-snackbar__close{display:inline-flex;align-items:center;height:44px;flex-shrink:0;margin-left:auto;cursor:pointer;background-color:unset;border:none}:host .toast-snackbar__close:before{content:\"\";display:block;width:24px;height:24px;background-image:url(/assets/icons/toast_close.svg);background-repeat:no-repeat;background-size:contain}:host .toast-snackbar__action{display:inline-block;border-radius:8px;background-color:#ffffff1a;color:#fff;padding:10px 16px;font-size:14px;font-weight:500;line-height:20px;flex-shrink:0;margin-left:auto;cursor:pointer;border:none}:host .toast-snackbar__action:hover{background-color:#fff3}:host .toast-snackbar__action:active{background-color:#ffffff4d}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastSnackbarComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-toast-snackbar', template: "<img\r\n  src=\"/assets/icons/toast_{{ toastData.status }}.svg\"\r\n  class=\"toast-snackbar__icon\"\r\n  alt=\"{{ toastData.status }}\"\r\n/>\r\n<ng-container [ngSwitch]=\"messageIsHTML\">\r\n  <span class=\"toast-snackbar__message\" *ngSwitchCase=\"false\">\r\n    {{ toastData.message }}\r\n  </span>\r\n  <div class=\"toast-snackbar__message\" *ngSwitchDefault [innerHTML]=\"toastData.message\"></div>\r\n</ng-container>\r\n<button\r\n  *ngIf=\"!toastData.action\"\r\n  class=\"toast-snackbar__close\"\r\n  (click)=\"close()\"\r\n></button>\r\n<button\r\n  *ngIf=\"toastData.action\"\r\n  class=\"toast-snackbar__action\"\r\n  (click)=\"onActionClick()\"\r\n>\r\n  {{ toastData.actionText }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center}:host .toast-snackbar__icon{display:block;width:24px;height:24px;margin-right:10px}:host .toast-snackbar__message{font-size:16px;font-weight:400;line-height:24px;margin-right:10px}:host .toast-snackbar__close{display:inline-flex;align-items:center;height:44px;flex-shrink:0;margin-left:auto;cursor:pointer;background-color:unset;border:none}:host .toast-snackbar__close:before{content:\"\";display:block;width:24px;height:24px;background-image:url(/assets/icons/toast_close.svg);background-repeat:no-repeat;background-size:contain}:host .toast-snackbar__action{display:inline-block;border-radius:8px;background-color:#ffffff1a;color:#fff;padding:10px 16px;font-size:14px;font-weight:500;line-height:20px;flex-shrink:0;margin-left:auto;cursor:pointer;border:none}:host .toast-snackbar__action:hover{background-color:#fff3}:host .toast-snackbar__action:active{background-color:#ffffff4d}\n"] }]
        }], ctorParameters: function () { return [{ type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_SNACK_BAR_DATA]
                }] }, { type: i1$1.MatSnackBarRef }, { type: CommonHelperService }]; } });

class ToastService {
    constructor(snackBar) {
        this.snackBar = snackBar;
        this.config = {
            duration: 50000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
            panelClass: ["corad-snack-bar-container"],
        };
    }
    showMessage(message, status = "info", action, actionText) {
        const data = {
            message,
            status,
            action,
            actionText,
        };
        return this.snackBar.openFromComponent(ToastSnackbarComponent, {
            ...this.config,
            panelClass: [...this.config.panelClass, status],
            data,
        });
    }
}
ToastService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, deps: [{ token: i1$1.MatSnackBar }], target: i0.ɵɵFactoryTarget.Injectable });
ToastService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, providedIn: "root" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: "root",
                }]
        }], ctorParameters: function () { return [{ type: i1$1.MatSnackBar }]; } });

class FileTypePipe {
    transform(type, filename) {
        if ([
            'pdf',
            'png',
            'jpeg',
            'jpg',
            'adoc',
            'docx',
            'xlsx',
            'xls',
            'adoc',
        ].includes(type)) {
            return type;
        }
        else {
            if (!type) {
                const splittedFilename = filename.split('.');
                return splittedFilename.length > 1
                    ? splittedFilename[splittedFilename.length - 1]
                    : 'file';
            }
            else {
                const splittedType = type.split('/');
                if (splittedType[0] === 'application') {
                    return this.getFileTypeByApplicationType(type);
                }
                else if (splittedType.length >= 2) {
                    return splittedType[1];
                }
                else {
                    return 'file';
                }
            }
        }
    }
    getFileTypeByApplicationType(type) {
        const allTypes = {
            'application/vnd.lotus-1-2-3': '123',
            'application/vnd.hzn-3d-crossword': 'x3d',
            'video/3gpp': '3gp',
            'video/3gpp2': '3g2',
            'application/vnd.mseq': 'mseq',
            'application/vnd.3m.post-it-notes': 'pwn',
            'application/vnd.3gpp.pic-bw-large': 'plb',
            'application/vnd.3gpp.pic-bw-small': 'psb',
            'application/vnd.3gpp.pic-bw-var': 'pvb',
            'application/vnd.3gpp2.tcap': 'tcap',
            'application/x-7z-compressed': '7z',
            'application/x-abiword': 'abw',
            'application/x-ace-compressed': 'ace',
            'application/vnd.americandynamics.acc': 'acc',
            'application/vnd.acucobol': 'acu',
            'application/vnd.acucorp': 'atc',
            'audio/adpcm': 'adp',
            'application/x-authorware-bin': 'aab',
            'application/x-authorware-map': 'aam',
            'application/x-authorware-seg': 'aas',
            'application/vnd.adobe.air-application-installer-package+zip': 'air',
            'application/x-shockwave-flash': 'swf',
            'application/vnd.adobe.fxp': 'fxp',
            'application/pdf': 'pdf',
            'application/vnd.cups-ppd': 'ppd',
            'application/x-director': 'dir',
            'application/vnd.adobe.xdp+xml': 'xdp',
            'application/vnd.adobe.xfdf': 'xfdf',
            'audio/x-aac': 'aac',
            'application/vnd.ahead.space': 'ahead',
            'application/vnd.airzip.filesecure.azf': 'azf',
            'application/vnd.airzip.filesecure.azs': 'azs',
            'application/vnd.amazon.ebook': 'azw',
            'application/vnd.amiga.ami': 'ami',
            'application/andrew-inset': 'N/A',
            'application/vnd.android.package-archive': 'apk',
            'application/vnd.anser-web-certificate-issue-initiation': 'cii',
            'application/vnd.anser-web-funds-transfer-initiation': 'fti',
            'application/vnd.antix.game-component': 'atx',
            'application/x-apple-diskimage': 'dmg',
            'application/vnd.apple.installer+xml': 'mpkg',
            'application/applixware': 'aw',
            'audio/mpeg': 'mpga',
            'application/vnd.hhe.lesson-player': 'les',
            'application/vnd.aristanetworks.swi': 'swi',
            'text/x-asm': 's',
            'application/atomcat+xml': 'atomcat',
            'application/atomsvc+xml': 'atomsvc',
            'application/atom+xml': 'atom',
            'application/pkix-attr-cert': 'ac',
            'audio/x-aiff': 'aif',
            'video/x-msvideo': 'avi',
            'application/vnd.audiograph': 'aep',
            'image/vnd.dxf': 'dxf',
            'model/vnd.dwf': 'dwf',
            'text/plain-bas': 'par',
            'application/x-bcpio': 'bcpio',
            'application/octet-stream': 'bin',
            'image/bmp': 'bmp',
            'application/x-bittorrent': 'torrent',
            'application/vnd.rim.cod': 'cod',
            'application/vnd.blueice.multipass': 'mpm',
            'application/vnd.bmi': 'bmi',
            'application/x-sh': 'sh',
            'image/prs.btif': 'btif',
            'application/vnd.businessobjects': 'rep',
            'application/x-bzip': 'bz',
            'application/x-bzip2': 'bz2',
            'application/x-csh': 'csh',
            'text/x-c': 'c',
            'application/vnd.chemdraw+xml': 'cdxml',
            'text/css': 'css',
            'chemical/x-cdx': 'cdx',
            'chemical/x-cml': 'cml',
            'chemical/x-csml': 'csml',
            'application/vnd.contact.cmsg': 'cdbcmsg',
            'application/vnd.claymore': 'cla',
            'application/vnd.clonk.c4group': 'c4g',
            'image/vnd.dvb.subtitle': 'sub',
            'application/cdmi-capability': 'cdmia',
            'application/cdmi-container': 'cdmic',
            'application/cdmi-domain': 'cdmid',
            'application/cdmi-object': 'cdmio',
            'application/cdmi-queue': 'cdmiq',
            'application/vnd.cluetrust.cartomobile-config': 'c11amc',
            'application/vnd.cluetrust.cartomobile-config-pkg': 'c11amz',
            'image/x-cmu-raster': 'ras',
            'model/vnd.collada+xml': 'dae',
            'text/csv': 'csv',
            'application/mac-compactpro': 'cpt',
            'application/vnd.wap.wmlc': 'wmlc',
            'image/cgm': 'cgm',
            'x-conference/x-cooltalk': 'ice',
            'image/x-cmx': 'cmx',
            'application/vnd.xara': 'xar',
            'application/vnd.cosmocaller': 'cmc',
            'application/x-cpio': 'cpio',
            'application/vnd.crick.clicker': 'clkx',
            'application/vnd.crick.clicker.keyboard': 'clkk',
            'application/vnd.crick.clicker.palette': 'clkp',
            'application/vnd.crick.clicker.template': 'clkt',
            'application/vnd.crick.clicker.wordbank': 'clkw',
            'application/vnd.criticaltools.wbs+xml': 'wbs',
            'application/vnd.rig.cryptonote': 'cryptonote',
            'chemical/x-cif': 'cif',
            'chemical/x-cmdf': 'cmdf',
            'application/cu-seeme': 'cu',
            'application/prs.cww': 'cww',
            'text/vnd.curl': 'curl',
            'text/vnd.curl.dcurl': 'dcurl',
            'text/vnd.curl.mcurl': 'mcurl',
            'text/vnd.curl.scurl': 'scurl',
            'application/vnd.curl.car': 'car',
            'application/vnd.curl.pcurl': 'pcurl',
            'application/vnd.yellowriver-custom-menu': 'cmp',
            'application/dssc+der': 'dssc',
            'application/dssc+xml': 'xdssc',
            'application/x-debian-package': 'deb',
            'audio/vnd.dece.audio': 'uva',
            'image/vnd.dece.graphic': 'uvi',
            'video/vnd.dece.hd': 'uvh',
            'video/vnd.dece.mobile': 'uvm',
            'video/vnd.uvvu.mp4': 'uvu',
            'video/vnd.dece.pd': 'uvp',
            'video/vnd.dece.sd': 'uvs',
            'video/vnd.dece.video': 'uvv',
            'application/x-dvi': 'dvi',
            'application/vnd.fdsn.seed': 'seed',
            'application/x-dtbook+xml': 'dtb',
            'application/x-dtbresource+xml': 'res',
            'application/vnd.dvb.ait': 'ait',
            'application/vnd.dvb.service': 'svc',
            'audio/vnd.digital-winds': 'eol',
            'image/vnd.djvu': 'djvu',
            'application/xml-dtd': 'dtd',
            'application/vnd.dolby.mlp': 'mlp',
            'application/x-doom': 'wad',
            'application/vnd.dpgraph': 'dpg',
            'audio/vnd.dra': 'dra',
            'application/vnd.dreamfactory': 'dfac',
            'audio/vnd.dts': 'dts',
            'audio/vnd.dts.hd': 'dtshd',
            'image/vnd.dwg': 'dwg',
            'application/vnd.dynageo': 'geo',
            'application/ecmascript': 'es',
            'application/vnd.ecowin.chart': 'mag',
            'image/vnd.fujixerox.edmics-mmr': 'mmr',
            'image/vnd.fujixerox.edmics-rlc': 'rlc',
            'application/exi': 'exi',
            'application/vnd.proteus.magazine': 'mgz',
            'application/epub+zip': 'epub',
            'message/rfc822': 'eml',
            'application/vnd.enliven': 'nml',
            'application/vnd.is-xpr': 'xpr',
            'image/vnd.xiff': 'xif',
            'application/vnd.xfdl': 'xfdl',
            'application/emma+xml': 'emma',
            'application/vnd.ezpix-album': 'ez2',
            'application/vnd.ezpix-package': 'ez3',
            'image/vnd.fst': 'fst',
            'video/vnd.fvt': 'fvt',
            'image/vnd.fastbidsheet': 'fbs',
            'application/vnd.denovo.fcselayout-link': 'fe_launch',
            'video/x-f4v': 'f4v',
            'video/x-flv': 'flv',
            'image/vnd.fpx': 'fpx',
            'image/vnd.net-fpx': 'npx',
            'text/vnd.fmi.flexstor': 'flx',
            'video/x-fli': 'fli',
            'application/vnd.fluxtime.clip': 'ftc',
            'application/vnd.fdf': 'fdf',
            'text/x-fortran': 'f',
            'application/vnd.mif': 'mif',
            'application/vnd.framemaker': 'fm',
            'image/x-freehand': 'fh',
            'application/vnd.fsc.weblaunch': 'fsc',
            'application/vnd.frogans.fnc': 'fnc',
            'application/vnd.frogans.ltf': 'ltf',
            'application/vnd.fujixerox.ddd': 'ddd',
            'application/vnd.fujixerox.docuworks': 'xdw',
            'application/vnd.fujixerox.docuworks.binder': 'xbd',
            'application/vnd.fujitsu.oasys': 'oas',
            'application/vnd.fujitsu.oasys2': 'oa2',
            'application/vnd.fujitsu.oasys3': 'oa3',
            'application/vnd.fujitsu.oasysgp': 'fg5',
            'application/vnd.fujitsu.oasysprs': 'bh2',
            'application/x-futuresplash': 'spl',
            'application/vnd.fuzzysheet': 'fzs',
            'image/g3fax': 'g3',
            'application/vnd.gmx': 'gmx',
            'model/vnd.gtw': 'gtw',
            'application/vnd.genomatix.tuxedo': 'txd',
            'application/vnd.geogebra.file': 'ggb',
            'application/vnd.geogebra.tool': 'ggt',
            'model/vnd.gdl': 'gdl',
            'application/vnd.geometry-explorer': 'gex',
            'application/vnd.geonext': 'gxt',
            'application/vnd.geoplan': 'g2w',
            'application/vnd.geospace': 'g3w',
            'application/x-font-ghostscript': 'gsf',
            'application/x-font-bdf': 'bdf',
            'application/x-gtar': 'gtar',
            'application/x-texinfo': 'texinfo',
            'application/x-gnumeric': 'gnumeric',
            'application/vnd.google-earth.kml+xml': 'kml',
            'application/vnd.google-earth.kmz': 'kmz',
            'application/vnd.grafeq': 'gqf',
            'image/gif': 'gif',
            'text/vnd.graphviz': 'gv',
            'application/vnd.groove-account': 'gac',
            'application/vnd.groove-help': 'ghf',
            'application/vnd.groove-identity-message': 'gim',
            'application/vnd.groove-injector': 'grv',
            'application/vnd.groove-tool-message': 'gtm',
            'application/vnd.groove-tool-template': 'tpl',
            'application/vnd.groove-vcard': 'vcg',
            'video/h261': 'h261',
            'video/h263': 'h263',
            'video/h264': 'h264',
            'application/vnd.hp-hpid': 'hpid',
            'application/vnd.hp-hps': 'hps',
            'application/x-hdf': 'hdf',
            'audio/vnd.rip': 'rip',
            'application/vnd.hbci': 'hbci',
            'application/vnd.hp-jlyt': 'jlt',
            'application/vnd.hp-pcl': 'pcl',
            'application/vnd.hp-hpgl': 'hpgl',
            'application/vnd.yamaha.hv-script': 'hvs',
            'application/vnd.yamaha.hv-dic': 'hvd',
            'application/vnd.yamaha.hv-voice': 'hvp',
            'application/vnd.hydrostatix.sof-data': 'sfd-hdstx',
            'application/hyperstudio': 'stk',
            'application/vnd.hal+xml': 'hal',
            'text/html': 'html',
            'application/vnd.ibm.rights-management': 'irm',
            'application/vnd.ibm.secure-container': 'sc',
            'text/calendar': 'ics',
            'application/vnd.iccprofile': 'icc',
            'image/x-icon': 'ico',
            'application/vnd.igloader': 'igl',
            'image/ief': 'ief',
            'application/vnd.immervision-ivp': 'ivp',
            'application/vnd.immervision-ivu': 'ivu',
            'application/reginfo+xml': 'rif',
            'text/vnd.in3d.3dml': '3dml',
            'text/vnd.in3d.spot': 'spot',
            'model/iges': 'igs',
            'application/vnd.intergeo': 'i2g',
            'application/vnd.cinderella': 'cdy',
            'application/vnd.intercon.formnet': 'xpw',
            'application/vnd.isac.fcs': 'fcs',
            'application/ipfix': 'ipfix',
            'application/pkix-cert': 'cer',
            'application/pkixcmp': 'pki',
            'application/pkix-crl': 'crl',
            'application/pkix-pkipath': 'pkipath',
            'application/vnd.insors.igm': 'igm',
            'application/vnd.ipunplugged.rcprofile': 'rcprofile',
            'application/vnd.irepository.package+xml': 'irp',
            'text/vnd.sun.j2me.app-descriptor': 'jad',
            'application/java-archive': 'jar',
            'application/java-vm': 'class',
            'application/x-java-jnlp-file': 'jnlp',
            'application/java-serialized-object': 'ser',
            'text/x-java-source,java': 'java',
            'application/javascript': 'js',
            'application/json': 'json',
            'application/vnd.joost.joda-archive': 'joda',
            'video/jpm': 'jpm',
            'image/x-citrix-jpeg': 'jpg',
            'image/pjpeg': 'pjpeg',
            'video/jpeg': 'jpgv',
            'application/vnd.kahootz': 'ktz',
            'application/vnd.chipnuts.karaoke-mmd': 'mmd',
            'application/vnd.kde.karbon': 'karbon',
            'application/vnd.kde.kchart': 'chrt',
            'application/vnd.kde.kformula': 'kfo',
            'application/vnd.kde.kivio': 'flw',
            'application/vnd.kde.kontour': 'kon',
            'application/vnd.kde.kpresenter': 'kpr',
            'application/vnd.kde.kspread': 'ksp',
            'application/vnd.kde.kword': 'kwd',
            'application/vnd.kenameaapp': 'htke',
            'application/vnd.kidspiration': 'kia',
            'application/vnd.kinar': 'kne',
            'application/vnd.kodak-descriptor': 'sse',
            'application/vnd.las.las+xml': 'lasxml',
            'application/x-latex': 'latex',
            'application/vnd.llamagraphics.life-balance.desktop': 'lbd',
            'application/vnd.llamagraphics.life-balance.exchange+xml': 'lbe',
            'application/vnd.jam': 'jam',
            'application/vnd.lotus-approach': 'apr',
            'application/vnd.lotus-freelance': 'pre',
            'application/vnd.lotus-notes': 'nsf',
            'application/vnd.lotus-organizer': 'org',
            'application/vnd.lotus-screencam': 'scm',
            'application/vnd.lotus-wordpro': 'lwp',
            'audio/vnd.lucent.voice': 'lvp',
            'audio/x-mpegurl': 'm3u',
            'video/x-m4v': 'm4v',
            'application/mac-binhex40': 'hqx',
            'application/vnd.macports.portpkg': 'portpkg',
            'application/vnd.osgeo.mapguide.package': 'mgp',
            'application/marc': 'mrc',
            'application/marcxml+xml': 'mrcx',
            'application/mxf': 'mxf',
            'application/vnd.wolfram.player': 'nbp',
            'application/mathematica': 'ma',
            'application/mathml+xml': 'mathml',
            'application/mbox': 'mbox',
            'application/vnd.medcalcdata': 'mc1',
            'application/mediaservercontrol+xml': 'mscml',
            'application/vnd.mediastation.cdkey': 'cdkey',
            'application/vnd.mfer': 'mwf',
            'application/vnd.mfmp': 'mfm',
            'model/mesh': 'msh',
            'application/mads+xml': 'mads',
            'application/mets+xml': 'mets',
            'application/mods+xml': 'mods',
            'application/metalink4+xml': 'meta4',
            'application/vnd.mcd': 'mcd',
            'application/vnd.micrografx.flo': 'flo',
            'application/vnd.micrografx.igx': 'igx',
            'application/vnd.eszigno3+xml': 'es3',
            'application/x-msaccess': 'mdb',
            'video/x-ms-asf': 'asf',
            'application/x-msdownload': 'exe',
            'application/vnd.ms-artgalry': 'cil',
            'application/vnd.ms-cab-compressed': 'cab',
            'application/vnd.ms-ims': 'ims',
            'application/x-ms-application': 'application',
            'application/x-msclip': 'clp',
            'image/vnd.ms-modi': 'mdi',
            'application/vnd.ms-fontobject': 'eot',
            'application/vnd.ms-excel': 'xls',
            'application/vnd.ms-excel.addin.macroenabled.12': 'xlam',
            'application/vnd.ms-excel.sheet.binary.macroenabled.12': 'xlsb',
            'application/vnd.ms-excel.template.macroenabled.12': 'xltm',
            'application/vnd.ms-excel.sheet.macroenabled.12': 'xlsm',
            'application/vnd.ms-htmlhelp': 'chm',
            'application/x-mscardfile': 'crd',
            'application/vnd.ms-lrm': 'lrm',
            'application/x-msmediaview': 'mvb',
            'application/x-msmoney': 'mny',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
            'application/vnd.openxmlformats-officedocument.presentationml.slide': 'sldx',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow': 'ppsx',
            'application/vnd.openxmlformats-officedocument.presentationml.template': 'potx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template': 'xltx',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template': 'dotx',
            'application/x-msbinder': 'obd',
            'application/vnd.ms-officetheme': 'thmx',
            'application/onenote': 'onetoc',
            'audio/vnd.ms-playready.media.pya': 'pya',
            'video/vnd.ms-playready.media.pyv': 'pyv',
            'application/vnd.ms-powerpoint': 'ppt',
            'application/vnd.ms-powerpoint.addin.macroenabled.12': 'ppam',
            'application/vnd.ms-powerpoint.slide.macroenabled.12': 'sldm',
            'application/vnd.ms-powerpoint.presentation.macroenabled.12': 'pptm',
            'application/vnd.ms-powerpoint.slideshow.macroenabled.12': 'ppsm',
            'application/vnd.ms-powerpoint.template.macroenabled.12': 'potm',
            'application/vnd.ms-project': 'mpp',
            'application/x-mspublisher': 'pub',
            'application/x-msschedule': 'scd',
            'application/x-silverlight-app': 'xap',
            'application/vnd.ms-pki.stl': 'stl',
            'application/vnd.ms-pki.seccat': 'cat',
            'application/vnd.visio': 'vsd',
            'application/vnd.visio2013': 'vsdx',
            'video/x-ms-wm': 'wm',
            'audio/x-ms-wma': 'wma',
            'audio/x-ms-wax': 'wax',
            'video/x-ms-wmx': 'wmx',
            'application/x-ms-wmd': 'wmd',
            'application/vnd.ms-wpl': 'wpl',
            'application/x-ms-wmz': 'wmz',
            'video/x-ms-wmv': 'wmv',
            'video/x-ms-wvx': 'wvx',
            'application/x-msmetafile': 'wmf',
            'application/x-msterminal': 'trm',
            'application/msword': 'doc',
            'application/vnd.ms-word.document.macroenabled.12': 'docm',
            'application/vnd.ms-word.template.macroenabled.12': 'dotm',
            'application/x-mswrite': 'wri',
            'application/vnd.ms-works': 'wps',
            'application/x-ms-xbap': 'xbap',
            'application/vnd.ms-xpsdocument': 'xps',
            'audio/midi': 'mid',
            'application/vnd.ibm.minipay': 'mpy',
            'application/vnd.ibm.modcap': 'afp',
            'application/vnd.jcp.javame.midlet-rms': 'rms',
            'application/vnd.tmobile-livetv': 'tmo',
            'application/x-mobipocket-ebook': 'prc',
            'application/vnd.mobius.mbk': 'mbk',
            'application/vnd.mobius.dis': 'dis',
            'application/vnd.mobius.plc': 'plc',
            'application/vnd.mobius.mqy': 'mqy',
            'application/vnd.mobius.msl': 'msl',
            'application/vnd.mobius.txf': 'txf',
            'application/vnd.mobius.daf': 'daf',
            'text/vnd.fly': 'fly',
            'application/vnd.mophun.certificate': 'mpc',
            'application/vnd.mophun.application': 'mpn',
            'video/mj2': 'mj2',
            'video/vnd.mpegurl': 'mxu',
            'video/mpeg': 'mpeg',
            'application/mp21': 'm21',
            'audio/mp4': 'mp4a',
            'video/mp4': 'mp4',
            'application/vnd.apple.mpegurl': 'm3u8',
            'application/vnd.musician': 'mus',
            'application/vnd.muvee.style': 'msty',
            'application/xv+xml': 'mxml',
            'application/vnd.nokia.n-gage.data': 'ngdat',
            'application/vnd.nokia.n-gage.symbian.install': 'n-gage',
            'application/x-dtbncx+xml': 'ncx',
            'application/x-netcdf': 'nc',
            'application/vnd.neurolanguage.nlu': 'nlu',
            'application/vnd.dna': 'dna',
            'application/vnd.noblenet-directory': 'nnd',
            'application/vnd.noblenet-sealer': 'nns',
            'application/vnd.noblenet-web': 'nnw',
            'application/vnd.nokia.radio-preset': 'rpst',
            'application/vnd.nokia.radio-presets': 'rpss',
            'text/n3': 'n3',
            'application/vnd.novadigm.edm': 'edm',
            'application/vnd.novadigm.edx': 'edx',
            'application/vnd.novadigm.ext': 'ext',
            'application/vnd.flographit': 'gph',
            'audio/vnd.nuera.ecelp4800': 'ecelp4800',
            'audio/vnd.nuera.ecelp7470': 'ecelp7470',
            'audio/vnd.nuera.ecelp9600': 'ecelp9600',
            'application/oda': 'oda',
            'application/ogg': 'ogx',
            'audio/ogg': 'oga',
            'video/ogg': 'ogv',
            'application/vnd.oma.dd2+xml': 'dd2',
            'application/vnd.oasis.opendocument.text-web': 'oth',
            'application/oebps-package+xml': 'opf',
            'application/vnd.intu.qbo': 'qbo',
            'application/vnd.openofficeorg.extension': 'oxt',
            'application/vnd.yamaha.openscoreformat': 'osf',
            'audio/webm': 'weba',
            'video/webm': 'webm',
            'application/vnd.oasis.opendocument.chart': 'odc',
            'application/vnd.oasis.opendocument.chart-template': 'otc',
            'application/vnd.oasis.opendocument.database': 'odb',
            'application/vnd.oasis.opendocument.formula': 'odf',
            'application/vnd.oasis.opendocument.formula-template': 'odft',
            'application/vnd.oasis.opendocument.graphics': 'odg',
            'application/vnd.oasis.opendocument.graphics-template': 'otg',
            'application/vnd.oasis.opendocument.image': 'odi',
            'application/vnd.oasis.opendocument.image-template': 'oti',
            'application/vnd.oasis.opendocument.presentation': 'odp',
            'application/vnd.oasis.opendocument.presentation-template': 'otp',
            'application/vnd.oasis.opendocument.spreadsheet': 'ods',
            'application/vnd.oasis.opendocument.spreadsheet-template': 'ots',
            'application/vnd.oasis.opendocument.text': 'odt',
            'application/vnd.oasis.opendocument.text-master': 'odm',
            'application/vnd.oasis.opendocument.text-template': 'ott',
            'image/ktx': 'ktx',
            'application/vnd.sun.xml.calc': 'sxc',
            'application/vnd.sun.xml.calc.template': 'stc',
            'application/vnd.sun.xml.draw': 'sxd',
            'application/vnd.sun.xml.draw.template': 'std',
            'application/vnd.sun.xml.impress': 'sxi',
            'application/vnd.sun.xml.impress.template': 'sti',
            'application/vnd.sun.xml.math': 'sxm',
            'application/vnd.sun.xml.writer': 'sxw',
            'application/vnd.sun.xml.writer.global': 'sxg',
            'application/vnd.sun.xml.writer.template': 'stw',
            'application/x-font-otf': 'otf',
            'application/vnd.yamaha.openscoreformat.osfpvg+xml': 'osfpvg',
            'application/vnd.osgi.dp': 'dp',
            'application/vnd.palm': 'pdb',
            'text/x-pascal': 'p',
            'application/vnd.pawaafile': 'paw',
            'application/vnd.hp-pclxl': 'pclxl',
            'application/vnd.picsel': 'efif',
            'image/x-pcx': 'pcx',
            'image/vnd.adobe.photoshop': 'psd',
            'application/pics-rules': 'prf',
            'image/x-pict': 'pic',
            'application/x-chat': 'chat',
            'application/pkcs10': 'p10',
            'application/x-pkcs12': 'p12',
            'application/pkcs7-mime': 'p7m',
            'application/pkcs7-signature': 'p7s',
            'application/x-pkcs7-certreqresp': 'p7r',
            'application/x-pkcs7-certificates': 'p7b',
            'application/pkcs8': 'p8',
            'application/vnd.pocketlearn': 'plf',
            'image/x-portable-anymap': 'pnm',
            'image/x-portable-bitmap': 'pbm',
            'application/x-font-pcf': 'pcf',
            'application/font-tdpfr': 'pfr',
            'application/x-chess-pgn': 'pgn',
            'image/x-portable-graymap': 'pgm',
            'image/x-png': 'png',
            'image/x-portable-pixmap': 'ppm',
            'application/pskc+xml': 'pskcxml',
            'application/vnd.ctc-posml': 'pml',
            'application/postscript': 'ai',
            'application/x-font-type1': 'pfa',
            'application/vnd.powerbuilder6': 'pbd',
            'application/pgp-signature': 'pgp',
            'application/vnd.previewsystems.box': 'box',
            'application/vnd.pvi.ptid1': 'ptid',
            'application/pls+xml': 'pls',
            'application/vnd.pg.format': 'str',
            'application/vnd.pg.osasli': 'ei6',
            'text/prs.lines.tag': 'dsc',
            'application/x-font-linux-psf': 'psf',
            'application/vnd.publishare-delta-tree': 'qps',
            'application/vnd.pmi.widget': 'wg',
            'application/vnd.quark.quarkxpress': 'qxd',
            'application/vnd.epson.esf': 'esf',
            'application/vnd.epson.msf': 'msf',
            'application/vnd.epson.ssf': 'ssf',
            'application/vnd.epson.quickanime': 'qam',
            'application/vnd.intu.qfx': 'qfx',
            'video/quicktime': 'qt',
            'application/x-rar-compressed': 'rar',
            'audio/x-pn-realaudio': 'ram',
            'audio/x-pn-realaudio-plugin': 'rmp',
            'application/rsd+xml': 'rsd',
            'application/vnd.rn-realmedia': 'rm',
            'application/vnd.realvnc.bed': 'bed',
            'application/vnd.recordare.musicxml': 'mxl',
            'application/vnd.recordare.musicxml+xml': 'musicxml',
            'application/relax-ng-compact-syntax': 'rnc',
            'application/vnd.data-vision.rdz': 'rdz',
            'application/rdf+xml': 'rdf',
            'application/vnd.cloanto.rp9': 'rp9',
            'application/vnd.jisp': 'jisp',
            'application/rtf': 'rtf',
            'text/richtext': 'rtx',
            'application/vnd.route66.link66+xml': 'link66',
            'application/rss+xml': 'rss',
            'application/shf+xml': 'shf',
            'application/vnd.sailingtracker.track': 'st',
            'image/svg+xml': 'svg',
            'application/vnd.sus-calendar': 'sus',
            'application/sru+xml': 'sru',
            'application/set-payment-initiation': 'setpay',
            'application/set-registration-initiation': 'setreg',
            'application/vnd.sema': 'sema',
            'application/vnd.semd': 'semd',
            'application/vnd.semf': 'semf',
            'application/vnd.seemail': 'see',
            'application/x-font-snf': 'snf',
            'application/scvp-vp-request': 'spq',
            'application/scvp-vp-response': 'spp',
            'application/scvp-cv-request': 'scq',
            'application/scvp-cv-response': 'scs',
            'application/sdp': 'sdp',
            'text/x-setext': 'etx',
            'video/x-sgi-movie': 'movie',
            'application/vnd.shana.informed.formdata': 'ifm',
            'application/vnd.shana.informed.formtemplate': 'itp',
            'application/vnd.shana.informed.interchange': 'iif',
            'application/vnd.shana.informed.package': 'ipk',
            'application/thraud+xml': 'tfi',
            'application/x-shar': 'shar',
            'image/x-rgb': 'rgb',
            'application/vnd.epson.salt': 'slt',
            'application/vnd.accpac.simply.aso': 'aso',
            'application/vnd.accpac.simply.imp': 'imp',
            'application/vnd.simtech-mindmapper': 'twd',
            'application/vnd.commonspace': 'csp',
            'application/vnd.yamaha.smaf-audio': 'saf',
            'application/vnd.smaf': 'mmf',
            'application/vnd.yamaha.smaf-phrase': 'spf',
            'application/vnd.smart.teacher': 'teacher',
            'application/vnd.svd': 'svd',
            'application/sparql-query': 'rq',
            'application/sparql-results+xml': 'srx',
            'application/srgs': 'gram',
            'application/srgs+xml': 'grxml',
            'application/ssml+xml': 'ssml',
            'application/vnd.koan': 'skp',
            'text/sgml': 'sgml',
            'application/vnd.stardivision.calc': 'sdc',
            'application/vnd.stardivision.draw': 'sda',
            'application/vnd.stardivision.impress': 'sdd',
            'application/vnd.stardivision.math': 'smf',
            'application/vnd.stardivision.writer': 'sdw',
            'application/vnd.stardivision.writer-global': 'sgl',
            'application/vnd.stepmania.stepchart': 'sm',
            'application/x-stuffit': 'sit',
            'application/x-stuffitx': 'sitx',
            'application/vnd.solent.sdkm+xml': 'sdkm',
            'application/vnd.olpc-sugar': 'xo',
            'audio/basic': 'au',
            'application/vnd.wqd': 'wqd',
            'application/vnd.symbian.install': 'sis',
            'application/smil+xml': 'smi',
            'application/vnd.syncml+xml': 'xsm',
            'application/vnd.syncml.dm+wbxml': 'bdm',
            'application/vnd.syncml.dm+xml': 'xdm',
            'application/x-sv4cpio': 'sv4cpio',
            'application/x-sv4crc': 'sv4crc',
            'application/sbml+xml': 'sbml',
            'text/tab-separated-values': 'tsv',
            'image/tiff': 'tiff',
            'application/vnd.tao.intent-module-archive': 'tao',
            'application/x-tar': 'tar',
            'application/x-tcl': 'tcl',
            'application/x-tex': 'tex',
            'application/x-tex-tfm': 'tfm',
            'application/tei+xml': 'tei',
            'text/plain': 'txt',
            'application/vnd.spotfire.dxp': 'dxp',
            'application/vnd.spotfire.sfs': 'sfs',
            'application/timestamped-data': 'tsd',
            'application/vnd.trid.tpt': 'tpt',
            'application/vnd.triscape.mxs': 'mxs',
            'text/troff': 't',
            'application/vnd.trueapp': 'tra',
            'application/x-font-ttf': 'ttf',
            'text/turtle': 'ttl',
            'application/vnd.umajin': 'umj',
            'application/vnd.uoml+xml': 'uoml',
            'application/vnd.unity': 'unityweb',
            'application/vnd.ufdl': 'ufd',
            'text/uri-list': 'uri',
            'application/vnd.uiq.theme': 'utz',
            'application/x-ustar': 'ustar',
            'text/x-uuencode': 'uu',
            'text/x-vcalendar': 'vcs',
            'text/x-vcard': 'vcf',
            'application/x-cdlink': 'vcd',
            'application/vnd.vsf': 'vsf',
            'model/vrml': 'wrl',
            'application/vnd.vcx': 'vcx',
            'model/vnd.mts': 'mts',
            'model/vnd.vtu': 'vtu',
            'application/vnd.visionary': 'vis',
            'video/vnd.vivo': 'viv',
            'application/ccxml+xml,': 'ccxml',
            'application/voicexml+xml': 'vxml',
            'application/x-wais-source': 'src',
            'application/vnd.wap.wbxml': 'wbxml',
            'image/vnd.wap.wbmp': 'wbmp',
            'audio/x-wav': 'wav',
            'application/davmount+xml': 'davmount',
            'application/x-font-woff': 'woff',
            'application/wspolicy+xml': 'wspolicy',
            'image/webp': 'webp',
            'application/vnd.webturbo': 'wtb',
            'application/widget': 'wgt',
            'application/winhlp': 'hlp',
            'text/vnd.wap.wml': 'wml',
            'text/vnd.wap.wmlscript': 'wmls',
            'application/vnd.wap.wmlscriptc': 'wmlsc',
            'application/vnd.wordperfect': 'wpd',
            'application/vnd.wt.stf': 'stf',
            'application/wsdl+xml': 'wsdl',
            'image/x-xbitmap': 'xbm',
            'image/x-xpixmap': 'xpm',
            'image/x-xwindowdump': 'xwd',
            'application/x-x509-ca-cert': 'der',
            'application/x-xfig': 'fig',
            'application/xhtml+xml': 'xhtml',
            'application/xml': 'xml',
            'application/xcap-diff+xml': 'xdf',
            'application/xenc+xml': 'xenc',
            'application/patch-ops-error+xml': 'xer',
            'application/resource-lists+xml': 'rl',
            'application/rls-services+xml': 'rs',
            'application/resource-lists-diff+xml': 'rld',
            'application/xslt+xml': 'xslt',
            'application/xop+xml': 'xop',
            'application/x-xpinstall': 'xpi',
            'application/xspf+xml': 'xspf',
            'application/vnd.mozilla.xul+xml': 'xul',
            'chemical/x-xyz': 'xyz',
            'text/yaml': 'yaml',
            'application/yang': 'yang',
            'application/yin+xml': 'yin',
            'application/vnd.zul': 'zir',
            'application/zip': 'zip',
            'application/vnd.handheld-entertainment+xml': 'zmm',
            'application/vnd.zzazz.deck+xml': 'zaz',
        };
        return allTypes[type] ? allTypes[type] : 'file';
    }
}
FileTypePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileTypePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
FileTypePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FileTypePipe, name: "fileType" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileTypePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'fileType',
                }]
        }] });

class TextShortenerPipe {
    transform(text, length = 10) {
        return text.length > length ? text.substr(0, length) + "..." : text;
    }
}
TextShortenerPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
TextShortenerPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, name: "textShortener" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: "textShortener",
                }]
        }] });

class FileUploadItemComponent {
    constructor() {
        this.hasDownloadButton = true;
        this.hasDeleteButton = false;
        this.icon = "file_icon";
        this.deleteFile = new EventEmitter();
        this.downloadFile = new EventEmitter();
    }
    onDeleteFile() {
        this.deleteFile.emit();
    }
    onDownloadFile() {
        this.downloadFile.emit();
    }
}
FileUploadItemComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadItemComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
FileUploadItemComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: FileUploadItemComponent, selector: "app-file-upload-item", inputs: { id: "id", name: "name", type: "type", size: "size", hasDownloadButton: "hasDownloadButton", hasDeleteButton: "hasDeleteButton", icon: "icon" }, outputs: { deleteFile: "deleteFile", downloadFile: "downloadFile" }, ngImport: i0, template: "<img src=\"/assets/icons/file_icon.svg\" class=\"file-upload-item__img\" alt=\"\" />\r\n\r\n<div class=\"file-upload-item__info\">\r\n  <span class=\"file-upload-item__name\" [title]=\"name\">{{\r\n    name | textShortener: 24\r\n  }}</span>\r\n  <span class=\"file-upload-item__description\"\r\n    >{{ size | fileSize }} &nbsp; &nbsp;\r\n    {{ type | fileType: name | uppercase }}</span\r\n  >\r\n</div>\r\n\r\n<div class=\"file-upload-item-actions\">\r\n  <button\r\n    *ngIf=\"hasDownloadButton\"\r\n    class=\"file-upload-item-actions__button file-upload-item-actions__button_download\"\r\n    (click)=\"onDownloadFile()\"\r\n  ></button>\r\n  <button\r\n    *ngIf=\"hasDeleteButton\"\r\n    class=\"file-upload-item-actions__button file-upload-item-actions__button_delete\"\r\n    (click)=\"onDeleteFile()\"\r\n  ></button>\r\n</div>\r\n", styles: [":host{display:flex;align-items:center;padding:18px 16px;background-color:#f7f7f7;border-radius:8px}:host .file-upload-item__img{display:block;width:32px;height:32px;margin-right:12px}:host .file-upload-item__info .file-upload-item__name{display:block;font-size:14px;font-weight:600;line-height:20px;color:#222;word-break:break-word}:host .file-upload-item__info .file-upload-item__description{display:block;font-size:11px;font-weight:500;line-height:14px;letter-spacing:.011px;color:#a0a0a0}:host .file-upload-item-actions{display:flex;align-items:center;margin-left:auto}:host .file-upload-item-actions .file-upload-item-actions__button{display:block;width:24px;height:24px;background-repeat:no-repeat;background-size:contain;margin-right:8px;border:none;cursor:pointer;background-color:unset}:host .file-upload-item-actions .file-upload-item-actions__button:last-child{margin-right:0}:host .file-upload-item-actions .file-upload-item-actions__button.file-upload-item-actions__button_download{background-image:url(/assets/icons/eye_brand.svg)}:host .file-upload-item-actions .file-upload-item-actions__button.file-upload-item-actions__button_delete{background-image:url(/assets/icons/trash_brand.svg)}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "pipe", type: i1.UpperCasePipe, name: "uppercase" }, { kind: "pipe", type: FileSizePipe, name: "fileSize" }, { kind: "pipe", type: FileTypePipe, name: "fileType" }, { kind: "pipe", type: TextShortenerPipe, name: "textShortener" }], changeDetection: i0.ChangeDetectionStrategy.OnPush });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadItemComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-file-upload-item", changeDetection: ChangeDetectionStrategy.OnPush, template: "<img src=\"/assets/icons/file_icon.svg\" class=\"file-upload-item__img\" alt=\"\" />\r\n\r\n<div class=\"file-upload-item__info\">\r\n  <span class=\"file-upload-item__name\" [title]=\"name\">{{\r\n    name | textShortener: 24\r\n  }}</span>\r\n  <span class=\"file-upload-item__description\"\r\n    >{{ size | fileSize }} &nbsp; &nbsp;\r\n    {{ type | fileType: name | uppercase }}</span\r\n  >\r\n</div>\r\n\r\n<div class=\"file-upload-item-actions\">\r\n  <button\r\n    *ngIf=\"hasDownloadButton\"\r\n    class=\"file-upload-item-actions__button file-upload-item-actions__button_download\"\r\n    (click)=\"onDownloadFile()\"\r\n  ></button>\r\n  <button\r\n    *ngIf=\"hasDeleteButton\"\r\n    class=\"file-upload-item-actions__button file-upload-item-actions__button_delete\"\r\n    (click)=\"onDeleteFile()\"\r\n  ></button>\r\n</div>\r\n", styles: [":host{display:flex;align-items:center;padding:18px 16px;background-color:#f7f7f7;border-radius:8px}:host .file-upload-item__img{display:block;width:32px;height:32px;margin-right:12px}:host .file-upload-item__info .file-upload-item__name{display:block;font-size:14px;font-weight:600;line-height:20px;color:#222;word-break:break-word}:host .file-upload-item__info .file-upload-item__description{display:block;font-size:11px;font-weight:500;line-height:14px;letter-spacing:.011px;color:#a0a0a0}:host .file-upload-item-actions{display:flex;align-items:center;margin-left:auto}:host .file-upload-item-actions .file-upload-item-actions__button{display:block;width:24px;height:24px;background-repeat:no-repeat;background-size:contain;margin-right:8px;border:none;cursor:pointer;background-color:unset}:host .file-upload-item-actions .file-upload-item-actions__button:last-child{margin-right:0}:host .file-upload-item-actions .file-upload-item-actions__button.file-upload-item-actions__button_download{background-image:url(/assets/icons/eye_brand.svg)}:host .file-upload-item-actions .file-upload-item-actions__button.file-upload-item-actions__button_delete{background-image:url(/assets/icons/trash_brand.svg)}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { id: [{
                type: Input
            }], name: [{
                type: Input
            }], type: [{
                type: Input
            }], size: [{
                type: Input
            }], hasDownloadButton: [{
                type: Input
            }], hasDeleteButton: [{
                type: Input
            }], icon: [{
                type: Input
            }], deleteFile: [{
                type: Output
            }], downloadFile: [{
                type: Output
            }] } });

class FileUploadComponent {
    constructor(fileSizePipe, toastService) {
        this.fileSizePipe = fileSizePipe;
        this.toastService = toastService;
        this.maxCount = -1;
        this.accept = [''];
        this.maxSize = -1;
        this.disabled = false;
        this.fileLabel = 'Sənəd';
        this.hasDownloadButton = true;
        this.hasDeleteButton = false;
        this.addFile = new EventEmitter();
        this.deleteFile = new EventEmitter();
        this.downloadFile = new EventEmitter();
    }
    onFileChange(event) {
        const fileElement = event.target;
        if (fileElement.files && fileElement.files.length > 0) {
            const file = fileElement.files[0];
            if (this.maxSize === -1 || this.maxSize >= file.size) {
                this.addFile.emit(file);
            }
            else {
                this.toastService.showMessage(`Fayl ${this.fileSizePipe.transform(this.maxSize)} həcmindən çoxdur!`, 'error');
            }
            fileElement.value = '';
        }
    }
    onDeleteFile(id) {
        this.deleteFile.emit(id);
    }
    onDownloadFile(id) {
        this.downloadFile.emit(id);
    }
}
FileUploadComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadComponent, deps: [{ token: FileSizePipe }, { token: ToastService }], target: i0.ɵɵFactoryTarget.Component });
FileUploadComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: FileUploadComponent, selector: "app-file-upload", inputs: { fileUploads: "fileUploads", maxCount: "maxCount", accept: "accept", maxSize: "maxSize", disabled: "disabled", fileLabel: "fileLabel", hasDownloadButton: "hasDownloadButton", hasDeleteButton: "hasDeleteButton" }, outputs: { addFile: "addFile", deleteFile: "deleteFile", downloadFile: "downloadFile" }, ngImport: i0, template: "<input\r\n  #file\r\n  type=\"file\"\r\n  hidden\r\n  (change)=\"onFileChange($event)\"\r\n  [accept]=\"accept.join(',')\"\r\n/>\r\n\r\n<button\r\n  class=\"file-upload-add-button\"\r\n  [disabled]=\"\r\n    disabled || (maxCount > -1 && fileUploads.length >= maxCount) ? true : null\r\n  \"\r\n  (click)=\"file.click()\"\r\n>\r\n  <span class=\"file-upload-add-button__doc-text\">{{ fileLabel }}</span>\r\n  <span class=\"file-upload-add-button__add-text\">+ Y\u00FCkl\u0259</span>\r\n</button>\r\n\r\n<app-file-upload-item\r\n  *ngFor=\"let file of fileUploads\"\r\n  [name]=\"file.name\"\r\n  [type]=\"file.type\"\r\n  [size]=\"file.size\"\r\n  [hasDownloadButton]=\"hasDownloadButton\"\r\n  [hasDeleteButton]=\"hasDeleteButton\"\r\n  (deleteFile)=\"onDeleteFile(file.id)\"\r\n  (downloadFile)=\"onDownloadFile(file.id)\"\r\n></app-file-upload-item>\r\n", styles: [":host{display:block;max-width:376px;padding:16px;border-radius:8px;border:1px solid #E2E2E2}:host>*{margin-bottom:10px}:host>*:last-child{margin-bottom:0}:host .file-upload-add-button{display:flex;align-items:center;justify-content:space-between;width:100%;padding:24px 16px;background-color:#f7f7f7;border-radius:8px;border:none;cursor:pointer}:host .file-upload-add-button:hover{background-color:#f2efef}:host .file-upload-add-button:active{background-color:#e6e3e3}:host .file-upload-add-button:disabled{cursor:not-allowed;background-color:#f7f7f7!important}:host .file-upload-add-button:disabled .file-upload-add-button__doc-text{color:#939393}:host .file-upload-add-button:disabled .file-upload-add-button__add-text{color:#ffbba8}:host .file-upload-add-button .file-upload-add-button__doc-text{font-size:14px;font-weight:600;line-height:20px;color:#222}:host .file-upload-add-button .file-upload-add-button__add-text{font-size:14px;font-weight:600;line-height:20px;color:#e66e4b}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "component", type: FileUploadItemComponent, selector: "app-file-upload-item", inputs: ["id", "name", "type", "size", "hasDownloadButton", "hasDeleteButton", "icon"], outputs: ["deleteFile", "downloadFile"] }], changeDetection: i0.ChangeDetectionStrategy.OnPush });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-file-upload', changeDetection: ChangeDetectionStrategy.OnPush, template: "<input\r\n  #file\r\n  type=\"file\"\r\n  hidden\r\n  (change)=\"onFileChange($event)\"\r\n  [accept]=\"accept.join(',')\"\r\n/>\r\n\r\n<button\r\n  class=\"file-upload-add-button\"\r\n  [disabled]=\"\r\n    disabled || (maxCount > -1 && fileUploads.length >= maxCount) ? true : null\r\n  \"\r\n  (click)=\"file.click()\"\r\n>\r\n  <span class=\"file-upload-add-button__doc-text\">{{ fileLabel }}</span>\r\n  <span class=\"file-upload-add-button__add-text\">+ Y\u00FCkl\u0259</span>\r\n</button>\r\n\r\n<app-file-upload-item\r\n  *ngFor=\"let file of fileUploads\"\r\n  [name]=\"file.name\"\r\n  [type]=\"file.type\"\r\n  [size]=\"file.size\"\r\n  [hasDownloadButton]=\"hasDownloadButton\"\r\n  [hasDeleteButton]=\"hasDeleteButton\"\r\n  (deleteFile)=\"onDeleteFile(file.id)\"\r\n  (downloadFile)=\"onDownloadFile(file.id)\"\r\n></app-file-upload-item>\r\n", styles: [":host{display:block;max-width:376px;padding:16px;border-radius:8px;border:1px solid #E2E2E2}:host>*{margin-bottom:10px}:host>*:last-child{margin-bottom:0}:host .file-upload-add-button{display:flex;align-items:center;justify-content:space-between;width:100%;padding:24px 16px;background-color:#f7f7f7;border-radius:8px;border:none;cursor:pointer}:host .file-upload-add-button:hover{background-color:#f2efef}:host .file-upload-add-button:active{background-color:#e6e3e3}:host .file-upload-add-button:disabled{cursor:not-allowed;background-color:#f7f7f7!important}:host .file-upload-add-button:disabled .file-upload-add-button__doc-text{color:#939393}:host .file-upload-add-button:disabled .file-upload-add-button__add-text{color:#ffbba8}:host .file-upload-add-button .file-upload-add-button__doc-text{font-size:14px;font-weight:600;line-height:20px;color:#222}:host .file-upload-add-button .file-upload-add-button__add-text{font-size:14px;font-weight:600;line-height:20px;color:#e66e4b}\n"] }]
        }], ctorParameters: function () { return [{ type: FileSizePipe }, { type: ToastService }]; }, propDecorators: { fileUploads: [{
                type: Input
            }], maxCount: [{
                type: Input
            }], accept: [{
                type: Input
            }], maxSize: [{
                type: Input
            }], disabled: [{
                type: Input
            }], fileLabel: [{
                type: Input
            }], hasDownloadButton: [{
                type: Input
            }], hasDeleteButton: [{
                type: Input
            }], addFile: [{
                type: Output
            }], deleteFile: [{
                type: Output
            }], downloadFile: [{
                type: Output
            }] } });

class LibsPipesModule {
}
LibsPipesModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
LibsPipesModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, declarations: [FileSizePipe, FileTypePipe, TextShortenerPipe], exports: [FileSizePipe, FileTypePipe, TextShortenerPipe] });
LibsPipesModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [FileSizePipe, FileTypePipe, TextShortenerPipe],
                    exports: [FileSizePipe, FileTypePipe, TextShortenerPipe],
                }]
        }] });

class ToastModule {
}
ToastModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ToastModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, declarations: [ToastSnackbarComponent], imports: [CommonModule, MatSnackBarModule] });
ToastModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, providers: [ToastService], imports: [CommonModule, MatSnackBarModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ToastSnackbarComponent],
                    imports: [CommonModule, MatSnackBarModule],
                    providers: [ToastService],
                }]
        }] });

class FileUploadModule {
}
FileUploadModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
FileUploadModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, declarations: [FileUploadComponent, FileUploadItemComponent], imports: [CommonModule, LibsPipesModule, ToastModule], exports: [FileUploadComponent] });
FileUploadModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, providers: [FileSizePipe], imports: [CommonModule, LibsPipesModule, ToastModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [FileUploadComponent, FileUploadItemComponent],
                    imports: [CommonModule, LibsPipesModule, ToastModule],
                    providers: [FileSizePipe],
                    exports: [FileUploadComponent],
                }]
        }] });

class BackgroundedTextComponent {
    set type(value) {
        this.innerType = value;
        this.resetTypeClass();
        this.setTypeClass();
    }
    constructor() { }
    resetTypeClass() {
        this.successTypeClass = false;
        this.dangerTypeClass = false;
        this.warningTypeClass = false;
    }
    setTypeClass() {
        switch (this.innerType) {
            case "success":
                this.successTypeClass = true;
                break;
            case "danger":
                this.dangerTypeClass = true;
                break;
            case "warning":
                this.warningTypeClass = true;
        }
    }
}
BackgroundedTextComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
BackgroundedTextComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: BackgroundedTextComponent, selector: "app-backgrounded-text", inputs: { text: "text", type: "type" }, host: { properties: { "class.backgrounded-text_success": "this.successTypeClass", "class.backgrounded-text_warning": "this.warningTypeClass", "class.backgrounded-text_danger": "this.dangerTypeClass" } }, ngImport: i0, template: "{{text}}", styles: [":host{display:inline-block;padding:8px 10px;font-weight:500;font-size:11px;line-height:14px;border-radius:6px}:host.backgrounded-text_success{background-color:#55a9321a;color:#55a932}:host.backgrounded-text_warning{background-color:#f9a0001a;color:#f9a000}:host.backgrounded-text_danger{background-color:#d3292d1a;color:#d3292d}\n"] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-backgrounded-text", template: "{{text}}", styles: [":host{display:inline-block;padding:8px 10px;font-weight:500;font-size:11px;line-height:14px;border-radius:6px}:host.backgrounded-text_success{background-color:#55a9321a;color:#55a932}:host.backgrounded-text_warning{background-color:#f9a0001a;color:#f9a000}:host.backgrounded-text_danger{background-color:#d3292d1a;color:#d3292d}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { text: [{
                type: Input
            }], type: [{
                type: Input
            }], successTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_success"]
            }], warningTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_warning"]
            }], dangerTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_danger"]
            }] } });

class BackgroundedTextModule {
}
BackgroundedTextModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
BackgroundedTextModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, declarations: [BackgroundedTextComponent], imports: [CommonModule], exports: [BackgroundedTextComponent] });
BackgroundedTextModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [BackgroundedTextComponent],
                    imports: [CommonModule],
                    exports: [BackgroundedTextComponent],
                }]
        }] });

class FormatTimePipe {
    transform(value) {
        const minutes = Math.floor(value / 60);
        return (('00' + minutes).slice(-2) +
            ':' +
            ('00' + Math.floor(value - minutes * 60)).slice(-2));
    }
}
FormatTimePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
FormatTimePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, name: "formatTime" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'formatTime',
                }]
        }] });

let CountdownComponent = class CountdownComponent {
    set counterMinutes(value) {
        this.currentCounterMinutes = value;
        this.maxCounterMinutes = value;
    }
    constructor() {
        this.timeIsUp = new EventEmitter();
    }
    ngOnInit() {
        this.startTick();
    }
    startTick() {
        timer(0, 1000)
            .pipe(untilDestroyed(this))
            .subscribe(() => {
            if (this.currentCounterMinutes <= 0) {
                this.timeIsUp.emit();
            }
            else {
                this.currentCounterMinutes--;
            }
        });
    }
};
CountdownComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CountdownComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: CountdownComponent, selector: "app-countdown", inputs: { counterMinutes: "counterMinutes" }, outputs: { timeIsUp: "timeIsUp" }, ngImport: i0, template: "<round-progress\r\n  [current]=\"currentCounterMinutes\"\r\n  [max]=\"maxCounterMinutes\"\r\n  [stroke]=\"18\"\r\n  color=\"#E66E4B\"\r\n  background=\"#F5F5F5\"\r\n>\r\n</round-progress>\r\n\r\n<div class=\"countdown-content\">\r\n  <span class=\"countdown-minutes\">{{ currentCounterMinutes | formatTime }}</span>\r\n  <span class=\"countdown-text\">Seconds left</span>\r\n</div>\r\n", styles: [":host{display:inline-block;position:relative}:host round-progress{width:112px!important;height:112px!important;position:unset!important}:host .countdown-content{width:83px;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}:host .countdown-content .countdown-minutes{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;text-align:center}:host .countdown-content .countdown-text{display:block;font-size:11px;font-weight:400;line-height:14px;color:#999;text-align:center}\n"], dependencies: [{ kind: "component", type: i1$2.RoundProgressComponent, selector: "round-progress", inputs: ["current", "max", "radius", "animation", "animationDelay", "duration", "stroke", "color", "background", "responsive", "clockwise", "semicircle", "rounded"], outputs: ["onRender"] }, { kind: "pipe", type: FormatTimePipe, name: "formatTime" }] });
CountdownComponent = __decorate([
    UntilDestroy()
], CountdownComponent);
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-countdown', template: "<round-progress\r\n  [current]=\"currentCounterMinutes\"\r\n  [max]=\"maxCounterMinutes\"\r\n  [stroke]=\"18\"\r\n  color=\"#E66E4B\"\r\n  background=\"#F5F5F5\"\r\n>\r\n</round-progress>\r\n\r\n<div class=\"countdown-content\">\r\n  <span class=\"countdown-minutes\">{{ currentCounterMinutes | formatTime }}</span>\r\n  <span class=\"countdown-text\">Seconds left</span>\r\n</div>\r\n", styles: [":host{display:inline-block;position:relative}:host round-progress{width:112px!important;height:112px!important;position:unset!important}:host .countdown-content{width:83px;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}:host .countdown-content .countdown-minutes{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;text-align:center}:host .countdown-content .countdown-text{display:block;font-size:11px;font-weight:400;line-height:14px;color:#999;text-align:center}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { counterMinutes: [{
                type: Input
            }], timeIsUp: [{
                type: Output
            }] } });

class CountdownModule {
}
CountdownModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CountdownModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, declarations: [CountdownComponent, FormatTimePipe], imports: [CommonModule, RoundProgressModule], exports: [CountdownComponent] });
CountdownModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, imports: [CommonModule, RoundProgressModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [CountdownComponent, FormatTimePipe],
                    imports: [CommonModule, RoundProgressModule],
                    exports: [CountdownComponent],
                }]
        }] });

class LoaderService {
    constructor() {
        this.isLoaderActive = new BehaviorSubject(0);
    }
    increaseLoader() {
        this.isLoaderActive.next(this.isLoaderActive.value + 1);
    }
    decreaseLoader() {
        this.isLoaderActive.next(this.isLoaderActive.value > 1 ? this.isLoaderActive.value - 1 : 0);
    }
}
LoaderService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LoaderService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
LoaderService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LoaderService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LoaderService, decorators: [{
            type: Injectable,
            args: [{ providedIn: 'root' }]
        }], ctorParameters: function () { return []; } });

class ConfirmationWithIconComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ngOnInit() { }
    onConfirmClick() {
        this.dialogRef.close(true);
    }
}
ConfirmationWithIconComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconComponent, deps: [{ token: i1$3.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component });
ConfirmationWithIconComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: ConfirmationWithIconComponent, selector: "app-confirmation-with-icon", ngImport: i0, template: "<div class=\"confirmation-with-icon__header\">\r\n  <img src=\"/assets/icons/{{data.iconType}}_big.svg\" class=\"d-block ml-auto mr-auto mb-20\" alt=\"{{data.iconType}}\">\r\n  <h2 class=\"confirmation-with-icon__title\">{{ data.title }}</h2>\r\n</div>\r\n<div>\r\n  <span\r\n    *ngIf=\"data.description\"\r\n    class=\"confirmation-with-icon__description\"\r\n    >{{ data.description }}</span\r\n  >\r\n</div>\r\n<div class=\"confirmation-with-icon__actions\" align=\"end\">\r\n  <button\r\n    class=\"button button_br button_bg-gray button_c-black mr-10\"\r\n    cdkFocusInitial\r\n    [mat-dialog-close]=\"false\"\r\n  >\r\n    {{ data.closeButtonText || \"Ba\u011Fla\" }}\r\n  </button>\r\n  <button\r\n    class=\"button button_br button_bg-brand-orange button_c-white\"\r\n    (click)=\"onConfirmClick()\"\r\n  >\r\n    {{ data.confirmatioButtonText }}\r\n  </button>\r\n</div>\r\n", styles: [":host .confirmation-with-icon__header .confirmation-with-icon__title{display:block;font-weight:600;font-size:16px;line-height:24px;color:#222;text-align:center}:host .confirmation-with-icon__description{display:block;font-weight:500;font-size:14px;line-height:24px;color:#a0a0a0;text-align:center;margin-top:20px}:host .confirmation-with-icon__actions{margin-top:32px}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$3.MatDialogClose, selector: "[mat-dialog-close], [matDialogClose]", inputs: ["aria-label", "type", "mat-dialog-close", "matDialogClose"], exportAs: ["matDialogClose"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-confirmation-with-icon', template: "<div class=\"confirmation-with-icon__header\">\r\n  <img src=\"/assets/icons/{{data.iconType}}_big.svg\" class=\"d-block ml-auto mr-auto mb-20\" alt=\"{{data.iconType}}\">\r\n  <h2 class=\"confirmation-with-icon__title\">{{ data.title }}</h2>\r\n</div>\r\n<div>\r\n  <span\r\n    *ngIf=\"data.description\"\r\n    class=\"confirmation-with-icon__description\"\r\n    >{{ data.description }}</span\r\n  >\r\n</div>\r\n<div class=\"confirmation-with-icon__actions\" align=\"end\">\r\n  <button\r\n    class=\"button button_br button_bg-gray button_c-black mr-10\"\r\n    cdkFocusInitial\r\n    [mat-dialog-close]=\"false\"\r\n  >\r\n    {{ data.closeButtonText || \"Ba\u011Fla\" }}\r\n  </button>\r\n  <button\r\n    class=\"button button_br button_bg-brand-orange button_c-white\"\r\n    (click)=\"onConfirmClick()\"\r\n  >\r\n    {{ data.confirmatioButtonText }}\r\n  </button>\r\n</div>\r\n", styles: [":host .confirmation-with-icon__header .confirmation-with-icon__title{display:block;font-weight:600;font-size:16px;line-height:24px;color:#222;text-align:center}:host .confirmation-with-icon__description{display:block;font-weight:500;font-size:14px;line-height:24px;color:#a0a0a0;text-align:center;margin-top:20px}:host .confirmation-with-icon__actions{margin-top:32px}\n"] }]
        }], ctorParameters: function () { return [{ type: i1$3.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }]; } });

class ConfirmationWithIconService {
    constructor(dialog) {
        this.dialog = dialog;
        this.configs = {
            width: '340px',
            panelClass: 'ib-mat-dialog',
        };
    }
    open(configs = {}) {
        return this.dialog.open(ConfirmationWithIconComponent, {
            ...this.configs,
            ...configs,
            data: {
                ...configs.data,
            },
        });
    }
}
ConfirmationWithIconService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, deps: [{ token: i1$3.MatDialog }], target: i0.ɵɵFactoryTarget.Injectable });
ConfirmationWithIconService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }], ctorParameters: function () { return [{ type: i1$3.MatDialog }]; } });

class ConfirmationWithIconModule {
}
ConfirmationWithIconModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ConfirmationWithIconModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, declarations: [ConfirmationWithIconComponent], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
ConfirmationWithIconModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, providers: [ConfirmationWithIconService], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ConfirmationWithIconComponent],
                    imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
                    providers: [ConfirmationWithIconService],
                }]
        }] });

class ConfirmationWithReasonComponent {
    constructor(fb, dialogRef, data) {
        this.fb = fb;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ngOnInit() {
        this.createFormGroup();
    }
    createFormGroup() {
        this.formGroup = this.fb.group({
            reason: [null],
        });
        const validatorFns = [];
        if (this.data.isReasonRequired) {
            validatorFns.push(Validators.required);
        }
        if (this.data.maxLength !== undefined) {
            validatorFns.push(Validators.maxLength(this.data.maxLength));
        }
        this.formGroup.get("reason")?.setValidators(validatorFns);
        this.formGroup.get("reason")?.updateValueAndValidity();
    }
    onConfirmClick() {
        this.formGroup.markAsTouched();
        if (this.formGroup.invalid) {
            return;
        }
        this.dialogRef.close(this.formGroup.getRawValue().reason);
    }
}
ConfirmationWithReasonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonComponent, deps: [{ token: i1$4.FormBuilder }, { token: i1$3.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component });
ConfirmationWithReasonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: ConfirmationWithReasonComponent, selector: "app-confirmation-with-reason", ngImport: i0, template: "<div class=\"confirmation-with-reason__header\">\r\n  <h2 class=\"confirmation-with-reason__title\">{{ data.title }}</h2>\r\n</div>\r\n<div>\r\n  <span\r\n    *ngIf=\"data.description\"\r\n    class=\"confirmation-with-reason__description\"\r\n    >{{ data.description }}</span\r\n  >\r\n  <form [formGroup]=\"formGroup\">\r\n    <div class=\"form-group\">\r\n      <label class=\"form-group__label\">S\u0259b\u0259b</label>\r\n      <div class=\"input-block input-block_h-48\">\r\n        <textarea\r\n          [maxlength]=\"data.maxLength || 5000\"\r\n          formControlName=\"reason\"\r\n          class=\"input-block__textarea\"\r\n        ></textarea>\r\n        <span\r\n          *ngIf=\"data.maxLength !== undefined\"\r\n          class=\"input-block__chars-count mt-20 ml-auto\"\r\n          >{{ formGroup.value.reason?.length || 0 }}/{{ data.maxLength }}</span\r\n        >\r\n      </div>\r\n      <span\r\n        *ngIf=\"formGroup.touched && formGroup?.hasError('required', 'reason')\"\r\n        class=\"error mt-5\"\r\n        >S\u0259b\u0259b sah\u0259si z\u0259ruridir</span\r\n      >\r\n    </div>\r\n  </form>\r\n</div>\r\n<mat-dialog-actions align=\"end\">\r\n  <button\r\n    class=\"button button_br button_bg-gray button_c-black mr-10\"\r\n    cdkFocusInitial\r\n    mat-dialog-close\r\n  >\r\n    {{ data.closeButtonText || \"Ba\u011Fla\" }}\r\n  </button>\r\n  <button\r\n    class=\"button button_br button_bg-brand-orange button_c-white\"\r\n    (click)=\"onConfirmClick()\"\r\n  >\r\n    {{ data.confirmatioButtonText }}\r\n  </button>\r\n</mat-dialog-actions>\r\n", styles: [":host .confirmation-with-reason__header .confirmation-with-reason__title{display:block;font-weight:600;font-size:16px;line-height:24px;color:#222;text-align:center}:host .confirmation-with-reason__description{display:block;font-weight:500;font-size:14px;line-height:24px;color:#a0a0a0;text-align:center;margin-top:20px}:host form{margin-top:26px}:host mat-dialog-actions{margin-top:32px}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$4.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { kind: "directive", type: i1$4.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$4.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$4.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { kind: "directive", type: i1$4.MaxLengthValidator, selector: "[maxlength][formControlName],[maxlength][formControl],[maxlength][ngModel]", inputs: ["maxlength"] }, { kind: "directive", type: i1$4.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { kind: "directive", type: i1$4.FormControlName, selector: "[formControlName]", inputs: ["formControlName", "disabled", "ngModel"], outputs: ["ngModelChange"] }, { kind: "directive", type: i1$3.MatDialogClose, selector: "[mat-dialog-close], [matDialogClose]", inputs: ["aria-label", "type", "mat-dialog-close", "matDialogClose"], exportAs: ["matDialogClose"] }, { kind: "directive", type: i1$3.MatDialogActions, selector: "[mat-dialog-actions], mat-dialog-actions, [matDialogActions]", inputs: ["align"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-confirmation-with-reason", template: "<div class=\"confirmation-with-reason__header\">\r\n  <h2 class=\"confirmation-with-reason__title\">{{ data.title }}</h2>\r\n</div>\r\n<div>\r\n  <span\r\n    *ngIf=\"data.description\"\r\n    class=\"confirmation-with-reason__description\"\r\n    >{{ data.description }}</span\r\n  >\r\n  <form [formGroup]=\"formGroup\">\r\n    <div class=\"form-group\">\r\n      <label class=\"form-group__label\">S\u0259b\u0259b</label>\r\n      <div class=\"input-block input-block_h-48\">\r\n        <textarea\r\n          [maxlength]=\"data.maxLength || 5000\"\r\n          formControlName=\"reason\"\r\n          class=\"input-block__textarea\"\r\n        ></textarea>\r\n        <span\r\n          *ngIf=\"data.maxLength !== undefined\"\r\n          class=\"input-block__chars-count mt-20 ml-auto\"\r\n          >{{ formGroup.value.reason?.length || 0 }}/{{ data.maxLength }}</span\r\n        >\r\n      </div>\r\n      <span\r\n        *ngIf=\"formGroup.touched && formGroup?.hasError('required', 'reason')\"\r\n        class=\"error mt-5\"\r\n        >S\u0259b\u0259b sah\u0259si z\u0259ruridir</span\r\n      >\r\n    </div>\r\n  </form>\r\n</div>\r\n<mat-dialog-actions align=\"end\">\r\n  <button\r\n    class=\"button button_br button_bg-gray button_c-black mr-10\"\r\n    cdkFocusInitial\r\n    mat-dialog-close\r\n  >\r\n    {{ data.closeButtonText || \"Ba\u011Fla\" }}\r\n  </button>\r\n  <button\r\n    class=\"button button_br button_bg-brand-orange button_c-white\"\r\n    (click)=\"onConfirmClick()\"\r\n  >\r\n    {{ data.confirmatioButtonText }}\r\n  </button>\r\n</mat-dialog-actions>\r\n", styles: [":host .confirmation-with-reason__header .confirmation-with-reason__title{display:block;font-weight:600;font-size:16px;line-height:24px;color:#222;text-align:center}:host .confirmation-with-reason__description{display:block;font-weight:500;font-size:14px;line-height:24px;color:#a0a0a0;text-align:center;margin-top:20px}:host form{margin-top:26px}:host mat-dialog-actions{margin-top:32px}\n"] }]
        }], ctorParameters: function () { return [{ type: i1$4.FormBuilder }, { type: i1$3.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }]; } });

class ConfirmationWithReasonService {
    constructor(dialog) {
        this.dialog = dialog;
        this.configs = {
            width: "468px",
            data: {
                isReasonRequired: false,
            },
        };
    }
    open(configs = {}) {
        return this.dialog.open(ConfirmationWithReasonComponent, {
            ...this.configs,
            ...configs,
            data: {
                ...this.configs.data,
                ...configs.data,
            },
        });
    }
}
ConfirmationWithReasonService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, deps: [{ token: i1$3.MatDialog }], target: i0.ɵɵFactoryTarget.Injectable });
ConfirmationWithReasonService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, providedIn: "root" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: "root",
                }]
        }], ctorParameters: function () { return [{ type: i1$3.MatDialog }]; } });

class ConfirmationWithReasonModule {
}
ConfirmationWithReasonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ConfirmationWithReasonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, declarations: [ConfirmationWithReasonComponent], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
ConfirmationWithReasonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, providers: [ConfirmationWithReasonService], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ConfirmationWithReasonComponent],
                    imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
                    providers: [ConfirmationWithReasonService],
                }]
        }] });

/*
 * Public API Surface of components
 */

/**
 * Generated bundle index. Do not edit.
 */

export { BackgroundedTextComponent, BackgroundedTextModule, CommonHelperService, ConfirmationWithIconComponent, ConfirmationWithIconModule, ConfirmationWithIconService, ConfirmationWithReasonComponent, ConfirmationWithReasonModule, ConfirmationWithReasonService, CountdownComponent, CountdownModule, FileSizePipe, FileTypePipe, FileUploadComponent, FileUploadItemComponent, FileUploadModule, FormatTimePipe, IconnedTextComponent, IconnedTextModule, LibsPipesModule, LoaderService, PaginationComponent, PaginationInfoComponent, PaginationModule, PanelInfoComponent, PanelInfoModule, RecordColumnComponent, RecordColumnModule, TabsBigComponent, TabsBigModule, TabsSmallComponent, TabsSmallModule, TextShortenerPipe, ToastModule, ToastService, ToastSnackbarComponent };
//# sourceMappingURL=components.mjs.map
