import { Injectable } from "@angular/core";
import { ConfirmationWithReasonComponent } from "../dialogs/confirmation-with-reason/confirmation-with-reason.component";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class ConfirmationWithReasonService {
    constructor(dialog) {
        this.dialog = dialog;
        this.configs = {
            width: "468px",
            data: {
                isReasonRequired: false,
            },
        };
    }
    open(configs = {}) {
        return this.dialog.open(ConfirmationWithReasonComponent, {
            ...this.configs,
            ...configs,
            data: {
                ...this.configs.data,
                ...configs.data,
            },
        });
    }
}
ConfirmationWithReasonService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, deps: [{ token: i1.MatDialog }], target: i0.ɵɵFactoryTarget.Injectable });
ConfirmationWithReasonService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, providedIn: "root" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: "root",
                }]
        }], ctorParameters: function () { return [{ type: i1.MatDialog }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvZGlhbG9ncy9jb25maXJtYXRpb24td2l0aC1yZWFzb24vc2VydmljZXMvY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU0zQyxPQUFPLEVBQUUsK0JBQStCLEVBQUUsTUFBTSx3RUFBd0UsQ0FBQzs7O0FBTXpILE1BQU0sT0FBTyw2QkFBNkI7SUFPeEMsWUFBb0IsTUFBaUI7UUFBakIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQU5wQixZQUFPLEdBQW9CO1lBQzFDLEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFO2dCQUNKLGdCQUFnQixFQUFFLEtBQUs7YUFDeEI7U0FDRixDQUFDO0lBQ3NDLENBQUM7SUFFekMsSUFBSSxDQUNGLFVBQTJCLEVBQUU7UUFFN0IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywrQkFBK0IsRUFBRTtZQUN2RCxHQUFHLElBQUksQ0FBQyxPQUFPO1lBQ2YsR0FBRyxPQUFPO1lBQ1YsSUFBSSxFQUFFO2dCQUNKLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUNwQixHQUFHLE9BQU8sQ0FBQyxJQUFJO2FBQ2hCO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7MEhBcEJVLDZCQUE2Qjs4SEFBN0IsNkJBQTZCLGNBRjVCLE1BQU07MkZBRVAsNkJBQTZCO2tCQUh6QyxVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1xyXG4gIE1hdERpYWxvZyxcclxuICBNYXREaWFsb2dDb25maWcsXHJcbiAgTWF0RGlhbG9nUmVmLFxyXG59IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2dcIjtcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uV2l0aFJlYXNvbkNvbXBvbmVudCB9IGZyb20gXCIuLi9kaWFsb2dzL2NvbmZpcm1hdGlvbi13aXRoLXJlYXNvbi9jb25maXJtYXRpb24td2l0aC1yZWFzb24uY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbldpdGhSZWFzb25EaWFsb2dEYXRhIH0gZnJvbSBcIi4uL2ludGVyZmFjZXMvY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLWRpYWxvZy1kYXRhLmludGVyZmFjZVwiO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46IFwicm9vdFwiLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29uZmlybWF0aW9uV2l0aFJlYXNvblNlcnZpY2Uge1xyXG4gIHByaXZhdGUgcmVhZG9ubHkgY29uZmlnczogTWF0RGlhbG9nQ29uZmlnID0ge1xyXG4gICAgd2lkdGg6IFwiNDY4cHhcIixcclxuICAgIGRhdGE6IHtcclxuICAgICAgaXNSZWFzb25SZXF1aXJlZDogZmFsc2UsXHJcbiAgICB9LFxyXG4gIH07XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBkaWFsb2c6IE1hdERpYWxvZykge31cclxuXHJcbiAgb3BlbihcclxuICAgIGNvbmZpZ3M6IE1hdERpYWxvZ0NvbmZpZyA9IHt9XHJcbiAgKTogTWF0RGlhbG9nUmVmPENvbmZpcm1hdGlvbldpdGhSZWFzb25Db21wb25lbnQsIENvbmZpcm1hdGlvbldpdGhSZWFzb25EaWFsb2dEYXRhPiB7XHJcbiAgICByZXR1cm4gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25XaXRoUmVhc29uQ29tcG9uZW50LCB7XHJcbiAgICAgIC4uLnRoaXMuY29uZmlncyxcclxuICAgICAgLi4uY29uZmlncyxcclxuICAgICAgZGF0YToge1xyXG4gICAgICAgIC4uLnRoaXMuY29uZmlncy5kYXRhLFxyXG4gICAgICAgIC4uLmNvbmZpZ3MuZGF0YSxcclxuICAgICAgfSxcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=