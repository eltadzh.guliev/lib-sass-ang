import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConfirmationWithReasonComponent } from "./dialogs/confirmation-with-reason/confirmation-with-reason.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ConfirmationWithReasonService } from "./services/confirmation-with-reason.service";
import { MatDialogModule } from "@angular/material/dialog";
import * as i0 from "@angular/core";
export class ConfirmationWithReasonModule {
}
ConfirmationWithReasonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ConfirmationWithReasonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, declarations: [ConfirmationWithReasonComponent], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
ConfirmationWithReasonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, providers: [ConfirmationWithReasonService], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithReasonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ConfirmationWithReasonComponent],
                    imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
                    providers: [ConfirmationWithReasonService],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9kaWFsb2dzL2NvbmZpcm1hdGlvbi13aXRoLXJlYXNvbi9jb25maXJtYXRpb24td2l0aC1yZWFzb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSwrQkFBK0IsRUFBRSxNQUFNLHVFQUF1RSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JELE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzVGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7QUFPM0QsTUFBTSxPQUFPLDRCQUE0Qjs7eUhBQTVCLDRCQUE0QjswSEFBNUIsNEJBQTRCLGlCQUp4QiwrQkFBK0IsYUFDcEMsWUFBWSxFQUFFLG1CQUFtQixFQUFFLGVBQWU7MEhBR2pELDRCQUE0QixhQUY1QixDQUFDLDZCQUE2QixDQUFDLFlBRGhDLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxlQUFlOzJGQUdqRCw0QkFBNEI7a0JBTHhDLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsK0JBQStCLENBQUM7b0JBQy9DLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxlQUFlLENBQUM7b0JBQzdELFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO2lCQUMzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25XaXRoUmVhc29uQ29tcG9uZW50IH0gZnJvbSBcIi4vZGlhbG9ncy9jb25maXJtYXRpb24td2l0aC1yZWFzb24vY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbldpdGhSZWFzb25TZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZXMvY29uZmlybWF0aW9uLXdpdGgtcmVhc29uLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTWF0RGlhbG9nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2RpYWxvZ1wiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtDb25maXJtYXRpb25XaXRoUmVhc29uQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlLCBNYXREaWFsb2dNb2R1bGVdLFxyXG4gIHByb3ZpZGVyczogW0NvbmZpcm1hdGlvbldpdGhSZWFzb25TZXJ2aWNlXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmZpcm1hdGlvbldpdGhSZWFzb25Nb2R1bGUge31cclxuIl19