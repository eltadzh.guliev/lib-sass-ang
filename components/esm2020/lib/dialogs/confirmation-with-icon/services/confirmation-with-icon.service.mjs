import { Injectable } from '@angular/core';
import { ConfirmationWithIconComponent } from '../dialogs/confirmation-with-icon/confirmation-with-icon.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class ConfirmationWithIconService {
    constructor(dialog) {
        this.dialog = dialog;
        this.configs = {
            width: '340px',
            panelClass: 'ib-mat-dialog',
        };
    }
    open(configs = {}) {
        return this.dialog.open(ConfirmationWithIconComponent, {
            ...this.configs,
            ...configs,
            data: {
                ...configs.data,
            },
        });
    }
}
ConfirmationWithIconService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, deps: [{ token: i1.MatDialog }], target: i0.ɵɵFactoryTarget.Injectable });
ConfirmationWithIconService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }], ctorParameters: function () { return [{ type: i1.MatDialog }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLXdpdGgtaWNvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2RpYWxvZ3MvY29uZmlybWF0aW9uLXdpdGgtaWNvbi9zZXJ2aWNlcy9jb25maXJtYXRpb24td2l0aC1pY29uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU0zQyxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQzs7O0FBTW5ILE1BQU0sT0FBTywyQkFBMkI7SUFLdEMsWUFBb0IsTUFBaUI7UUFBakIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUpwQixZQUFPLEdBQW9CO1lBQzFDLEtBQUssRUFBRSxPQUFPO1lBQ2QsVUFBVSxFQUFFLGVBQWU7U0FDNUIsQ0FBQztJQUNzQyxDQUFDO0lBRXpDLElBQUksQ0FDRixVQUEyQixFQUFFO1FBSzdCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEVBQUU7WUFDckQsR0FBRyxJQUFJLENBQUMsT0FBTztZQUNmLEdBQUcsT0FBTztZQUNWLElBQUksRUFBRTtnQkFDSixHQUFHLE9BQU8sQ0FBQyxJQUFJO2FBQ2hCO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7d0hBcEJVLDJCQUEyQjs0SEFBM0IsMkJBQTJCLGNBRjFCLE1BQU07MkZBRVAsMkJBQTJCO2tCQUh2QyxVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBNYXREaWFsb2csXHJcbiAgTWF0RGlhbG9nQ29uZmlnLFxyXG4gIE1hdERpYWxvZ1JlZixcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25XaXRoSWNvbkNvbXBvbmVudCB9IGZyb20gJy4uL2RpYWxvZ3MvY29uZmlybWF0aW9uLXdpdGgtaWNvbi9jb25maXJtYXRpb24td2l0aC1pY29uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbldpdGhJY29uRGlhbG9nRGF0YSB9IGZyb20gJy4uL2ludGVyZmFjZXMvY29uZmlybWF0aW9uLXdpdGgtaWNvbi1kaWFsb2ctZGF0YS5pbnRlcmZhY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290JyxcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmZpcm1hdGlvbldpdGhJY29uU2VydmljZSB7XHJcbiAgcHJpdmF0ZSByZWFkb25seSBjb25maWdzOiBNYXREaWFsb2dDb25maWcgPSB7XHJcbiAgICB3aWR0aDogJzM0MHB4JyxcclxuICAgIHBhbmVsQ2xhc3M6ICdpYi1tYXQtZGlhbG9nJyxcclxuICB9O1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGlhbG9nOiBNYXREaWFsb2cpIHt9XHJcblxyXG4gIG9wZW4oXHJcbiAgICBjb25maWdzOiBNYXREaWFsb2dDb25maWcgPSB7fVxyXG4gICk6IE1hdERpYWxvZ1JlZjxcclxuICAgIENvbmZpcm1hdGlvbldpdGhJY29uQ29tcG9uZW50LFxyXG4gICAgQ29uZmlybWF0aW9uV2l0aEljb25EaWFsb2dEYXRhXHJcbiAgPiB7XHJcbiAgICByZXR1cm4gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25XaXRoSWNvbkNvbXBvbmVudCwge1xyXG4gICAgICAuLi50aGlzLmNvbmZpZ3MsXHJcbiAgICAgIC4uLmNvbmZpZ3MsXHJcbiAgICAgIGRhdGE6IHtcclxuICAgICAgICAuLi5jb25maWdzLmRhdGEsXHJcbiAgICAgIH0sXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19