import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationWithIconComponent } from './dialogs/confirmation-with-icon/confirmation-with-icon.component';
import { ConfirmationWithIconService } from './services/confirmation-with-icon.service';
import { MatDialogModule } from '@angular/material/dialog';
import * as i0 from "@angular/core";
export class ConfirmationWithIconModule {
}
ConfirmationWithIconModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ConfirmationWithIconModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, declarations: [ConfirmationWithIconComponent], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
ConfirmationWithIconModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, providers: [ConfirmationWithIconService], imports: [CommonModule, ReactiveFormsModule, MatDialogModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ConfirmationWithIconModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ConfirmationWithIconComponent],
                    imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
                    providers: [ConfirmationWithIconService],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLXdpdGgtaWNvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvZGlhbG9ncy9jb25maXJtYXRpb24td2l0aC1pY29uL2NvbmZpcm1hdGlvbi13aXRoLWljb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JELE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG1FQUFtRSxDQUFDO0FBQ2xILE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7QUFPM0QsTUFBTSxPQUFPLDBCQUEwQjs7dUhBQTFCLDBCQUEwQjt3SEFBMUIsMEJBQTBCLGlCQUp0Qiw2QkFBNkIsYUFDbEMsWUFBWSxFQUFFLG1CQUFtQixFQUFFLGVBQWU7d0hBR2pELDBCQUEwQixhQUYxQixDQUFDLDJCQUEyQixDQUFDLFlBRDlCLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxlQUFlOzJGQUdqRCwwQkFBMEI7a0JBTHRDLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsNkJBQTZCLENBQUM7b0JBQzdDLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxlQUFlLENBQUM7b0JBQzdELFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2lCQUN6QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbldpdGhJY29uQ29tcG9uZW50IH0gZnJvbSAnLi9kaWFsb2dzL2NvbmZpcm1hdGlvbi13aXRoLWljb24vY29uZmlybWF0aW9uLXdpdGgtaWNvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25XaXRoSWNvblNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2NvbmZpcm1hdGlvbi13aXRoLWljb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0NvbmZpcm1hdGlvbldpdGhJY29uQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlLCBNYXREaWFsb2dNb2R1bGVdLFxyXG4gIHByb3ZpZGVyczogW0NvbmZpcm1hdGlvbldpdGhJY29uU2VydmljZV0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25maXJtYXRpb25XaXRoSWNvbk1vZHVsZSB7fVxyXG4iXX0=