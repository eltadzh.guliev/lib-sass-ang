import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class CommonHelperService {
    constructor() { }
    downloadBLOBWithDispositionName(blob, headers) {
        const filename = this.getFilenameFromHeader(headers) || 'file';
        this.downloadBLOB(blob, filename);
    }
    getFilenameFromHeader(headers) {
        const contentDisposition = headers.get('Content-Disposition');
        if (contentDisposition) {
            const contentDispositionMatches = contentDisposition.match(/.*filename="(.*)"/);
            return contentDispositionMatches.length >= 2
                ? contentDispositionMatches[1]
                : '';
        }
        else {
            return '';
        }
    }
    downloadBLOB(blob, filename) {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
    isHTML(text) {
        const doc = new DOMParser().parseFromString(text, 'text/html');
        return Array.from(doc.body.childNodes).some((node) => node.nodeType === 1);
    }
    getRandomNumbers(numbersLength) {
        return Math.floor(Math.pow(10, numbersLength - 1) +
            Math.random() * Math.pow(9, numbersLength - 1)).toString();
    }
    blobToDataURL(blob) {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        });
    }
    readFileAsText(file) {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsText(file);
        });
    }
}
CommonHelperService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
CommonHelperService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CommonHelperService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root',
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmhlbHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9zZXJ2aWNlcy9jb21tb24uaGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxtQkFBbUI7SUFDOUIsZ0JBQWUsQ0FBQztJQUVoQiwrQkFBK0IsQ0FBQyxJQUFVLEVBQUUsT0FBb0I7UUFDOUQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQztRQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBb0I7UUFDeEMsTUFBTSxrQkFBa0IsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFFOUQsSUFBSSxrQkFBa0IsRUFBRTtZQUN0QixNQUFNLHlCQUF5QixHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FDeEQsbUJBQW1CLENBQ3BCLENBQUM7WUFFRixPQUFPLHlCQUF5QixDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUMxQyxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ1I7YUFBTTtZQUNMLE9BQU8sRUFBRSxDQUFDO1NBQ1g7SUFDSCxDQUFDO0lBRUQsWUFBWSxDQUFDLElBQVUsRUFBRSxRQUFnQjtRQUN2QyxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN6QixDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNiLHdCQUF3QjtRQUN4QixDQUFDLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN0QixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM3QixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDVixNQUFNLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDYixDQUFDO0lBRUQsTUFBTSxDQUFDLElBQVk7UUFDakIsTUFBTSxHQUFHLEdBQUcsSUFBSSxTQUFTLEVBQUUsQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9ELE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsYUFBcUI7UUFDcEMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLGFBQWEsR0FBRyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FDakQsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFRCxhQUFhLENBQUMsSUFBVTtRQUN0QixPQUFPLElBQUksT0FBTyxDQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0RCxNQUFNLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFVO1FBQ3ZCLE9BQU8sSUFBSSxPQUFPLENBQXVCLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RELE1BQU0sTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7WUFDaEMsTUFBTSxDQUFDLFNBQVMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnSEEvRFUsbUJBQW1CO29IQUFuQixtQkFBbUIsY0FGbEIsTUFBTTsyRkFFUCxtQkFBbUI7a0JBSC9CLFVBQVU7bUJBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tb25IZWxwZXJTZXJ2aWNlIHtcclxuICBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gIGRvd25sb2FkQkxPQldpdGhEaXNwb3NpdGlvbk5hbWUoYmxvYjogQmxvYiwgaGVhZGVyczogSHR0cEhlYWRlcnMpOiB2b2lkIHtcclxuICAgIGNvbnN0IGZpbGVuYW1lID0gdGhpcy5nZXRGaWxlbmFtZUZyb21IZWFkZXIoaGVhZGVycykgfHwgJ2ZpbGUnO1xyXG4gICAgdGhpcy5kb3dubG9hZEJMT0IoYmxvYiwgZmlsZW5hbWUpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmlsZW5hbWVGcm9tSGVhZGVyKGhlYWRlcnM6IEh0dHBIZWFkZXJzKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IGNvbnRlbnREaXNwb3NpdGlvbiA9IGhlYWRlcnMuZ2V0KCdDb250ZW50LURpc3Bvc2l0aW9uJyk7XHJcblxyXG4gICAgaWYgKGNvbnRlbnREaXNwb3NpdGlvbikge1xyXG4gICAgICBjb25zdCBjb250ZW50RGlzcG9zaXRpb25NYXRjaGVzID0gY29udGVudERpc3Bvc2l0aW9uLm1hdGNoKFxyXG4gICAgICAgIC8uKmZpbGVuYW1lPVwiKC4qKVwiL1xyXG4gICAgICApO1xyXG5cclxuICAgICAgcmV0dXJuIGNvbnRlbnREaXNwb3NpdGlvbk1hdGNoZXMubGVuZ3RoID49IDJcclxuICAgICAgICA/IGNvbnRlbnREaXNwb3NpdGlvbk1hdGNoZXNbMV1cclxuICAgICAgICA6ICcnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZG93bmxvYWRCTE9CKGJsb2I6IEJsb2IsIGZpbGVuYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGNvbnN0IHVybCA9IHdpbmRvdy5VUkwuY3JlYXRlT2JqZWN0VVJMKGJsb2IpO1xyXG4gICAgY29uc3QgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcclxuICAgIGEuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgIGEuaHJlZiA9IHVybDtcclxuICAgIC8vIHRoZSBmaWxlbmFtZSB5b3Ugd2FudFxyXG4gICAgYS5kb3dubG9hZCA9IGZpbGVuYW1lO1xyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChhKTtcclxuICAgIGEuY2xpY2soKTtcclxuICAgIHdpbmRvdy5VUkwucmV2b2tlT2JqZWN0VVJMKHVybCk7XHJcbiAgICBhLnJlbW92ZSgpO1xyXG4gIH1cclxuXHJcbiAgaXNIVE1MKHRleHQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgZG9jID0gbmV3IERPTVBhcnNlcigpLnBhcnNlRnJvbVN0cmluZyh0ZXh0LCAndGV4dC9odG1sJyk7XHJcbiAgICByZXR1cm4gQXJyYXkuZnJvbShkb2MuYm9keS5jaGlsZE5vZGVzKS5zb21lKChub2RlKSA9PiBub2RlLm5vZGVUeXBlID09PSAxKTtcclxuICB9XHJcblxyXG4gIGdldFJhbmRvbU51bWJlcnMobnVtYmVyc0xlbmd0aDogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgIHJldHVybiBNYXRoLmZsb29yKFxyXG4gICAgICBNYXRoLnBvdygxMCwgbnVtYmVyc0xlbmd0aCAtIDEpICtcclxuICAgICAgICBNYXRoLnJhbmRvbSgpICogTWF0aC5wb3coOSwgbnVtYmVyc0xlbmd0aCAtIDEpXHJcbiAgICApLnRvU3RyaW5nKCk7XHJcbiAgfVxyXG5cclxuICBibG9iVG9EYXRhVVJMKGJsb2I6IEJsb2IpOiBQcm9taXNlPHN0cmluZyB8IEFycmF5QnVmZmVyPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nIHwgQXJyYXlCdWZmZXI+KChyZXNvbHZlLCBfKSA9PiB7XHJcbiAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgIHJlYWRlci5vbmxvYWRlbmQgPSAoKSA9PiByZXNvbHZlKHJlYWRlci5yZXN1bHQpO1xyXG4gICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChibG9iKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmVhZEZpbGVBc1RleHQoZmlsZTogRmlsZSk6IFByb21pc2U8c3RyaW5nIHwgQXJyYXlCdWZmZXI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxzdHJpbmcgfCBBcnJheUJ1ZmZlcj4oKHJlc29sdmUsIF8pID0+IHtcclxuICAgICAgY29uc3QgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgcmVhZGVyLm9ubG9hZGVuZCA9ICgpID0+IHJlc29sdmUocmVhZGVyLnJlc3VsdCk7XHJcbiAgICAgIHJlYWRlci5yZWFkQXNUZXh0KGZpbGUpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==