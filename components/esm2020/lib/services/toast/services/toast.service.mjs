import { Injectable } from "@angular/core";
import { ToastSnackbarComponent } from "../components/toast-snackbar/toast-snackbar.component";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
export class ToastService {
    constructor(snackBar) {
        this.snackBar = snackBar;
        this.config = {
            duration: 50000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
            panelClass: ["corad-snack-bar-container"],
        };
    }
    showMessage(message, status = "info", action, actionText) {
        const data = {
            message,
            status,
            action,
            actionText,
        };
        return this.snackBar.openFromComponent(ToastSnackbarComponent, {
            ...this.config,
            panelClass: [...this.config.panelClass, status],
            data,
        });
    }
}
ToastService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, deps: [{ token: i1.MatSnackBar }], target: i0.ɵɵFactoryTarget.Injectable });
ToastService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, providedIn: "root" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: "root",
                }]
        }], ctorParameters: function () { return [{ type: i1.MatSnackBar }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9zZXJ2aWNlcy90b2FzdC9zZXJ2aWNlcy90b2FzdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNM0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdURBQXVELENBQUM7OztBQU8vRixNQUFNLE9BQU8sWUFBWTtJQVF2QixZQUE2QixRQUFxQjtRQUFyQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBUHpDLFdBQU0sR0FBc0I7WUFDbkMsUUFBUSxFQUFFLEtBQUs7WUFDZixrQkFBa0IsRUFBRSxPQUFPO1lBQzNCLGdCQUFnQixFQUFFLFFBQVE7WUFDMUIsVUFBVSxFQUFFLENBQUMsMkJBQTJCLENBQUM7U0FDMUMsQ0FBQztJQUVtRCxDQUFDO0lBRXRELFdBQVcsQ0FDVCxPQUFlLEVBQ2YsU0FBb0IsTUFBTSxFQUMxQixNQUEwQixFQUMxQixVQUFtQjtRQUVuQixNQUFNLElBQUksR0FBYztZQUN0QixPQUFPO1lBQ1AsTUFBTTtZQUNOLE1BQU07WUFDTixVQUFVO1NBQ1gsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FDcEMsc0JBQXNCLEVBQ3RCO1lBQ0UsR0FBRyxJQUFJLENBQUMsTUFBTTtZQUNkLFVBQVUsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDO1lBQy9DLElBQUk7U0FDTCxDQUNGLENBQUM7SUFDSixDQUFDOzt5R0E5QlUsWUFBWTs2R0FBWixZQUFZLGNBRlgsTUFBTTsyRkFFUCxZQUFZO2tCQUh4QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1xyXG4gIE1hdFNuYWNrQmFyLFxyXG4gIE1hdFNuYWNrQmFyQ29uZmlnLFxyXG4gIE1hdFNuYWNrQmFyUmVmLFxyXG59IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXJcIjtcclxuaW1wb3J0IHsgVG9hc3RTbmFja2JhckNvbXBvbmVudCB9IGZyb20gXCIuLi9jb21wb25lbnRzL3RvYXN0LXNuYWNrYmFyL3RvYXN0LXNuYWNrYmFyLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBUb2FzdERhdGEgfSBmcm9tIFwiLi4vaW50ZXJmYWNlcy90b2FzdC1kYXRhLmludGVyZmFjZVwiO1xyXG5pbXBvcnQgeyBUb2FzdFR5cGUgfSBmcm9tIFwiLi4vdHlwZXMvdG9hc3QudHlwZVwiO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46IFwicm9vdFwiLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9hc3RTZXJ2aWNlIHtcclxuICByZWFkb25seSBjb25maWc6IE1hdFNuYWNrQmFyQ29uZmlnID0ge1xyXG4gICAgZHVyYXRpb246IDUwMDAwLFxyXG4gICAgaG9yaXpvbnRhbFBvc2l0aW9uOiBcInJpZ2h0XCIsXHJcbiAgICB2ZXJ0aWNhbFBvc2l0aW9uOiBcImJvdHRvbVwiLFxyXG4gICAgcGFuZWxDbGFzczogW1wiY29yYWQtc25hY2stYmFyLWNvbnRhaW5lclwiXSxcclxuICB9O1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IHNuYWNrQmFyOiBNYXRTbmFja0Jhcikge31cclxuXHJcbiAgc2hvd01lc3NhZ2UoXHJcbiAgICBtZXNzYWdlOiBzdHJpbmcsXHJcbiAgICBzdGF0dXM6IFRvYXN0VHlwZSA9IFwiaW5mb1wiLFxyXG4gICAgYWN0aW9uPzogKCkgPT4gdm9pZCB8IG51bGwsXHJcbiAgICBhY3Rpb25UZXh0Pzogc3RyaW5nXHJcbiAgKTogTWF0U25hY2tCYXJSZWY8VG9hc3RTbmFja2JhckNvbXBvbmVudD4ge1xyXG4gICAgY29uc3QgZGF0YTogVG9hc3REYXRhID0ge1xyXG4gICAgICBtZXNzYWdlLFxyXG4gICAgICBzdGF0dXMsXHJcbiAgICAgIGFjdGlvbixcclxuICAgICAgYWN0aW9uVGV4dCxcclxuICAgIH07XHJcbiAgICByZXR1cm4gdGhpcy5zbmFja0Jhci5vcGVuRnJvbUNvbXBvbmVudDxUb2FzdFNuYWNrYmFyQ29tcG9uZW50PihcclxuICAgICAgVG9hc3RTbmFja2JhckNvbXBvbmVudCxcclxuICAgICAge1xyXG4gICAgICAgIC4uLnRoaXMuY29uZmlnLFxyXG4gICAgICAgIHBhbmVsQ2xhc3M6IFsuLi50aGlzLmNvbmZpZy5wYW5lbENsYXNzLCBzdGF0dXNdLFxyXG4gICAgICAgIGRhdGEsXHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiJdfQ==