import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ToastSnackbarComponent } from "./components/toast-snackbar/toast-snackbar.component";
import { ToastService } from "./services/toast.service";
import * as i0 from "@angular/core";
export class ToastModule {
}
ToastModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
ToastModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, declarations: [ToastSnackbarComponent], imports: [CommonModule, MatSnackBarModule] });
ToastModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, providers: [ToastService], imports: [CommonModule, MatSnackBarModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: ToastModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ToastSnackbarComponent],
                    imports: [CommonModule, MatSnackBarModule],
                    providers: [ToastService],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL3NlcnZpY2VzL3RvYXN0L3RvYXN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUM5RixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7O0FBT3hELE1BQU0sT0FBTyxXQUFXOzt3R0FBWCxXQUFXO3lHQUFYLFdBQVcsaUJBSlAsc0JBQXNCLGFBQzNCLFlBQVksRUFBRSxpQkFBaUI7eUdBRzlCLFdBQVcsYUFGWCxDQUFDLFlBQVksQ0FBQyxZQURmLFlBQVksRUFBRSxpQkFBaUI7MkZBRzlCLFdBQVc7a0JBTHZCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsc0JBQXNCLENBQUM7b0JBQ3RDLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxpQkFBaUIsQ0FBQztvQkFDMUMsU0FBUyxFQUFFLENBQUMsWUFBWSxDQUFDO2lCQUMxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBNYXRTbmFja0Jhck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXJcIjtcclxuaW1wb3J0IHsgVG9hc3RTbmFja2JhckNvbXBvbmVudCB9IGZyb20gXCIuL2NvbXBvbmVudHMvdG9hc3Qtc25hY2tiYXIvdG9hc3Qtc25hY2tiYXIuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFRvYXN0U2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL3RvYXN0LnNlcnZpY2VcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbVG9hc3RTbmFja2JhckNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgTWF0U25hY2tCYXJNb2R1bGVdLFxyXG4gIHByb3ZpZGVyczogW1RvYXN0U2VydmljZV0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUb2FzdE1vZHVsZSB7fVxyXG4iXX0=