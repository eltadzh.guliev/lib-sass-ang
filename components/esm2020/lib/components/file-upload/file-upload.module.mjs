import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FileUploadItemComponent } from './components/file-upload-item/file-upload-item.component';
import { LibsPipesModule } from '../../pipes/libs-pipes.module';
import { FileSizePipe } from '../../pipes/file-size.pipe';
import { ToastModule } from '../../services/toast/toast.module';
import * as i0 from "@angular/core";
export class FileUploadModule {
}
FileUploadModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
FileUploadModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, declarations: [FileUploadComponent, FileUploadItemComponent], imports: [CommonModule, LibsPipesModule, ToastModule], exports: [FileUploadComponent] });
FileUploadModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, providers: [FileSizePipe], imports: [CommonModule, LibsPipesModule, ToastModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FileUploadModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [FileUploadComponent, FileUploadItemComponent],
                    imports: [CommonModule, LibsPipesModule, ToastModule],
                    providers: [FileSizePipe],
                    exports: [FileUploadComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11cGxvYWQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2NvbXBvbmVudHMvZmlsZS11cGxvYWQvZmlsZS11cGxvYWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ25HLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1DQUFtQyxDQUFDOztBQVFoRSxNQUFNLE9BQU8sZ0JBQWdCOzs2R0FBaEIsZ0JBQWdCOzhHQUFoQixnQkFBZ0IsaUJBTFosbUJBQW1CLEVBQUUsdUJBQXVCLGFBQ2pELFlBQVksRUFBRSxlQUFlLEVBQUUsV0FBVyxhQUUxQyxtQkFBbUI7OEdBRWxCLGdCQUFnQixhQUhoQixDQUFDLFlBQVksQ0FBQyxZQURmLFlBQVksRUFBRSxlQUFlLEVBQUUsV0FBVzsyRkFJekMsZ0JBQWdCO2tCQU41QixRQUFRO21CQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLG1CQUFtQixFQUFFLHVCQUF1QixDQUFDO29CQUM1RCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsZUFBZSxFQUFFLFdBQVcsQ0FBQztvQkFDckQsU0FBUyxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUN6QixPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztpQkFDL0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGaWxlVXBsb2FkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZpbGVVcGxvYWRJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZpbGUtdXBsb2FkLWl0ZW0vZmlsZS11cGxvYWQtaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMaWJzUGlwZXNNb2R1bGUgfSBmcm9tICcuLi8uLi9waXBlcy9saWJzLXBpcGVzLm1vZHVsZSc7XHJcbmltcG9ydCB7IEZpbGVTaXplUGlwZSB9IGZyb20gJy4uLy4uL3BpcGVzL2ZpbGUtc2l6ZS5waXBlJztcclxuaW1wb3J0IHsgVG9hc3RNb2R1bGUgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90b2FzdC90b2FzdC5tb2R1bGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtGaWxlVXBsb2FkQ29tcG9uZW50LCBGaWxlVXBsb2FkSXRlbUNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgTGlic1BpcGVzTW9kdWxlLCBUb2FzdE1vZHVsZV0sXHJcbiAgcHJvdmlkZXJzOiBbRmlsZVNpemVQaXBlXSxcclxuICBleHBvcnRzOiBbRmlsZVVwbG9hZENvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkTW9kdWxlIHt9XHJcbiJdfQ==