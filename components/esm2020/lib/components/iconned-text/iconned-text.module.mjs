import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconnedTextComponent } from './components/iconned-text/iconned-text.component';
import * as i0 from "@angular/core";
export class IconnedTextModule {
}
IconnedTextModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
IconnedTextModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, declarations: [IconnedTextComponent], imports: [CommonModule], exports: [IconnedTextComponent] });
IconnedTextModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: IconnedTextModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [IconnedTextComponent],
                    imports: [CommonModule],
                    exports: [IconnedTextComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbm5lZC10ZXh0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL2ljb25uZWQtdGV4dC9pY29ubmVkLXRleHQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOztBQU94RixNQUFNLE9BQU8saUJBQWlCOzs4R0FBakIsaUJBQWlCOytHQUFqQixpQkFBaUIsaUJBSmIsb0JBQW9CLGFBQ3pCLFlBQVksYUFDWixvQkFBb0I7K0dBRW5CLGlCQUFpQixZQUhsQixZQUFZOzJGQUdYLGlCQUFpQjtrQkFMN0IsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDcEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUN2QixPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBJY29ubmVkVGV4dENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9pY29ubmVkLXRleHQvaWNvbm5lZC10ZXh0LmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0ljb25uZWRUZXh0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcclxuICBleHBvcnRzOiBbSWNvbm5lZFRleHRDb21wb25lbnRdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgSWNvbm5lZFRleHRNb2R1bGUge31cclxuIl19