import { Component, Input } from "@angular/core";
import * as i0 from "@angular/core";
export class PaginationInfoComponent {
    constructor() { }
    ngOnInit() { }
}
PaginationInfoComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationInfoComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PaginationInfoComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: PaginationInfoComponent, selector: "app-pagination-info", inputs: { pageFrom: "pageFrom", pageTo: "pageTo", totalItemCount: "totalItemCount" }, ngImport: i0, template: "{{pageFrom}}-{{pageTo}} {{totalItemCount}}-dan", styles: [":host{font-size:14px;line-height:24px;color:#000}\n"] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PaginationInfoComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-pagination-info", template: "{{pageFrom}}-{{pageTo}} {{totalItemCount}}-dan", styles: [":host{font-size:14px;line-height:24px;color:#000}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { pageFrom: [{
                type: Input
            }], pageTo: [{
                type: Input
            }], totalItemCount: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi1pbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL3BhZ2luYXRpb24vY29tcG9uZW50cy9wYWdpbmF0aW9uLWluZm8vcGFnaW5hdGlvbi1pbmZvLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL3BhZ2luYXRpb24vY29tcG9uZW50cy9wYWdpbmF0aW9uLWluZm8vcGFnaW5hdGlvbi1pbmZvLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDOztBQU96RCxNQUFNLE9BQU8sdUJBQXVCO0lBS2xDLGdCQUFlLENBQUM7SUFFaEIsUUFBUSxLQUFVLENBQUM7O29IQVBSLHVCQUF1Qjt3R0FBdkIsdUJBQXVCLGlKQ1BwQyxnREFBOEM7MkZET2pDLHVCQUF1QjtrQkFMbkMsU0FBUzsrQkFDRSxxQkFBcUI7MEVBS3RCLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0csTUFBTTtzQkFBZCxLQUFLO2dCQUNHLGNBQWM7c0JBQXRCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiYXBwLXBhZ2luYXRpb24taW5mb1wiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vcGFnaW5hdGlvbi1pbmZvLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3BhZ2luYXRpb24taW5mby5jb21wb25lbnQuc2Fzc1wiXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFBhZ2luYXRpb25JbmZvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBwYWdlRnJvbTogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIHBhZ2VUbzogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIHRvdGFsSXRlbUNvdW50OiBudW1iZXI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge31cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7fVxyXG59XHJcbiIsInt7cGFnZUZyb219fS17e3BhZ2VUb319IHt7dG90YWxJdGVtQ291bnR9fS1kYW4iXX0=