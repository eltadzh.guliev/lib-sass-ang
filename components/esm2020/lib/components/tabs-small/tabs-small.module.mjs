import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsSmallComponent } from './components/tabs-small/tabs-small.component';
import * as i0 from "@angular/core";
export class TabsSmallModule {
}
TabsSmallModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
TabsSmallModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, declarations: [TabsSmallComponent], imports: [CommonModule], exports: [TabsSmallComponent] });
TabsSmallModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TabsSmallComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        TabsSmallComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy1zbWFsbC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy90YWJzLXNtYWxsL3RhYnMtc21hbGwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDOztBQWFsRixNQUFNLE9BQU8sZUFBZTs7NEdBQWYsZUFBZTs2R0FBZixlQUFlLGlCQVR4QixrQkFBa0IsYUFHbEIsWUFBWSxhQUdaLGtCQUFrQjs2R0FHVCxlQUFlLFlBTnhCLFlBQVk7MkZBTUgsZUFBZTtrQkFYM0IsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osa0JBQWtCO3FCQUNuQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsWUFBWTtxQkFDYjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1Asa0JBQWtCO3FCQUNuQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFRhYnNTbWFsbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy90YWJzLXNtYWxsL3RhYnMtc21hbGwuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBUYWJzU21hbGxDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgVGFic1NtYWxsQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFic1NtYWxsTW9kdWxlIHsgfVxyXG4iXX0=