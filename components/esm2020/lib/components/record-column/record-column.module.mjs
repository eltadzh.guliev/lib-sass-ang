import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RecordColumnComponent } from './components/record-column/record-column.component';
import * as i0 from "@angular/core";
export class RecordColumnModule {
}
RecordColumnModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
RecordColumnModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, declarations: [RecordColumnComponent], imports: [CommonModule, ReactiveFormsModule], exports: [RecordColumnComponent] });
RecordColumnModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, imports: [CommonModule, ReactiveFormsModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: RecordColumnModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [RecordColumnComponent],
                    imports: [CommonModule, ReactiveFormsModule],
                    exports: [RecordColumnComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVjb3JkLWNvbHVtbi5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy9yZWNvcmQtY29sdW1uL3JlY29yZC1jb2x1bW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDOztBQU8zRixNQUFNLE9BQU8sa0JBQWtCOzsrR0FBbEIsa0JBQWtCO2dIQUFsQixrQkFBa0IsaUJBSmQscUJBQXFCLGFBQzFCLFlBQVksRUFBRSxtQkFBbUIsYUFDakMscUJBQXFCO2dIQUVwQixrQkFBa0IsWUFIbkIsWUFBWSxFQUFFLG1CQUFtQjsyRkFHaEMsa0JBQWtCO2tCQUw5QixRQUFRO21CQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO29CQUNyQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLENBQUM7b0JBQzVDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO2lCQUNqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFJlY29yZENvbHVtbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9yZWNvcmQtY29sdW1uL3JlY29yZC1jb2x1bW4uY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbUmVjb3JkQ29sdW1uQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlXSxcclxuICBleHBvcnRzOiBbUmVjb3JkQ29sdW1uQ29tcG9uZW50XSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFJlY29yZENvbHVtbk1vZHVsZSB7fVxyXG4iXX0=