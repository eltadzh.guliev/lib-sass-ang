import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelInfoComponent } from './components/panel-info/panel-info.component';
import * as i0 from "@angular/core";
export class PanelInfoModule {
}
PanelInfoModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
PanelInfoModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, declarations: [PanelInfoComponent], imports: [CommonModule], exports: [PanelInfoComponent] });
PanelInfoModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [PanelInfoComponent],
                    imports: [CommonModule],
                    exports: [PanelInfoComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtaW5mby5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy9wYW5lbC1pbmZvL3BhbmVsLWluZm8ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDOztBQU9sRixNQUFNLE9BQU8sZUFBZTs7NEdBQWYsZUFBZTs2R0FBZixlQUFlLGlCQUpYLGtCQUFrQixhQUN2QixZQUFZLGFBQ1osa0JBQWtCOzZHQUVqQixlQUFlLFlBSGhCLFlBQVk7MkZBR1gsZUFBZTtrQkFMM0IsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztvQkFDbEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUN2QixPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztpQkFDOUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBQYW5lbEluZm9Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcGFuZWwtaW5mby9wYW5lbC1pbmZvLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1BhbmVsSW5mb0NvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXHJcbiAgZXhwb3J0czogW1BhbmVsSW5mb0NvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYW5lbEluZm9Nb2R1bGUge31cclxuIl19