import { Component, Input, ChangeDetectionStrategy, HostBinding, } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class PanelInfoComponent {
    set backgroundColorName(backgroundColorName) {
        this.backgroundColorBrand = backgroundColorName === 'brand';
        this.backgroundColorDefault =
            backgroundColorName === 'default' || !backgroundColorName;
    }
    constructor() {
        this.isDescriptionHTML = false;
        this.backgroundColorDefault = false;
        this.backgroundColorBrand = true;
    }
    ngOnInit() { }
}
PanelInfoComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PanelInfoComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: PanelInfoComponent, selector: "app-panel-info", inputs: { iconName: "iconName", title: "title", description: "description", backgroundColorName: "backgroundColorName", isDescriptionHTML: "isDescriptionHTML" }, host: { properties: { "class.panel-info_default": "this.backgroundColorDefault", "class.panel-info_brand": "this.backgroundColorBrand" } }, ngImport: i0, template: "<img src=\"./assets/icons/{{ iconName }}.svg\" class=\"panel-info__img\" />\r\n\r\n<span class=\"panel-info__title\">{{ title }}</span>\r\n\r\n<span *ngIf=\"!isDescriptionHTML\" class=\"panel-info__description\">{{\r\n  description\r\n}}</span>\r\n\r\n<div\r\n  *ngIf=\"isDescriptionHTML\"\r\n  [innerHTML]=\"description\"\r\n  class=\"panel-info__html-description\"\r\n></div>\r\n", styles: [":host{display:block;padding:16px 40px 24px;border-radius:12px}:host.panel-info_default{background-color:#f7f7f7}:host.panel-info_brand{background-color:#e66e4b1a}:host .panel-info__img{display:block;width:24px;height:24px;margin-bottom:16px}:host .panel-info__title{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;margin-bottom:10px}:host .panel-info__description{display:block;font-size:14px;font-weight:400;line-height:20px;color:#222}:host .panel-info__html-description{font-size:14px}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], changeDetection: i0.ChangeDetectionStrategy.OnPush });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: PanelInfoComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-panel-info', changeDetection: ChangeDetectionStrategy.OnPush, template: "<img src=\"./assets/icons/{{ iconName }}.svg\" class=\"panel-info__img\" />\r\n\r\n<span class=\"panel-info__title\">{{ title }}</span>\r\n\r\n<span *ngIf=\"!isDescriptionHTML\" class=\"panel-info__description\">{{\r\n  description\r\n}}</span>\r\n\r\n<div\r\n  *ngIf=\"isDescriptionHTML\"\r\n  [innerHTML]=\"description\"\r\n  class=\"panel-info__html-description\"\r\n></div>\r\n", styles: [":host{display:block;padding:16px 40px 24px;border-radius:12px}:host.panel-info_default{background-color:#f7f7f7}:host.panel-info_brand{background-color:#e66e4b1a}:host .panel-info__img{display:block;width:24px;height:24px;margin-bottom:16px}:host .panel-info__title{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;margin-bottom:10px}:host .panel-info__description{display:block;font-size:14px;font-weight:400;line-height:20px;color:#222}:host .panel-info__html-description{font-size:14px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { iconName: [{
                type: Input
            }], title: [{
                type: Input
            }], description: [{
                type: Input
            }], backgroundColorName: [{
                type: Input
            }], isDescriptionHTML: [{
                type: Input
            }], backgroundColorDefault: [{
                type: HostBinding,
                args: ['class.panel-info_default']
            }], backgroundColorBrand: [{
                type: HostBinding,
                args: ['class.panel-info_brand']
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtaW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy9wYW5lbC1pbmZvL2NvbXBvbmVudHMvcGFuZWwtaW5mby9wYW5lbC1pbmZvLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL3BhbmVsLWluZm8vY29tcG9uZW50cy9wYW5lbC1pbmZvL3BhbmVsLWluZm8uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFFVCxLQUFLLEVBQ0wsdUJBQXVCLEVBQ3ZCLFdBQVcsR0FDWixNQUFNLGVBQWUsQ0FBQzs7O0FBUXZCLE1BQU0sT0FBTyxrQkFBa0I7SUFLN0IsSUFBYSxtQkFBbUIsQ0FBQyxtQkFBd0M7UUFDdkUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLG1CQUFtQixLQUFLLE9BQU8sQ0FBQztRQUM1RCxJQUFJLENBQUMsc0JBQXNCO1lBQ3pCLG1CQUFtQixLQUFLLFNBQVMsSUFBSSxDQUFDLG1CQUFtQixDQUFDO0lBQzlELENBQUM7SUFPRDtRQUxTLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUVNLDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUNqQyx5QkFBb0IsR0FBRyxJQUFJLENBQUM7SUFFcEQsQ0FBQztJQUVoQixRQUFRLEtBQVUsQ0FBQzs7K0dBbEJSLGtCQUFrQjttR0FBbEIsa0JBQWtCLG9XQ2QvQiwrWEFhQTsyRkRDYSxrQkFBa0I7a0JBTjlCLFNBQVM7K0JBQ0UsZ0JBQWdCLG1CQUdULHVCQUF1QixDQUFDLE1BQU07MEVBR3RDLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBRU8sbUJBQW1CO3NCQUEvQixLQUFLO2dCQU1HLGlCQUFpQjtzQkFBekIsS0FBSztnQkFFbUMsc0JBQXNCO3NCQUE5RCxXQUFXO3VCQUFDLDBCQUEwQjtnQkFDQSxvQkFBb0I7c0JBQTFELFdBQVc7dUJBQUMsd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIElucHV0LFxyXG4gIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gIEhvc3RCaW5kaW5nLFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtcGFuZWwtaW5mbycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3BhbmVsLWluZm8uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3BhbmVsLWluZm8uY29tcG9uZW50LnNhc3MnXSxcclxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxufSlcclxuZXhwb3J0IGNsYXNzIFBhbmVsSW5mb0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgaWNvbk5hbWU6IHN0cmluZztcclxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGRlc2NyaXB0aW9uOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpIHNldCBiYWNrZ3JvdW5kQ29sb3JOYW1lKGJhY2tncm91bmRDb2xvck5hbWU6ICdkZWZhdWx0JyB8ICdicmFuZCcpIHtcclxuICAgIHRoaXMuYmFja2dyb3VuZENvbG9yQnJhbmQgPSBiYWNrZ3JvdW5kQ29sb3JOYW1lID09PSAnYnJhbmQnO1xyXG4gICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3JEZWZhdWx0ID1cclxuICAgICAgYmFja2dyb3VuZENvbG9yTmFtZSA9PT0gJ2RlZmF1bHQnIHx8ICFiYWNrZ3JvdW5kQ29sb3JOYW1lO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KCkgaXNEZXNjcmlwdGlvbkhUTUwgPSBmYWxzZTtcclxuXHJcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5wYW5lbC1pbmZvX2RlZmF1bHQnKSBiYWNrZ3JvdW5kQ29sb3JEZWZhdWx0ID0gZmFsc2U7XHJcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5wYW5lbC1pbmZvX2JyYW5kJykgYmFja2dyb3VuZENvbG9yQnJhbmQgPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge31cclxufVxyXG4iLCI8aW1nIHNyYz1cIi4vYXNzZXRzL2ljb25zL3t7IGljb25OYW1lIH19LnN2Z1wiIGNsYXNzPVwicGFuZWwtaW5mb19faW1nXCIgLz5cclxuXHJcbjxzcGFuIGNsYXNzPVwicGFuZWwtaW5mb19fdGl0bGVcIj57eyB0aXRsZSB9fTwvc3Bhbj5cclxuXHJcbjxzcGFuICpuZ0lmPVwiIWlzRGVzY3JpcHRpb25IVE1MXCIgY2xhc3M9XCJwYW5lbC1pbmZvX19kZXNjcmlwdGlvblwiPnt7XHJcbiAgZGVzY3JpcHRpb25cclxufX08L3NwYW4+XHJcblxyXG48ZGl2XHJcbiAgKm5nSWY9XCJpc0Rlc2NyaXB0aW9uSFRNTFwiXHJcbiAgW2lubmVySFRNTF09XCJkZXNjcmlwdGlvblwiXHJcbiAgY2xhc3M9XCJwYW5lbC1pbmZvX19odG1sLWRlc2NyaXB0aW9uXCJcclxuPjwvZGl2PlxyXG4iXX0=