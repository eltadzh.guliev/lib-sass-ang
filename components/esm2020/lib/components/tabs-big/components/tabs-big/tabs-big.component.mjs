import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class TabsBigComponent {
    constructor() {
        this.selectedIdChange = new EventEmitter();
    }
    ngOnInit() { }
    onTabClick(id) {
        this.selectedId = id;
        this.selectedIdChange.emit(id);
    }
}
TabsBigComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TabsBigComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: TabsBigComponent, selector: "app-tabs-big", inputs: { tabs: "tabs", selectedId: "selectedId" }, outputs: { selectedIdChange: "selectedIdChange" }, ngImport: i0, template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-big__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center;flex-wrap:wrap}:host .tabs-big__button{display:block;font-size:20px;font-weight:600;line-height:32px;color:#999;cursor:pointer;background-color:unset;border:none;margin-right:24px}:host .tabs-big__button:last-child{margin-right:0}:host .tabs-big__button:hover{color:#838282}:host .tabs-big__button.active{color:#222}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsBigComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-tabs-big', template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-big__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:flex;align-items:center;flex-wrap:wrap}:host .tabs-big__button{display:block;font-size:20px;font-weight:600;line-height:32px;color:#999;cursor:pointer;background-color:unset;border:none;margin-right:24px}:host .tabs-big__button:last-child{margin-right:0}:host .tabs-big__button:hover{color:#838282}:host .tabs-big__button.active{color:#222}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { tabs: [{
                type: Input
            }], selectedId: [{
                type: Input
            }], selectedIdChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy1iaWcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2NvbXBvbmVudHMvdGFicy1iaWcvY29tcG9uZW50cy90YWJzLWJpZy90YWJzLWJpZy5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy90YWJzLWJpZy9jb21wb25lbnRzL3RhYnMtYmlnL3RhYnMtYmlnLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQU8vRSxNQUFNLE9BQU8sZ0JBQWdCO0lBTTNCO1FBRlUscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztJQUV6QyxDQUFDO0lBRWhCLFFBQVEsS0FBVSxDQUFDO0lBRW5CLFVBQVUsQ0FBQyxFQUFVO1FBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7NkdBYlUsZ0JBQWdCO2lHQUFoQixnQkFBZ0IsMkpDUDdCLHFNQVFBOzJGRERhLGdCQUFnQjtrQkFMNUIsU0FBUzsrQkFDRSxjQUFjOzBFQUtmLElBQUk7c0JBQVosS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUVJLGdCQUFnQjtzQkFBekIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRhYnMtYmlnJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdGFicy1iaWcuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3RhYnMtYmlnLmNvbXBvbmVudC5zYXNzJ10sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJzQmlnQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSB0YWJzO1xyXG4gIEBJbnB1dCgpIHNlbGVjdGVkSWQ6IHN0cmluZztcclxuXHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkSWRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7fVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gIG9uVGFiQ2xpY2soaWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5zZWxlY3RlZElkID0gaWQ7XHJcbiAgICB0aGlzLnNlbGVjdGVkSWRDaGFuZ2UuZW1pdChpZCk7XHJcbiAgfVxyXG59XHJcbiIsIjxidXR0b25cclxuICAqbmdGb3I9XCJsZXQgdGFiIG9mIHRhYnNcIlxyXG4gIGNsYXNzPVwidGFicy1iaWdfX2J1dHRvblwiXHJcbiAgW2NsYXNzLmFjdGl2ZV09XCJ0YWIuaWQgPT09IHNlbGVjdGVkSWRcIlxyXG4gIChjbGljayk9XCJvblRhYkNsaWNrKHRhYi5pZClcIlxyXG4+XHJcbiAge3sgdGFiLnRleHQgfX1cclxuPC9idXR0b24+XHJcbiJdfQ==