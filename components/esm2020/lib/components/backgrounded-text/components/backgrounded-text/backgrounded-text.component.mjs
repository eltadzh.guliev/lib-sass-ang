import { Component, HostBinding, Input } from "@angular/core";
import * as i0 from "@angular/core";
export class BackgroundedTextComponent {
    set type(value) {
        this.innerType = value;
        this.resetTypeClass();
        this.setTypeClass();
    }
    constructor() { }
    resetTypeClass() {
        this.successTypeClass = false;
        this.dangerTypeClass = false;
        this.warningTypeClass = false;
    }
    setTypeClass() {
        switch (this.innerType) {
            case "success":
                this.successTypeClass = true;
                break;
            case "danger":
                this.dangerTypeClass = true;
                break;
            case "warning":
                this.warningTypeClass = true;
        }
    }
}
BackgroundedTextComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
BackgroundedTextComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: BackgroundedTextComponent, selector: "app-backgrounded-text", inputs: { text: "text", type: "type" }, host: { properties: { "class.backgrounded-text_success": "this.successTypeClass", "class.backgrounded-text_warning": "this.warningTypeClass", "class.backgrounded-text_danger": "this.dangerTypeClass" } }, ngImport: i0, template: "{{text}}", styles: [":host{display:inline-block;padding:8px 10px;font-weight:500;font-size:11px;line-height:14px;border-radius:6px}:host.backgrounded-text_success{background-color:#55a9321a;color:#55a932}:host.backgrounded-text_warning{background-color:#f9a0001a;color:#f9a000}:host.backgrounded-text_danger{background-color:#d3292d1a;color:#d3292d}\n"] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextComponent, decorators: [{
            type: Component,
            args: [{ selector: "app-backgrounded-text", template: "{{text}}", styles: [":host{display:inline-block;padding:8px 10px;font-weight:500;font-size:11px;line-height:14px;border-radius:6px}:host.backgrounded-text_success{background-color:#55a9321a;color:#55a932}:host.backgrounded-text_warning{background-color:#f9a0001a;color:#f9a000}:host.backgrounded-text_danger{background-color:#d3292d1a;color:#d3292d}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { text: [{
                type: Input
            }], type: [{
                type: Input
            }], successTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_success"]
            }], warningTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_warning"]
            }], dangerTypeClass: [{
                type: HostBinding,
                args: ["class.backgrounded-text_danger"]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFja2dyb3VuZGVkLXRleHQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2NvbXBvbmVudHMvYmFja2dyb3VuZGVkLXRleHQvY29tcG9uZW50cy9iYWNrZ3JvdW5kZWQtdGV4dC9iYWNrZ3JvdW5kZWQtdGV4dC5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvY29tcG9uZW50cy9iYWNrZ3JvdW5kZWQtdGV4dC9jb21wb25lbnRzL2JhY2tncm91bmRlZC10ZXh0L2JhY2tncm91bmRlZC10ZXh0LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFPOUQsTUFBTSxPQUFPLHlCQUF5QjtJQUVwQyxJQUFhLElBQUksQ0FBQyxLQUF1QztRQUN2RCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUV2QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFRRCxnQkFBZSxDQUFDO0lBRVIsY0FBYztRQUNwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVPLFlBQVk7UUFDbEIsUUFBUSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3RCLEtBQUssU0FBUztnQkFDWixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7SUFDSCxDQUFDOztzSEFsQ1UseUJBQXlCOzBHQUF6Qix5QkFBeUIsaVRDUHRDLFVBQVE7MkZET0sseUJBQXlCO2tCQUxyQyxTQUFTOytCQUNFLHVCQUF1QjswRUFLeEIsSUFBSTtzQkFBWixLQUFLO2dCQUNPLElBQUk7c0JBQWhCLEtBQUs7Z0JBUzBDLGdCQUFnQjtzQkFBL0QsV0FBVzt1QkFBQyxpQ0FBaUM7Z0JBQ0UsZ0JBQWdCO3NCQUEvRCxXQUFXO3VCQUFDLGlDQUFpQztnQkFDQyxlQUFlO3NCQUE3RCxXQUFXO3VCQUFDLGdDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImFwcC1iYWNrZ3JvdW5kZWQtdGV4dFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vYmFja2dyb3VuZGVkLXRleHQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vYmFja2dyb3VuZGVkLXRleHQuY29tcG9uZW50LnNhc3NcIl0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCYWNrZ3JvdW5kZWRUZXh0Q29tcG9uZW50IHtcclxuICBASW5wdXQoKSB0ZXh0OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc2V0IHR5cGUodmFsdWU6IFwic3VjY2Vzc1wiIHwgXCJ3YXJuaW5nXCIgfCBcImRhbmdlclwiKSB7XHJcbiAgICB0aGlzLmlubmVyVHlwZSA9IHZhbHVlO1xyXG5cclxuICAgIHRoaXMucmVzZXRUeXBlQ2xhc3MoKTtcclxuICAgIHRoaXMuc2V0VHlwZUNsYXNzKCk7XHJcbiAgfVxyXG5cclxuICBpbm5lclR5cGU6IFwic3VjY2Vzc1wiIHwgXCJ3YXJuaW5nXCIgfCBcImRhbmdlclwiO1xyXG5cclxuICBASG9zdEJpbmRpbmcoXCJjbGFzcy5iYWNrZ3JvdW5kZWQtdGV4dF9zdWNjZXNzXCIpIHN1Y2Nlc3NUeXBlQ2xhc3M6IGJvb2xlYW47XHJcbiAgQEhvc3RCaW5kaW5nKFwiY2xhc3MuYmFja2dyb3VuZGVkLXRleHRfd2FybmluZ1wiKSB3YXJuaW5nVHlwZUNsYXNzOiBib29sZWFuO1xyXG4gIEBIb3N0QmluZGluZyhcImNsYXNzLmJhY2tncm91bmRlZC10ZXh0X2RhbmdlclwiKSBkYW5nZXJUeXBlQ2xhc3M6IGJvb2xlYW47XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge31cclxuXHJcbiAgcHJpdmF0ZSByZXNldFR5cGVDbGFzcygpOiB2b2lkIHtcclxuICAgIHRoaXMuc3VjY2Vzc1R5cGVDbGFzcyA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYW5nZXJUeXBlQ2xhc3MgPSBmYWxzZTtcclxuICAgIHRoaXMud2FybmluZ1R5cGVDbGFzcyA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRUeXBlQ2xhc3MoKTogdm9pZCB7XHJcbiAgICBzd2l0Y2ggKHRoaXMuaW5uZXJUeXBlKSB7XHJcbiAgICAgIGNhc2UgXCJzdWNjZXNzXCI6XHJcbiAgICAgICAgdGhpcy5zdWNjZXNzVHlwZUNsYXNzID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBcImRhbmdlclwiOlxyXG4gICAgICAgIHRoaXMuZGFuZ2VyVHlwZUNsYXNzID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBcIndhcm5pbmdcIjpcclxuICAgICAgICB0aGlzLndhcm5pbmdUeXBlQ2xhc3MgPSB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCJ7e3RleHR9fSJdfQ==