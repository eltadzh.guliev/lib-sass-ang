import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BackgroundedTextComponent } from "./components/backgrounded-text/backgrounded-text.component";
import * as i0 from "@angular/core";
export class BackgroundedTextModule {
}
BackgroundedTextModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
BackgroundedTextModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, declarations: [BackgroundedTextComponent], imports: [CommonModule], exports: [BackgroundedTextComponent] });
BackgroundedTextModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: BackgroundedTextModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [BackgroundedTextComponent],
                    imports: [CommonModule],
                    exports: [BackgroundedTextComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFja2dyb3VuZGVkLXRleHQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2NvbXBvbmVudHMvYmFja2dyb3VuZGVkLXRleHQvYmFja2dyb3VuZGVkLXRleHQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDOztBQU92RyxNQUFNLE9BQU8sc0JBQXNCOzttSEFBdEIsc0JBQXNCO29IQUF0QixzQkFBc0IsaUJBSmxCLHlCQUF5QixhQUM5QixZQUFZLGFBQ1oseUJBQXlCO29IQUV4QixzQkFBc0IsWUFIdkIsWUFBWTsyRkFHWCxzQkFBc0I7a0JBTGxDLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMseUJBQXlCLENBQUM7b0JBQ3pDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsT0FBTyxFQUFFLENBQUMseUJBQXlCLENBQUM7aUJBQ3JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IEJhY2tncm91bmRlZFRleHRDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL2JhY2tncm91bmRlZC10ZXh0L2JhY2tncm91bmRlZC10ZXh0LmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtCYWNrZ3JvdW5kZWRUZXh0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcclxuICBleHBvcnRzOiBbQmFja2dyb3VuZGVkVGV4dENvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCYWNrZ3JvdW5kZWRUZXh0TW9kdWxlIHt9XHJcbiJdfQ==