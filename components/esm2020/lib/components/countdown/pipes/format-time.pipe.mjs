import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FormatTimePipe {
    transform(value) {
        const minutes = Math.floor(value / 60);
        return (('00' + minutes).slice(-2) +
            ':' +
            ('00' + Math.floor(value - minutes * 60)).slice(-2));
    }
}
FormatTimePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
FormatTimePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, name: "formatTime" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: FormatTimePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'formatTime',
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRpbWUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL2NvdW50ZG93bi9waXBlcy9mb3JtYXQtdGltZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUtwRCxNQUFNLE9BQU8sY0FBYztJQUN6QixTQUFTLENBQUMsS0FBYTtRQUNyQixNQUFNLE9BQU8sR0FBVyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMvQyxPQUFPLENBQ0wsQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLEdBQUc7WUFDSCxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FDcEQsQ0FBQztJQUNKLENBQUM7OzJHQVJVLGNBQWM7eUdBQWQsY0FBYzsyRkFBZCxjQUFjO2tCQUgxQixJQUFJO21CQUFDO29CQUNKLElBQUksRUFBRSxZQUFZO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnZm9ybWF0VGltZScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtYXRUaW1lUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gIHRyYW5zZm9ybSh2YWx1ZTogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IG1pbnV0ZXM6IG51bWJlciA9IE1hdGguZmxvb3IodmFsdWUgLyA2MCk7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAoJzAwJyArIG1pbnV0ZXMpLnNsaWNlKC0yKSArXHJcbiAgICAgICc6JyArXHJcbiAgICAgICgnMDAnICsgTWF0aC5mbG9vcih2YWx1ZSAtIG1pbnV0ZXMgKiA2MCkpLnNsaWNlKC0yKVxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuIl19