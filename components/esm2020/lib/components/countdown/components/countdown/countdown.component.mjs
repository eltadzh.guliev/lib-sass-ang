import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { timer } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "angular-svg-round-progressbar";
import * as i2 from "../../pipes/format-time.pipe";
let CountdownComponent = class CountdownComponent {
    set counterMinutes(value) {
        this.currentCounterMinutes = value;
        this.maxCounterMinutes = value;
    }
    constructor() {
        this.timeIsUp = new EventEmitter();
    }
    ngOnInit() {
        this.startTick();
    }
    startTick() {
        timer(0, 1000)
            .pipe(untilDestroyed(this))
            .subscribe(() => {
            if (this.currentCounterMinutes <= 0) {
                this.timeIsUp.emit();
            }
            else {
                this.currentCounterMinutes--;
            }
        });
    }
};
CountdownComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CountdownComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: CountdownComponent, selector: "app-countdown", inputs: { counterMinutes: "counterMinutes" }, outputs: { timeIsUp: "timeIsUp" }, ngImport: i0, template: "<round-progress\r\n  [current]=\"currentCounterMinutes\"\r\n  [max]=\"maxCounterMinutes\"\r\n  [stroke]=\"18\"\r\n  color=\"#E66E4B\"\r\n  background=\"#F5F5F5\"\r\n>\r\n</round-progress>\r\n\r\n<div class=\"countdown-content\">\r\n  <span class=\"countdown-minutes\">{{ currentCounterMinutes | formatTime }}</span>\r\n  <span class=\"countdown-text\">Seconds left</span>\r\n</div>\r\n", styles: [":host{display:inline-block;position:relative}:host round-progress{width:112px!important;height:112px!important;position:unset!important}:host .countdown-content{width:83px;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}:host .countdown-content .countdown-minutes{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;text-align:center}:host .countdown-content .countdown-text{display:block;font-size:11px;font-weight:400;line-height:14px;color:#999;text-align:center}\n"], dependencies: [{ kind: "component", type: i1.RoundProgressComponent, selector: "round-progress", inputs: ["current", "max", "radius", "animation", "animationDelay", "duration", "stroke", "color", "background", "responsive", "clockwise", "semicircle", "rounded"], outputs: ["onRender"] }, { kind: "pipe", type: i2.FormatTimePipe, name: "formatTime" }] });
CountdownComponent = __decorate([
    UntilDestroy()
], CountdownComponent);
export { CountdownComponent };
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-countdown', template: "<round-progress\r\n  [current]=\"currentCounterMinutes\"\r\n  [max]=\"maxCounterMinutes\"\r\n  [stroke]=\"18\"\r\n  color=\"#E66E4B\"\r\n  background=\"#F5F5F5\"\r\n>\r\n</round-progress>\r\n\r\n<div class=\"countdown-content\">\r\n  <span class=\"countdown-minutes\">{{ currentCounterMinutes | formatTime }}</span>\r\n  <span class=\"countdown-text\">Seconds left</span>\r\n</div>\r\n", styles: [":host{display:inline-block;position:relative}:host round-progress{width:112px!important;height:112px!important;position:unset!important}:host .countdown-content{width:83px;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}:host .countdown-content .countdown-minutes{display:block;font-size:16px;font-weight:600;line-height:24px;color:#222;text-align:center}:host .countdown-content .countdown-text{display:block;font-size:11px;font-weight:400;line-height:14px;color:#999;text-align:center}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { counterMinutes: [{
                type: Input
            }], timeIsUp: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRkb3duLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL2NvdW50ZG93bi9jb21wb25lbnRzL2NvdW50ZG93bi9jb3VudGRvd24uY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY29tcG9uZW50cy9zcmMvbGliL2NvbXBvbmVudHMvY291bnRkb3duL2NvbXBvbmVudHMvY291bnRkb3duL2NvdW50ZG93bi5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7QUFRdEIsSUFBTSxrQkFBa0IsR0FBeEIsTUFBTSxrQkFBa0I7SUFDN0IsSUFBYSxjQUFjLENBQUMsS0FBYTtRQUN2QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7SUFDakMsQ0FBQztJQU9EO1FBTFUsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFRLENBQUM7SUFLL0IsQ0FBQztJQUVoQixRQUFRO1FBQ04sSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxTQUFTO1FBQ1AsS0FBSyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUM7YUFDWCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDdEI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFDOUI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7OytHQTNCVSxrQkFBa0I7bUdBQWxCLGtCQUFrQixzSUNWL0IsbVlBYUE7QURIYSxrQkFBa0I7SUFEOUIsWUFBWSxFQUFFO0dBQ0Ysa0JBQWtCLENBNEI5QjtTQTVCWSxrQkFBa0I7MkZBQWxCLGtCQUFrQjtrQkFOOUIsU0FBUzsrQkFDRSxlQUFlOzBFQU1aLGNBQWM7c0JBQTFCLEtBQUs7Z0JBS0ksUUFBUTtzQkFBakIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVW50aWxEZXN0cm95LCB1bnRpbERlc3Ryb3llZCB9IGZyb20gJ0BuZ25lYXQvdW50aWwtZGVzdHJveSc7XHJcbmltcG9ydCB7IHRpbWVyIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1jb3VudGRvd24nLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb3VudGRvd24uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvdW50ZG93bi5jb21wb25lbnQuc2FzcyddLFxyXG59KVxyXG5AVW50aWxEZXN0cm95KClcclxuZXhwb3J0IGNsYXNzIENvdW50ZG93bkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgc2V0IGNvdW50ZXJNaW51dGVzKHZhbHVlOiBudW1iZXIpIHtcclxuICAgIHRoaXMuY3VycmVudENvdW50ZXJNaW51dGVzID0gdmFsdWU7XHJcbiAgICB0aGlzLm1heENvdW50ZXJNaW51dGVzID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICBAT3V0cHV0KCkgdGltZUlzVXAgPSBuZXcgRXZlbnRFbWl0dGVyPHZvaWQ+KCk7XHJcblxyXG4gIGN1cnJlbnRDb3VudGVyTWludXRlczogbnVtYmVyO1xyXG4gIG1heENvdW50ZXJNaW51dGVzOiBudW1iZXI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge31cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnN0YXJ0VGljaygpO1xyXG4gIH1cclxuXHJcbiAgc3RhcnRUaWNrKCk6IHZvaWQge1xyXG4gICAgdGltZXIoMCwgMTAwMClcclxuICAgICAgLnBpcGUodW50aWxEZXN0cm95ZWQodGhpcykpXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRDb3VudGVyTWludXRlcyA8PSAwKSB7XHJcbiAgICAgICAgICB0aGlzLnRpbWVJc1VwLmVtaXQoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5jdXJyZW50Q291bnRlck1pbnV0ZXMtLTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iLCI8cm91bmQtcHJvZ3Jlc3NcclxuICBbY3VycmVudF09XCJjdXJyZW50Q291bnRlck1pbnV0ZXNcIlxyXG4gIFttYXhdPVwibWF4Q291bnRlck1pbnV0ZXNcIlxyXG4gIFtzdHJva2VdPVwiMThcIlxyXG4gIGNvbG9yPVwiI0U2NkU0QlwiXHJcbiAgYmFja2dyb3VuZD1cIiNGNUY1RjVcIlxyXG4+XHJcbjwvcm91bmQtcHJvZ3Jlc3M+XHJcblxyXG48ZGl2IGNsYXNzPVwiY291bnRkb3duLWNvbnRlbnRcIj5cclxuICA8c3BhbiBjbGFzcz1cImNvdW50ZG93bi1taW51dGVzXCI+e3sgY3VycmVudENvdW50ZXJNaW51dGVzIHwgZm9ybWF0VGltZSB9fTwvc3Bhbj5cclxuICA8c3BhbiBjbGFzcz1cImNvdW50ZG93bi10ZXh0XCI+U2Vjb25kcyBsZWZ0PC9zcGFuPlxyXG48L2Rpdj5cclxuIl19