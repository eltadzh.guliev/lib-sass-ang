import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownComponent } from './components/countdown/countdown.component';
import { FormatTimePipe } from './pipes/format-time.pipe';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import * as i0 from "@angular/core";
export class CountdownModule {
}
CountdownModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CountdownModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, declarations: [CountdownComponent, FormatTimePipe], imports: [CommonModule, RoundProgressModule], exports: [CountdownComponent] });
CountdownModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, imports: [CommonModule, RoundProgressModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: CountdownModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [CountdownComponent, FormatTimePipe],
                    imports: [CommonModule, RoundProgressModule],
                    exports: [CountdownComponent],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRkb3duLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9jb21wb25lbnRzL2NvdW50ZG93bi9jb3VudGRvd24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7QUFPcEUsTUFBTSxPQUFPLGVBQWU7OzRHQUFmLGVBQWU7NkdBQWYsZUFBZSxpQkFKWCxrQkFBa0IsRUFBRSxjQUFjLGFBQ3ZDLFlBQVksRUFBRSxtQkFBbUIsYUFDakMsa0JBQWtCOzZHQUVqQixlQUFlLFlBSGhCLFlBQVksRUFBRSxtQkFBbUI7MkZBR2hDLGVBQWU7a0JBTDNCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsY0FBYyxDQUFDO29CQUNsRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLENBQUM7b0JBQzVDLE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUM5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IENvdW50ZG93bkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb3VudGRvd24vY291bnRkb3duLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1hdFRpbWVQaXBlIH0gZnJvbSAnLi9waXBlcy9mb3JtYXQtdGltZS5waXBlJztcclxuaW1wb3J0IHsgUm91bmRQcm9ncmVzc01vZHVsZSB9IGZyb20gJ2FuZ3VsYXItc3ZnLXJvdW5kLXByb2dyZXNzYmFyJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbQ291bnRkb3duQ29tcG9uZW50LCBGb3JtYXRUaW1lUGlwZV0sXHJcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgUm91bmRQcm9ncmVzc01vZHVsZV0sXHJcbiAgZXhwb3J0czogW0NvdW50ZG93bkNvbXBvbmVudF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3VudGRvd25Nb2R1bGUge31cclxuIl19