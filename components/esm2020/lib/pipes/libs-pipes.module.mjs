import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { FileTypePipe } from './file-type.pipe';
import { TextShortenerPipe } from './text-shortener.pipe';
import * as i0 from "@angular/core";
export class LibsPipesModule {
}
LibsPipesModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
LibsPipesModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, declarations: [FileSizePipe, FileTypePipe, TextShortenerPipe], exports: [FileSizePipe, FileTypePipe, TextShortenerPipe] });
LibsPipesModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: LibsPipesModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [FileSizePipe, FileTypePipe, TextShortenerPipe],
                    exports: [FileSizePipe, FileTypePipe, TextShortenerPipe],
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlicy1waXBlcy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jb21wb25lbnRzL3NyYy9saWIvcGlwZXMvbGlicy1waXBlcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDaEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDOztBQU0xRCxNQUFNLE9BQU8sZUFBZTs7NEdBQWYsZUFBZTs2R0FBZixlQUFlLGlCQUhYLFlBQVksRUFBRSxZQUFZLEVBQUUsaUJBQWlCLGFBQ2xELFlBQVksRUFBRSxZQUFZLEVBQUUsaUJBQWlCOzZHQUU1QyxlQUFlOzJGQUFmLGVBQWU7a0JBSjNCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxpQkFBaUIsQ0FBQztvQkFDN0QsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxpQkFBaUIsQ0FBQztpQkFDekQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGaWxlU2l6ZVBpcGUgfSBmcm9tICcuL2ZpbGUtc2l6ZS5waXBlJztcclxuaW1wb3J0IHsgRmlsZVR5cGVQaXBlIH0gZnJvbSAnLi9maWxlLXR5cGUucGlwZSc7XHJcbmltcG9ydCB7IFRleHRTaG9ydGVuZXJQaXBlIH0gZnJvbSAnLi90ZXh0LXNob3J0ZW5lci5waXBlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbRmlsZVNpemVQaXBlLCBGaWxlVHlwZVBpcGUsIFRleHRTaG9ydGVuZXJQaXBlXSxcclxuICBleHBvcnRzOiBbRmlsZVNpemVQaXBlLCBGaWxlVHlwZVBpcGUsIFRleHRTaG9ydGVuZXJQaXBlXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIExpYnNQaXBlc01vZHVsZSB7fVxyXG4iXX0=