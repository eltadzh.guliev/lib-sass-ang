import { Pipe } from "@angular/core";
import * as i0 from "@angular/core";
export class TextShortenerPipe {
    transform(text, length = 10) {
        return text.length > length ? text.substr(0, length) + "..." : text;
    }
}
TextShortenerPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
TextShortenerPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, name: "textShortener" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TextShortenerPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: "textShortener",
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1zaG9ydGVuZXIucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2NvbXBvbmVudHMvc3JjL2xpYi9waXBlcy90ZXh0LXNob3J0ZW5lci5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUtwRCxNQUFNLE9BQU8saUJBQWlCO0lBQzVCLFNBQVMsQ0FBQyxJQUFZLEVBQUUsU0FBaUIsRUFBRTtRQUN6QyxPQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN0RSxDQUFDOzs4R0FIVSxpQkFBaUI7NEdBQWpCLGlCQUFpQjsyRkFBakIsaUJBQWlCO2tCQUg3QixJQUFJO21CQUFDO29CQUNKLElBQUksRUFBRSxlQUFlO2lCQUN0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQFBpcGUoe1xyXG4gIG5hbWU6IFwidGV4dFNob3J0ZW5lclwiLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGV4dFNob3J0ZW5lclBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICB0cmFuc2Zvcm0odGV4dDogc3RyaW5nLCBsZW5ndGg6IG51bWJlciA9IDEwKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0ZXh0Lmxlbmd0aCA+IGxlbmd0aCA/IHRleHQuc3Vic3RyKDAsIGxlbmd0aCkgKyBcIi4uLlwiIDogdGV4dDtcclxuICB9XHJcbn1cclxuIl19