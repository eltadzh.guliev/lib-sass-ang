import * as i0 from "@angular/core";
import * as i1 from "./file-size.pipe";
import * as i2 from "./file-type.pipe";
import * as i3 from "./text-shortener.pipe";
export declare class LibsPipesModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<LibsPipesModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<LibsPipesModule, [typeof i1.FileSizePipe, typeof i2.FileTypePipe, typeof i3.TextShortenerPipe], never, [typeof i1.FileSizePipe, typeof i2.FileTypePipe, typeof i3.TextShortenerPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<LibsPipesModule>;
}
