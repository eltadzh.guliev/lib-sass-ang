import { PipeTransform } from "@angular/core";
import * as i0 from "@angular/core";
export declare class TextShortenerPipe implements PipeTransform {
    transform(text: string, length?: number): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<TextShortenerPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<TextShortenerPipe, "textShortener", false>;
}
