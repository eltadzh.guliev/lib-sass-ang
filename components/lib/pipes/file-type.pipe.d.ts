import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class FileTypePipe implements PipeTransform {
    transform(type: string, filename: string): string;
    private getFileTypeByApplicationType;
    static ɵfac: i0.ɵɵFactoryDeclaration<FileTypePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<FileTypePipe, "fileType", false>;
}
