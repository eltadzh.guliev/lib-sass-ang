import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ConfirmationWithIconDialogData } from '../../interfaces/confirmation-with-icon-dialog-data.interface';
import * as i0 from "@angular/core";
export declare class ConfirmationWithIconComponent implements OnInit {
    private readonly dialogRef;
    data: ConfirmationWithIconDialogData;
    constructor(dialogRef: MatDialogRef<ConfirmationWithIconComponent>, data: ConfirmationWithIconDialogData);
    ngOnInit(): void;
    onConfirmClick(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ConfirmationWithIconComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ConfirmationWithIconComponent, "app-confirmation-with-icon", never, {}, {}, never, never, false, never>;
}
