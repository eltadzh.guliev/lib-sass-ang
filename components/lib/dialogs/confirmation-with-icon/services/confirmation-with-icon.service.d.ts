import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationWithIconComponent } from '../dialogs/confirmation-with-icon/confirmation-with-icon.component';
import { ConfirmationWithIconDialogData } from '../interfaces/confirmation-with-icon-dialog-data.interface';
import * as i0 from "@angular/core";
export declare class ConfirmationWithIconService {
    private dialog;
    private readonly configs;
    constructor(dialog: MatDialog);
    open(configs?: MatDialogConfig): MatDialogRef<ConfirmationWithIconComponent, ConfirmationWithIconDialogData>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ConfirmationWithIconService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ConfirmationWithIconService>;
}
