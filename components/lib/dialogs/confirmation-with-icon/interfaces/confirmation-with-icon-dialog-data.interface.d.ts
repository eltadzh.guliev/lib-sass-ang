export interface ConfirmationWithIconDialogData {
    title: string;
    description: string;
    iconType: 'danger';
    closeButtonText: string;
    confirmatioButtonText: string;
}
