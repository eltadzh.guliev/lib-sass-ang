import * as i0 from "@angular/core";
import * as i1 from "./dialogs/confirmation-with-reason/confirmation-with-reason.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/material/dialog";
export declare class ConfirmationWithReasonModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<ConfirmationWithReasonModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<ConfirmationWithReasonModule, [typeof i1.ConfirmationWithReasonComponent], [typeof i2.CommonModule, typeof i3.ReactiveFormsModule, typeof i4.MatDialogModule], never>;
    static ɵinj: i0.ɵɵInjectorDeclaration<ConfirmationWithReasonModule>;
}
