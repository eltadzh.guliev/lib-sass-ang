import { OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { ConfirmationWithReasonDialogData } from "../../interfaces/confirmation-with-reason-dialog-data.interface";
import * as i0 from "@angular/core";
export declare class ConfirmationWithReasonComponent implements OnInit {
    private readonly fb;
    private readonly dialogRef;
    data: ConfirmationWithReasonDialogData;
    formGroup: FormGroup;
    constructor(fb: FormBuilder, dialogRef: MatDialogRef<ConfirmationWithReasonComponent>, data: ConfirmationWithReasonDialogData);
    ngOnInit(): void;
    private createFormGroup;
    onConfirmClick(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ConfirmationWithReasonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ConfirmationWithReasonComponent, "app-confirmation-with-reason", never, {}, {}, never, never, false, never>;
}
