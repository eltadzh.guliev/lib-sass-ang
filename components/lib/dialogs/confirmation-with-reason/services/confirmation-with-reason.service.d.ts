import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
import { ConfirmationWithReasonComponent } from "../dialogs/confirmation-with-reason/confirmation-with-reason.component";
import { ConfirmationWithReasonDialogData } from "../interfaces/confirmation-with-reason-dialog-data.interface";
import * as i0 from "@angular/core";
export declare class ConfirmationWithReasonService {
    private dialog;
    private readonly configs;
    constructor(dialog: MatDialog);
    open(configs?: MatDialogConfig): MatDialogRef<ConfirmationWithReasonComponent, ConfirmationWithReasonDialogData>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ConfirmationWithReasonService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ConfirmationWithReasonService>;
}
