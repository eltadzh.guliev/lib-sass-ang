import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from "@angular/material/snack-bar";
import { ToastSnackbarComponent } from "../components/toast-snackbar/toast-snackbar.component";
import { ToastType } from "../types/toast.type";
import * as i0 from "@angular/core";
export declare class ToastService {
    private readonly snackBar;
    readonly config: MatSnackBarConfig;
    constructor(snackBar: MatSnackBar);
    showMessage(message: string, status?: ToastType, action?: () => void | null, actionText?: string): MatSnackBarRef<ToastSnackbarComponent>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ToastService>;
}
