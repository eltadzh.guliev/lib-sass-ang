import { MatSnackBarRef } from '@angular/material/snack-bar';
import { CommonHelperService } from '../../../common.helper';
import { ToastData } from '../../interfaces/toast-data.interface';
import * as i0 from "@angular/core";
export declare class ToastSnackbarComponent {
    toastData: ToastData;
    private readonly matSnackBarRef;
    private readonly commonHelperService;
    get messageIsHTML(): boolean;
    constructor(toastData: ToastData, matSnackBarRef: MatSnackBarRef<ToastSnackbarComponent>, commonHelperService: CommonHelperService);
    onActionClick(): void;
    close(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastSnackbarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ToastSnackbarComponent, "app-toast-snackbar", never, {}, {}, never, never, false, never>;
}
