import * as i0 from "@angular/core";
import * as i1 from "./components/toast-snackbar/toast-snackbar.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/snack-bar";
export declare class ToastModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<ToastModule, [typeof i1.ToastSnackbarComponent], [typeof i2.CommonModule, typeof i3.MatSnackBarModule], never>;
    static ɵinj: i0.ɵɵInjectorDeclaration<ToastModule>;
}
