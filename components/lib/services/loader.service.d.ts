import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
export declare class LoaderService {
    isLoaderActive: BehaviorSubject<number>;
    constructor();
    increaseLoader(): void;
    decreaseLoader(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<LoaderService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<LoaderService>;
}
