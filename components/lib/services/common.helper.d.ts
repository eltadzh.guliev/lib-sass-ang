import { HttpHeaders } from '@angular/common/http';
import * as i0 from "@angular/core";
export declare class CommonHelperService {
    constructor();
    downloadBLOBWithDispositionName(blob: Blob, headers: HttpHeaders): void;
    getFilenameFromHeader(headers: HttpHeaders): string;
    downloadBLOB(blob: Blob, filename: string): void;
    isHTML(text: string): boolean;
    getRandomNumbers(numbersLength: number): string;
    blobToDataURL(blob: Blob): Promise<string | ArrayBuffer>;
    readFileAsText(file: File): Promise<string | ArrayBuffer>;
    static ɵfac: i0.ɵɵFactoryDeclaration<CommonHelperService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<CommonHelperService>;
}
