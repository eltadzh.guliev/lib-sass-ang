import { OnInit } from "@angular/core";
import * as i0 from "@angular/core";
export declare class PaginationInfoComponent implements OnInit {
    pageFrom: number;
    pageTo: number;
    totalItemCount: number;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<PaginationInfoComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<PaginationInfoComponent, "app-pagination-info", never, { "pageFrom": "pageFrom"; "pageTo": "pageTo"; "totalItemCount": "totalItemCount"; }, {}, never, never, false, never>;
}
