import { EventEmitter, OnInit } from "@angular/core";
import { PagenationResponse } from '../interfaces/paginationResponse';
import * as i0 from "@angular/core";
export declare class PaginationComponent implements OnInit {
    identifier: string;
    pageableData: PagenationResponse<any>;
    pageChange: EventEmitter<number>;
    constructor();
    ngOnInit(): void;
    onPageChange(page: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<PaginationComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<PaginationComponent, "app-pagination", never, { "identifier": "identifier"; "pageableData": "pageableData"; }, { "pageChange": "pageChange"; }, never, never, false, never>;
}
