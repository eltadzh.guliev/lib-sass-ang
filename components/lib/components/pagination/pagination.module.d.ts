import * as i0 from "@angular/core";
import * as i1 from "./components/pagination/pagination.component";
import * as i2 from "./components/pagination-info/pagination-info.component";
import * as i3 from "@angular/common";
export declare class PaginationModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<PaginationModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<PaginationModule, [typeof i1.PaginationComponent, typeof i2.PaginationInfoComponent], [typeof i3.CommonModule], [typeof i1.PaginationComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<PaginationModule>;
}
