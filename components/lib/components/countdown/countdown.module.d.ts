import * as i0 from "@angular/core";
import * as i1 from "./components/countdown/countdown.component";
import * as i2 from "./pipes/format-time.pipe";
import * as i3 from "@angular/common";
import * as i4 from "angular-svg-round-progressbar";
export declare class CountdownModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<CountdownModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<CountdownModule, [typeof i1.CountdownComponent, typeof i2.FormatTimePipe], [typeof i3.CommonModule, typeof i4.RoundProgressModule], [typeof i1.CountdownComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<CountdownModule>;
}
