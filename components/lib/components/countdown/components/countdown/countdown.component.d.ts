import { EventEmitter, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class CountdownComponent implements OnInit {
    set counterMinutes(value: number);
    timeIsUp: EventEmitter<void>;
    currentCounterMinutes: number;
    maxCounterMinutes: number;
    constructor();
    ngOnInit(): void;
    startTick(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CountdownComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CountdownComponent, "app-countdown", never, { "counterMinutes": "counterMinutes"; }, { "timeIsUp": "timeIsUp"; }, never, never, false, never>;
}
