import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class FormatTimePipe implements PipeTransform {
    transform(value: number): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<FormatTimePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<FormatTimePipe, "formatTime", false>;
}
