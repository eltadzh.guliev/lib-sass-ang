import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class PanelInfoComponent implements OnInit {
    iconName: string;
    title: string;
    description: string;
    set backgroundColorName(backgroundColorName: 'default' | 'brand');
    isDescriptionHTML: boolean;
    backgroundColorDefault: boolean;
    backgroundColorBrand: boolean;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<PanelInfoComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<PanelInfoComponent, "app-panel-info", never, { "iconName": "iconName"; "title": "title"; "description": "description"; "backgroundColorName": "backgroundColorName"; "isDescriptionHTML": "isDescriptionHTML"; }, {}, never, never, false, never>;
}
