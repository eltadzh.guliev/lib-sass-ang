import * as i0 from "@angular/core";
import * as i1 from "./components/panel-info/panel-info.component";
import * as i2 from "@angular/common";
export declare class PanelInfoModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<PanelInfoModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<PanelInfoModule, [typeof i1.PanelInfoComponent], [typeof i2.CommonModule], [typeof i1.PanelInfoComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<PanelInfoModule>;
}
