import * as i0 from "@angular/core";
export declare class BackgroundedTextComponent {
    text: string;
    set type(value: "success" | "warning" | "danger");
    innerType: "success" | "warning" | "danger";
    successTypeClass: boolean;
    warningTypeClass: boolean;
    dangerTypeClass: boolean;
    constructor();
    private resetTypeClass;
    private setTypeClass;
    static ɵfac: i0.ɵɵFactoryDeclaration<BackgroundedTextComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<BackgroundedTextComponent, "app-backgrounded-text", never, { "text": "text"; "type": "type"; }, {}, never, never, false, never>;
}
