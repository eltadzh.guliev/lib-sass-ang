import * as i0 from "@angular/core";
import * as i1 from "./components/backgrounded-text/backgrounded-text.component";
import * as i2 from "@angular/common";
export declare class BackgroundedTextModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<BackgroundedTextModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<BackgroundedTextModule, [typeof i1.BackgroundedTextComponent], [typeof i2.CommonModule], [typeof i1.BackgroundedTextComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<BackgroundedTextModule>;
}
