import { OnInit, EventEmitter } from '@angular/core';
import { TabSmall } from '../../interfaces/tab-small.interface';
import * as i0 from "@angular/core";
export declare class TabsSmallComponent implements OnInit {
    tabs: TabSmall[];
    selectedId: string;
    selectedIdChange: EventEmitter<string>;
    constructor();
    ngOnInit(): void;
    onTabClick(id: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TabsSmallComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TabsSmallComponent, "app-tabs-small", never, { "tabs": "tabs"; "selectedId": "selectedId"; }, { "selectedIdChange": "selectedIdChange"; }, never, never, false, never>;
}
