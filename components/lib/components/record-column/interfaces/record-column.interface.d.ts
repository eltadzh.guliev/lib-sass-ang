export interface RecordColumn {
    name: string;
    value: string;
}
