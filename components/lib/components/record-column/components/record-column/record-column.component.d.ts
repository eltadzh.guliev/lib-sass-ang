import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class RecordColumnComponent implements OnInit {
    name: string;
    value: string;
    valueColorName: 'brand' | 'default';
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<RecordColumnComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<RecordColumnComponent, "app-record-column", never, { "name": "name"; "value": "value"; "valueColorName": "valueColorName"; }, {}, never, ["*"], false, never>;
}
