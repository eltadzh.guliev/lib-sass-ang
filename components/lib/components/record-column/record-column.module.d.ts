import * as i0 from "@angular/core";
import * as i1 from "./components/record-column/record-column.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
export declare class RecordColumnModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<RecordColumnModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<RecordColumnModule, [typeof i1.RecordColumnComponent], [typeof i2.CommonModule, typeof i3.ReactiveFormsModule], [typeof i1.RecordColumnComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<RecordColumnModule>;
}
