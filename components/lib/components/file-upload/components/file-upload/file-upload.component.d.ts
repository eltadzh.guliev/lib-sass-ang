import { EventEmitter } from '@angular/core';
import { FileSizePipe } from '../../../../pipes/file-size.pipe';
import { ToastService } from '../../../../services/toast/services/toast.service';
import { FileUpload } from '../../interfaces/file-upload';
import * as i0 from "@angular/core";
export declare class FileUploadComponent {
    private readonly fileSizePipe;
    private readonly toastService;
    fileUploads: FileUpload[];
    maxCount: number;
    accept: ('' | '.pdf' | '.docx' | '.doc' | '.xlsx' | '.xls' | '.adoc' | '.jpg' | '.jpeg' | '.png')[];
    maxSize: number;
    disabled: boolean;
    fileLabel: string;
    hasDownloadButton: boolean;
    hasDeleteButton: boolean;
    addFile: EventEmitter<File>;
    deleteFile: EventEmitter<string>;
    downloadFile: EventEmitter<string>;
    constructor(fileSizePipe: FileSizePipe, toastService: ToastService);
    onFileChange(event: Event): void;
    onDeleteFile(id: string): void;
    onDownloadFile(id: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<FileUploadComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<FileUploadComponent, "app-file-upload", never, { "fileUploads": "fileUploads"; "maxCount": "maxCount"; "accept": "accept"; "maxSize": "maxSize"; "disabled": "disabled"; "fileLabel": "fileLabel"; "hasDownloadButton": "hasDownloadButton"; "hasDeleteButton": "hasDeleteButton"; }, { "addFile": "addFile"; "deleteFile": "deleteFile"; "downloadFile": "downloadFile"; }, never, never, false, never>;
}
