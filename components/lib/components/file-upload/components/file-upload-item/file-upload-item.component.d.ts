import { EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export declare class FileUploadItemComponent {
    id: string;
    name: string;
    type: string;
    size: number;
    hasDownloadButton: boolean;
    hasDeleteButton: boolean;
    icon: "file_icon";
    deleteFile: EventEmitter<void>;
    downloadFile: EventEmitter<void>;
    constructor();
    onDeleteFile(): void;
    onDownloadFile(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<FileUploadItemComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<FileUploadItemComponent, "app-file-upload-item", never, { "id": "id"; "name": "name"; "type": "type"; "size": "size"; "hasDownloadButton": "hasDownloadButton"; "hasDeleteButton": "hasDeleteButton"; "icon": "icon"; }, { "deleteFile": "deleteFile"; "downloadFile": "downloadFile"; }, never, never, false, never>;
}
