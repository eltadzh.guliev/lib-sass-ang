import * as i0 from "@angular/core";
import * as i1 from "./components/file-upload/file-upload.component";
import * as i2 from "./components/file-upload-item/file-upload-item.component";
import * as i3 from "@angular/common";
import * as i4 from "../../pipes/libs-pipes.module";
import * as i5 from "../../services/toast/toast.module";
export declare class FileUploadModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<FileUploadModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<FileUploadModule, [typeof i1.FileUploadComponent, typeof i2.FileUploadItemComponent], [typeof i3.CommonModule, typeof i4.LibsPipesModule, typeof i5.ToastModule], [typeof i1.FileUploadComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<FileUploadModule>;
}
