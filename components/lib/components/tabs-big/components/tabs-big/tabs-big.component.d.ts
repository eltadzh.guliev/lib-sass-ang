import { OnInit, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class TabsBigComponent implements OnInit {
    tabs: any;
    selectedId: string;
    selectedIdChange: EventEmitter<string>;
    constructor();
    ngOnInit(): void;
    onTabClick(id: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TabsBigComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TabsBigComponent, "app-tabs-big", never, { "tabs": "tabs"; "selectedId": "selectedId"; }, { "selectedIdChange": "selectedIdChange"; }, never, never, false, never>;
}
