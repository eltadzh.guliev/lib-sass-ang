import * as i0 from "@angular/core";
import * as i1 from "./components/tabs-big/tabs-big.component";
import * as i2 from "@angular/common";
export declare class TabsBigModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<TabsBigModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<TabsBigModule, [typeof i1.TabsBigComponent], [typeof i2.CommonModule], [typeof i1.TabsBigComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<TabsBigModule>;
}
