import * as i0 from "@angular/core";
import * as i1 from "./components/iconned-text/iconned-text.component";
import * as i2 from "@angular/common";
export declare class IconnedTextModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<IconnedTextModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<IconnedTextModule, [typeof i1.IconnedTextComponent], [typeof i2.CommonModule], [typeof i1.IconnedTextComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<IconnedTextModule>;
}
