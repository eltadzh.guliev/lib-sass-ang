import * as i0 from "@angular/core";
export declare class IconnedTextComponent {
    text: string;
    iconName: string;
    constructor();
    static ɵfac: i0.ɵɵFactoryDeclaration<IconnedTextComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IconnedTextComponent, "app-iconned-text", never, { "text": "text"; "iconName": "iconName"; }, {}, never, never, false, never>;
}
